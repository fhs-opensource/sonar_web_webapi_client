package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AuthenticationRpcServiceTest {
    private AuthenticationRpcService authenticationRpcService;

    @BeforeEach
    void setUp() {
        authenticationRpcService = ApiBilderFactory.getApiBuilder().buildApi(AuthenticationRpcService.class);
    }

    @Test
    void login() {
        System.out.println(authenticationRpcService.login("haha", "123"));
    }

    @Test
    void logout() {
        authenticationRpcService.logout();
    }

    @Test
    void checkCredentials() {
        System.out.println(authenticationRpcService.checkCredentials());
    }
}