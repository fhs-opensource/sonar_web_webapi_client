package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProjectLinksRpcServiceTest {

    private ProjectLinksRpcService projectLinksRpcService;

    @BeforeEach
    void setUp() {
        projectLinksRpcService = ApiBilderFactory.getApiBuilder().buildApi(ProjectLinksRpcService.class);
    }

    @Test
    void createNewProjectLink() {
        System.out.println(projectLinksRpcService.createNewProjectLink("grand", "AU-Tpxb--iU5OvuD2FLy", "key", "http://www.baidu.com"));
    }

    @Test
    void deleteProjectLink() {
        projectLinksRpcService.deleteProjectLink("17");
    }

    @Test
    void getLinkOfProject() {
        System.out.println(projectLinksRpcService.getLinkOfProject("AU-Tpxb--iU5OvuD2FLy", "pro"));
    }
}