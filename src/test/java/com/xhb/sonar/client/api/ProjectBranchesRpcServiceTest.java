package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProjectBranchesRpcServiceTest {

    private ProjectBranchesRpcService projectBranchesRpcService;

    @BeforeEach
    void setUp() {
        projectBranchesRpcService = ApiBilderFactory.getApiBuilder().buildApi(ProjectBranchesRpcService.class);
    }


    @Test
    void deleteProjectBranches() {
        projectBranchesRpcService.deleteProjectBranches("branch","gaga");
    }

    @Test
    void getProjectBranches() {
        System.out.println(projectBranchesRpcService.getProjectBranches("gaga"));
    }

    @Test
    void renameForBranch() {
        projectBranchesRpcService.renameForBranch("branch","gaga");
    }
}