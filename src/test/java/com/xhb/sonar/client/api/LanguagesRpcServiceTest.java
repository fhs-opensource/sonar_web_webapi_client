package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LanguagesRpcServiceTest {

    private LanguagesRpcService programmingLanguagesRpcService;

    @BeforeEach
    void setUp() {
        programmingLanguagesRpcService = ApiBilderFactory.getApiBuilder().buildApi(LanguagesRpcService.class);
    }


    @Test
    void getProgrammingLanguages() {
        System.out.println(programmingLanguagesRpcService.getProgrammingLanguages(0, "java"));
    }
}