package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PermissionsRpcServiceTest {

    private PermissionsRpcService permissionsRpcService;

    @BeforeEach
    void setUp() {
        permissionsRpcService = ApiBilderFactory.getApiBuilder().buildApi(PermissionsRpcService.class);
    }


    @Test
    void addGroup() {
        permissionsRpcService.addGroup("42","admin","admin","ce4c03d6-430f-40a9-b777-ad877c00aa4d","project");
    }

    @Test
    void addGroupToTemplate() {
        permissionsRpcService.addGroupToTemplate("42","admin","admin","AU-Tpxb--iU5OvuD2FLy","Default Permission Template for Projects");
    }

    @Test
    void addProjectCreatorToTemplate() {
        permissionsRpcService.addProjectCreatorToTemplate("admin","AU-Tpxb--iU5OvuD2FLy","Default Permission Template for Projects");
    }

    @Test
    void addUser() {
        permissionsRpcService.addUser("admin","admin","ce4c03d6-430f-40a9-b777-ad877c00aa4d","project");
    }

    @Test
    void addUserToTemplate() {
        permissionsRpcService.addUserToTemplate("admin","admin","AU-Tpxb--iU5OvuD2FLy","Default Permission Template for Projects");
    }

    @Test
    void applyTemplate() {
        permissionsRpcService.applyTemplate("ce4c03d6-430f-40a9-b777-ad877c00aa4d","project","AU-Tpxb--iU5OvuD2FLy","Default Permission Template for Projects");
    }

    @Test
    void bulkApplyTemplate() {
        permissionsRpcService.bulkApplyTemplate(null,null,null,null,null,null,null);
    }

    @Test
    void createTemplate() {
        System.out.println(permissionsRpcService.createTemplate(null, "admin", null));
    }

    @Test
    void deleteTemplate() {
        permissionsRpcService.deleteTemplate(null,null);
    }

    @Test
    void removeGroup() {
        permissionsRpcService.removeGroup(null,null,"admin",null,null);
    }

    @Test
    void removeGroupFromTemplate() {
        permissionsRpcService.removeGroupFromTemplate(null,"admin","admin","AU-Tpxb--iU5OvuD2FLy","aa");
    }

    @Test
    void removeProjectCreatorFromTemplate() {
        permissionsRpcService.removeProjectCreatorFromTemplate("admin","AU-Tpxb--iU5OvuD2FLy","aa");
    }

    @Test
    void removeUser() {
        permissionsRpcService.removeUser("admin","admin","ce4c03d6-430f-40a9-b777-ad877c00aa4d","project");
    }


    @Test
    void removeUserFromTemplate() {
        permissionsRpcService.removeUserFromTemplate("admin","admin","AU-Tpxb--iU5OvuD2FLy","ad");
    }

    @Test
    void searchTemplates() {
        permissionsRpcService.searchTemplates("default");
    }

    @Test
    void setDefaultTemplate() {
        permissionsRpcService.setDefaultTemplate("TRK","AU-Tpxb--iU5OvuD2FLy","Default Permission Template for Projects");
    }

    @Test
    void updateTemplate() {
        System.out.println(permissionsRpcService.updateTemplate(null, "af8cb8cc-1e78-4c4e-8c00-ee8e814009a5", null, null));
    }
}