package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserGroupsRpcServiceTest {

    private UserGroupsRpcService userGroupsRpcService;

    @BeforeEach
    void setUp() {
        userGroupsRpcService = ApiBilderFactory.getApiBuilder().buildApi(UserGroupsRpcService.class);
    }


    @Test
    void addUserToGroup() {
        userGroupsRpcService.addUserToGroup("42","admin","ga");
    }

    @Test
    void createGroup() {
        System.out.println(userGroupsRpcService.createGroup("Default group for new users", "admin"));
    }

    @Test
    void deleteGroup() {
        userGroupsRpcService.deleteGroup("42","admin");
    }

    @Test
    void removeUser() {
        userGroupsRpcService.removeUser("42","admin","admin");
    }

    @Test
    void userGroupSearchResult() {
        System.out.println(userGroupsRpcService.userGroupSearchResult("description", "1", "100", "ga"));
    }

    @Test
    void updateGroup() {
        userGroupsRpcService.updateGroup("ga","42","admin");
    }

    @Test
    void getMembershipUser() {
        System.out.println(userGroupsRpcService.getMembershipUser("42","admin","1","25","afa","deselected"));
    }
}