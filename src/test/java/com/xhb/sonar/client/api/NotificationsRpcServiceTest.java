package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NotificationsRpcServiceTest {

    private NotificationsRpcService notificationsRpcService;

    @BeforeEach
    void setUp() {
        notificationsRpcService = ApiBilderFactory.getApiBuilder().buildApi(NotificationsRpcService.class);
    }


    @Test
    void addNotification() {
        notificationsRpcService.addNotification("EmailNotificationChannel",null,"gaga","SQ-MyNewIssues");
    }

    @Test
    void getUserNotifications() {
        System.out.println(notificationsRpcService.getUserNotifications("admin"));
    }

    @Test
    void removeNotification() {
        notificationsRpcService.removeNotification("EmailNotificationChannel",null,"gaga","SQ-MyNewIssues");
    }
}