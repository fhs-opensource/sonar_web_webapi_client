package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.ComponentsSearchResultDTO;
import com.xhb.sonar.client.dto.GetComputeDTO;
import com.xhb.sonar.client.dto.MeasuresComponentResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IssuesProjectsComponentsRpcServiceTest {

    ComponentsRpcService componentsRpcService;


    @BeforeEach
    void setUp() {
        componentsRpcService = ApiBilderFactory.getApiBuilder().buildApi(ComponentsRpcService.class);
    }


    @Test
    void search() {
        ComponentsSearchResultDTO result = componentsRpcService.search("java",null,null,null,"BRC,DIR,FIL,TRK,UTS");
        System.out.println(JsonUtil.bean2json(result));
    }

    @Test
    void getCompute() {
        GetComputeDTO gaga = componentsRpcService.getCompute("gaga");
        System.out.println(JsonUtil.bean2json(gaga));
    }

    @Test
    void getComputeByChosenStrategy() {
        System.out.println(JsonUtil.bean2json(componentsRpcService.getComputeByChosenStrategy(true, "gaga", 1, 100, "FILE_NAM", "BRC,DIR,FIL,TRK,UTS", "name", "all")));
    }
}