package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PluginsRpcServiceTest {

    private PluginsRpcService pluginsRpcService;

    @BeforeEach
    void setUp() {
        pluginsRpcService = ApiBilderFactory.getApiBuilder().buildApi(PluginsRpcService.class);
    }


    @Test
    void getAvailablePlugins() {
        System.out.println(pluginsRpcService.getAvailablePlugins());
    }

    @Test
    void cancelAllOperation() {
        pluginsRpcService.cancelAllOperation();
    }

    @Test
    void installPlugins() {
        pluginsRpcService.installPlugins("plugin");
    }

    @Test
    void getPluginsInstalled() {
        System.out.println(pluginsRpcService.getPluginsInstalled("category"));
    }

    @Test
    void getPendingPlugins() {
        System.out.println(pluginsRpcService.getPendingPlugins());
    }

    @Test
    void uninstallPlugin() {
        pluginsRpcService.uninstallPlugin("plugin");
    }

    @Test
    void updatePlugins() {
        pluginsRpcService.updatePlugins("plugin");
    }

    @Test
    void getUpdatesPlugins() {
        System.out.println(pluginsRpcService.getUpdatesPlugins());
    }
}