package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProjectPullRequestsRpcServiceTest {


    private ProjectPullRequestsRpcService projectPullRequestsRpcService;

    @BeforeEach
    void setUp() {
        projectPullRequestsRpcService = ApiBilderFactory.getApiBuilder().buildApi(ProjectPullRequestsRpcService.class);
    }

    @Test
    void deletePullRequest() {
        projectPullRequestsRpcService.deletePullRequest(10,"request");

    }

    @Test
    void getPullRequestsFroProject() {
        System.out.println(projectPullRequestsRpcService.getPullRequestsFroProject(1));
    }
}