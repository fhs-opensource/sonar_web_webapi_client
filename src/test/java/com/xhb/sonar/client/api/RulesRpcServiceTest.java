package com.xhb.sonar.client.api;

import com.xhb.sonar.client.builder.ApiBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RulesRpcServiceTest {

    public RulesRpcService rulesRpcService;

    public String username = "admin";
    public String password = "admin";
    public String url = "http://192.168.0.213:9000/";

    @BeforeEach
    void setUp() {
        ApiBuilder apiBuilder = new ApiBuilder(username, password, url);
        rulesRpcService = apiBuilder.buildApi(RulesRpcService.class);
    }

    @Test
    void changePassword() {
        System.out.println(rulesRpcService.createRule("Todo_should_not_be_used",
                "Description of my custom rule", "My custom rule",
                null, null, null, null, null, null));
    }

    @Test
    void deleteRule() {
        rulesRpcService.deleteRule("squid:XPath_1402065390816");
    }

    @Test
    void getRuleRepositories() {
        System.out.println(rulesRpcService.getRuleRepositories("java", "squid"));
    }

    @Test
    void ruleSearchResult() {
        System.out.println(rulesRpcService.ruleSearchResult());
    }

    @Test
    void getRuleInfo() {
        System.out.println(rulesRpcService.getRuleInfo("true", "javascript:EmptyBlock"));
    }

    @Test
    void getRuleTags() {
        System.out.println(rulesRpcService.getRuleTags("10", "misra"));
    }

    @Test
    void updateRule() {
        System.out.println(rulesRpcService.updateRule("javascript:NullCheck", null, null, null, null, null, null, null, null, null, null));
    }
}