package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.Sources;
import com.xhb.sonar.client.dto.SourcesLinesDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SourcesRpcServiceTest {

    private SourcesRpcService sourcesRpcService;

    @BeforeEach
    void setUp() {
        sourcesRpcService = ApiBilderFactory.getApiBuilder().buildApi(SourcesRpcService.class);
    }

    @Test
    void getRaw() {
        sourcesRpcService.getRaw("my_project:src/foo/Bar.php");
    }

    @Test
    void testGetRaw() {
        System.out.println(sourcesRpcService.getScmInformation("true", "1", "my_project:/src/foo/Bar.php", "20"));
    }

    @Test
    void testGetRaw1() {
        System.out.println(sourcesRpcService.getSourceCode("1", "my_project:/src/foo/Bar.php", "20"));
    }

    @Test
    void getSourcesLines()
    {
        SourcesLinesDTO sourcesLines = sourcesRpcService.getSourcesLines(null, 13, "ks-cms-unicorn:fhs_app/fhs_app_all_in_one/src/main/java/com/fhs/app/SingleApplication.java", null, 13, null);
        System.out.println("--------------------------");
        for (Sources source : sourcesLines.getSources())
        {
            System.out.println(source);
        }
    }
}