package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FavoritesRpcServiceTest {

    FavoritesRpcService favoritesRpcService;


    @BeforeEach
    void setUp() {
        favoritesRpcService = ApiBilderFactory.getApiBuilder().buildApi(FavoritesRpcService.class);
    }


    @Test
    void addComment() {
        favoritesRpcService.addComment("my_project:/src/foo/Bar.php");
    }

    @Test
    void removeComponent() {
        favoritesRpcService.removeComponent("gaga");
    }

    @Test
    void searchAuthenticatedFavorites() {
        System.out.println(favoritesRpcService.searchAuthenticatedFavorites(1, 10));
    }
}