package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProjectsBadgesRpcServiceTest {
    private ProjectsBadgesRpcService projectsBadgesRpcService;

    @BeforeEach
    void setUp() {
        projectsBadgesRpcService = ApiBilderFactory.getApiBuilder().buildApi(ProjectsBadgesRpcService.class);
    }
    @Test
    void getProjectBadgesMeasure() {
        System.out.println(projectsBadgesRpcService.getProjectBadgesMeasure("feature/my_branch", "bugs", "pro"));
    }

    @Test
    void generateBadgeForQualityGate() {
        System.out.println(projectsBadgesRpcService.generateBadgeForQualityGate("feature/my_branch", "pro"));
    }
}