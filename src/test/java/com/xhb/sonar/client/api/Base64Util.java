package com.xhb.sonar.client.api;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

public class Base64Util {

    /**
     * 将 s 进行 BASE64 编码
     *
     * @param s
     * @return
     */
    public static String getBASE64(String s) {
        if (s == null) {
            return null;
        }
        return byte2Base64(s.getBytes());
    }

    /**
     * byte数组转base64字符串
     *
     * @param bytes
     * @return
     */
    public static String byte2Base64(byte[] bytes) {
        try {
            return new String(Base64.getEncoder().encode(bytes), "UTF8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
