package com.xhb.sonar.client.api;

import com.xhb.sonar.client.builder.ApiBuilder;
import com.xhb.sonar.client.dto.MeasuresComponentResult;
import feign.Feign;
import feign.Request;
import feign.Retryer;
import feign.form.FormEncoder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MeasuresRpcServiceTest
{

    private MeasuresRpcService measuresRpcService;

    @BeforeEach
    void setUp()
    {
        measuresRpcService = ApiBilderFactory.getApiBuilder().buildApi(MeasuresRpcService.class);
    }

    @Test
    void componentResult()
    {
        MeasuresComponentResult result = measuresRpcService.componentResult(null, null,
                "ks-cms-unicorn",
                "blocker_violations,critical_violations,major_violations,minor_violations"
                , null);
        System.out.println(JsonUtil.bean2json(result));
    }

    @Test
    void getComponentTree()
    {
        System.out.println(measuresRpcService.getComponentTree("metrics", true, "gaga", "ncloc,complexity,violations", null, null, null, null, null, null, null, null, null));
    }

    @Test
    void getSearchHistory()
    {
        System.out.println(measuresRpcService.getSearchHistory("gaga", "2017-10-19", "ncloc,coverage,new_violations", null, null, null));
    }
}