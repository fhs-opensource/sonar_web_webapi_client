package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ComputeEngineRpcServiceTest {


    ComputeEngineRpcService computeEngineRpcService;


    @BeforeEach
    void setUp() {
        computeEngineRpcService = ApiBilderFactory.getApiBuilder().buildApi(ComputeEngineRpcService.class);
    }


    @Test
    void searchTasks() {
        System.out.println(computeEngineRpcService.searchTasks(null, "2017-10-19T13:00:00+0200", "2017-10-19T13:00:00+0200", true, 100, "Apache", "SUCCESS", "REPORT"));
    }

    @Test
    void getActivityStatus() {
        System.out.println(computeEngineRpcService.getActivityStatus("AU-TpxcA-iU5OvuD2FL0"));
    }

    @Test
    void getComponent() {
        System.out.println(computeEngineRpcService.getComponent("my_project"));
    }

    @Test
    void getComputeEngineTask() {
        System.out.println(computeEngineRpcService.getComputeEngineTask("stacktrace", "AU-Tpxb--iU5OvuD2FLy"));
    }
}