package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WebhooksRpcServiceTest {
    private WebhooksRpcService webhooksRpcService;

    @BeforeEach
    void setUp() {
        webhooksRpcService = ApiBilderFactory.getApiBuilder().buildApi(WebhooksRpcService.class);
    }



    @Test
    void createWebhook() {
        System.out.println(webhooksRpcService.createWebhook("My webhook", "gaga", null, "https://www.my-webhook-listener.com/sonar"));
    }

    @Test
    void getRecentDeliveries() {
        webhooksRpcService.getRecentDeliveries("AU-Tpxb--iU5OvuD2FLy","my_project",1,10,"AU-TpxcA-iU5OvuD2FLz");
    }

    @Test
    void getWebhookDelivery() {
        System.out.println(webhooksRpcService.getWebhookDelivery("AU-TpxcA-iU5OvuD2FL3"));
    }

    @Test
    void searchGlobalWebhooks() {
        System.out.println(webhooksRpcService.searchGlobalWebhooks("gaga"));
    }

    @Test
    void updateWebhooks() {
        webhooksRpcService.updateWebhooks("My Webhook","your_secret","https://www.my-webhook-listener.com/sonar","gaga");
    }

    @Test
    void deleteWebhook() {
        webhooksRpcService.deleteWebhook("my_project");
    }
}