package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QualitygatesRpcServiceTest {


    private QualitygatesRpcService qualitygatesRpcService;

    @BeforeEach
    void setUp() {
        qualitygatesRpcService = ApiBilderFactory.getApiBuilder().buildApi(QualitygatesRpcService.class);
    }


    @Test
    void copyQualityGate() {
        qualitygatesRpcService.copyQualityGate("AU-Tpxb--iU5OvuD2FLy",null,null);
    }

    @Test
    void createQualityGate() {
        System.out.println(qualitygatesRpcService.createQualityGate("gaga", "dada"));
    }

    @Test
    void createCondition() {
        System.out.println(qualitygatesRpcService.createCondition("10", "1", "bula", "LT", "org"));
    }

    @Test
    void deleteCondition() {
        qualitygatesRpcService.deleteCondition("moba","my-org");
    }

    @Test
    void removeProjectAssociationForQualitygate() {
        qualitygatesRpcService.removeProjectAssociationForQualitygate("my-org","my_project");
    }

    @Test
    void deleteQualitygates() {
        qualitygatesRpcService.deleteQualitygates("1","my-org");
    }

    @Test
    void getByProject() {
        System.out.println(qualitygatesRpcService.getByProject("my-org", "pro"));
    }

    @Test
    void getQualitygates() {
        System.out.println(qualitygatesRpcService.getQualitygates("ga"));
    }

    @Test
    void getQualitygatesStatusForProject() {
        System.out.println(qualitygatesRpcService.getQualitygatesStatusForProject("ga", "ba", "fa", "da", "wa"));
    }

    @Test
    void renameOfQualitygates() {
        qualitygatesRpcService.renameOfQualitygates("1","gate","org");
    }

    @Test
    void searchProjeectForQualitygates() {
        System.out.println(qualitygatesRpcService.searchProjeectForQualitygates("1", "org", null, null, null, null));
    }

    @Test
    void associateProjectToQualitygate() {
        qualitygatesRpcService.associateProjectToQualitygate("1",null,null,null);
    }

    @Test
    void setQualitygatesDefault() {
        qualitygatesRpcService.setQualitygatesDefault("1","org");
    }

    @Test
    void getQualitygatesDetails() {
        System.out.println(qualitygatesRpcService.getQualitygatesDetails("1", "gate", "org"));
    }

    @Test
    void updateCondition() {
        qualitygatesRpcService.updateCondition("10","10","ga","LT",null);
    }
}