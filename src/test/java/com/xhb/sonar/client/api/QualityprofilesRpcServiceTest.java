package com.xhb.sonar.client.api;

import com.xhb.sonar.client.builder.ApiBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QualityprofilesRpcServiceTest {

    private QualityprofilesRpcService qualityprofilesRpcService;

    @BeforeEach
    void setUp() {
        qualityprofilesRpcService = ApiBilderFactory.getApiBuilder().buildApi(QualityprofilesRpcService.class);
    }


    @Test
    void activateRule() {
        qualityprofilesRpcService.activateRule("AU-Tpxb--iU5OvuD2FLy", null, null, "squid:AvoidCycles", null);
    }

    @Test
    void activateRules() {
        qualityprofilesRpcService.activateRules(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    @Test
    void addProject() {
        qualityprofilesRpcService.addProject("java", "project", "file");
    }

    @Test
    void backupConfigXML() {
        System.out.println(qualityprofilesRpcService.backupConfigXML("xml", "application"));
    }

    @Test
    void changeParent() {
        qualityprofilesRpcService.changeParent("java","file","chileFile");
    }

    @Test
    void qualityprofilesChangelog() {
        System.out.println(qualityprofilesRpcService.qualityprofilesChangelog(null, null, null, null, null, null));
    }

    @Test
    void copyQualityProfile() {
        System.out.println(qualityprofilesRpcService.copyQualityProfile("AXTE9-eVCY4r44UAF5fq", "Sonar way33"));
    }

    @Test
    void createQualityProfile() {
        System.out.println(qualityprofilesRpcService.createQualityProfile("java", "Sonar way33",null));
    }

    @Test
    void deactivateRule() {
        qualityprofilesRpcService.deactivateRule("AU-Tpxb--iU5OvuD2FLy","squid:AvoidCycles");
    }

    @Test
    void deactivateRules() {
        qualityprofilesRpcService.deactivateRules(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    @Test
    void deleteAllQualityProfile() {
        qualityprofilesRpcService.deleteAllQualityProfile("java","Sonar way");
    }

    @Test
    void exportQualityProfile() {
        qualityprofilesRpcService = new ApiBuilder("admin", "admin", "http://192.168.0.213:9000/").buildApiNoJson(QualityprofilesRpcService.class);
        System.out.println(qualityprofilesRpcService.exportQualityProfile("java",null, "Sonar way"));
    }

    @Test
    void getQualityProfileFormatList() {
        System.out.println(qualityprofilesRpcService.getQualityProfileFormatList());
    }

    @Test
    void getSupportedImporters() {
        System.out.println(qualityprofilesRpcService.getSupportedImporters());
    }

    @Test
    void getQualityprofilesInheritance() {
        System.out.println(qualityprofilesRpcService.getQualityprofilesInheritance("java", "file"));
    }

    @Test
    void getProjectsAnQualityProfileAssociationStatus() {
        System.out.println(qualityprofilesRpcService.getProjectsAnQualityProfileAssociationStatus("AU-Tpxb--iU5OvuD2FLy", null, null, null, null));
    }

    @Test
    void removeProject() {
        qualityprofilesRpcService.removeProject("java","project","file");
    }

    @Test
    void renameToQualityProfile() {
        qualityprofilesRpcService.renameToQualityProfile("AU-Tpxb--iU5OvuD2FLy","MySonar");
    }

    @Test
    void restoreQualityProfile() {
        qualityprofilesRpcService.restoreQualityProfile("file");
    }

    @Test
    void searchQualityProfiles() {
        System.out.println(qualityprofilesRpcService.searchQualityProfiles("true", "java", null,null, null));
    }

    @Test
    void setDefault() {
        qualityprofilesRpcService.setDefault("java","MySonar");
    }
}