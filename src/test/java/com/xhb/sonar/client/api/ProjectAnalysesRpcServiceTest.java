package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProjectAnalysesRpcServiceTest {

    private ProjectAnalysesRpcService projectAnalysesRpcService;

    @BeforeEach
    void setUp() {
        projectAnalysesRpcService = ApiBilderFactory.getApiBuilder().buildApi(ProjectAnalysesRpcService.class);
    }


    @Test
    void createAnalysisEvent() {
        System.out.println(projectAnalysesRpcService.createAnalysisEvent("AU-Tpxb--iU5OvuD2FLy", "OTHER", "5.6"));
    }

    @Test
    void deleteProjectAnalysis() {
        projectAnalysesRpcService.deleteProjectAnalysis("AU-TpxcA-iU5OvuD2FL1");
    }

    @Test
    void deleteProjectAnalysisEvent() {
        projectAnalysesRpcService.deleteProjectAnalysisEvent("AU-TpxcA-iU5OvuD2FLz");
    }

    @Test
    void searchAnalysesAndAttachedEvents() {
        System.out.println(projectAnalysesRpcService.searchAnalysesAndAttachedEvents("OTHER", "2013-05-01", 1, "my_project", 100, "2017-10-19"));
    }

    @Test
    void setBaseline() {
        projectAnalysesRpcService.setBaseline("AU-Tpxb--iU5OvuD2FLy",null,"gaga");
    }

    @Test
    void unsetBaseline() {
        projectAnalysesRpcService.unsetBaseline(null,"gaga");
    }

    @Test
    void updateEvent() {
        System.out.println(projectAnalysesRpcService.updateEvent("AU-TpxcA-iU5OvuD2FL5", "5.6"));
    }
}