package com.xhb.sonar.client.api;

import com.xhb.sonar.client.builder.ApiBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UsersRpcServiceTest {


    public UsersRpcService usersRpcService;

    public String username = "admin";
    public String password = "admin";
    public String url = "http://192.168.0.213:9000/";

    @BeforeEach
    void setUp() {
        ApiBuilder apiBuilder = new ApiBuilder(username, password, url);
        usersRpcService = apiBuilder.buildApi(UsersRpcService.class);
    }


    @Test
    void changePassword() {
        usersRpcService.changePassword("myuser","mypassword","oldpassword");
    }

    @Test
    void createUser() {
        System.out.println(usersRpcService.createUser(null, null, "myuser", "myname", "mypassword", null));

    }

    @Test
    void deactivateUser() {
        System.out.println(usersRpcService.deactivateUser("myuser"));
    }

    @Test
    void getGroups() {
        System.out.println(usersRpcService.getGroups("admin", "1", "25", "users", "all"));
    }

    @Test
    void userSearchResult() {
        System.out.println(usersRpcService.userSearchResult("1", "25", "users"));
    }

    @Test
    void updateUser() {
        System.out.println(usersRpcService.updateUser(null, "admin", null, null));
    }

    @Test
    void updateLogin() {
        usersRpcService.updateLogin("admin","mynewlogin");
    }
}