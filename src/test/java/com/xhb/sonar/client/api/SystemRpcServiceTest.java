package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SystemRpcServiceTest {

    private SystemRpcService systemRpcService;

    @BeforeEach
    void setUp() {
        systemRpcService = ApiBilderFactory.getApiBuilder().buildApi(SystemRpcService.class);
    }


    @Test
    void changeLogLevel() {
        systemRpcService.changeLogLevel("TRACE");
    }

    @Test
    void getDbMigrationStatus() {
        System.out.println(systemRpcService.getDbMigrationStatus());
    }

    @Test
    void getHealthStatus() {
        System.out.println(systemRpcService.getHealthStatus());
    }

    @Test
    void getSettingLogs() {
        System.out.println(systemRpcService.getSettingLogs("app"));
    }

    @Test
    void migrateDb() {
        System.out.println(systemRpcService.migrateDb());
    }

    @Test
    void getPing() {
        System.out.println(systemRpcService.getPing());
    }

    @Test
    void restartServer() {
        systemRpcService.restartServer();
    }

    @Test
    void getSonarQubeStatus() {
        System.out.println(systemRpcService.getSonarQubeStatus());
    }

    @Test
    void getUpgradesSystem() {
        System.out.println(systemRpcService.getUpgradesSystem());
    }
}