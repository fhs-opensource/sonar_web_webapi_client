package com.xhb.sonar.client.api;

import com.sun.org.apache.xpath.internal.operations.String;
import com.xhb.sonar.client.dto.MeasuresComponentResult;
import com.xhb.sonar.client.dto.MetricsSearchResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MetricsRpcServiceTest {

    private MetricsRpcService metricsRpcService;

    @BeforeEach
    void setUp() {
        metricsRpcService = ApiBilderFactory.getApiBuilder().buildApi(MetricsRpcService.class);
    }

    @Test
    void search() {
        MetricsSearchResult result = metricsRpcService.search(null, null);
        StringBuilder keys = new StringBuilder();
        result.getMetrics().forEach(metrics -> {
            keys.append(metrics.getKey() + ",");
        });

        System.out.println(JsonUtil.bean2json(result));
        System.out.println(keys);
    }

    @Test
    void getMetricTypes() {
        System.out.println(metricsRpcService.getMetricTypes());
    }
}