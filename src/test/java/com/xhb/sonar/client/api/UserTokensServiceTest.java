package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UserTokensServiceTest {

    private UserTokensRpcService userTokensService;

    @BeforeEach
    void setUp() {
        userTokensService = ApiBilderFactory.getApiBuilder().buildApi(UserTokensRpcService.class);
    }


    @Test
    void generateUserToken() {
        System.out.println(userTokensService.generateUserToken(null, "Project scan on Travis"));
    }

    @Test
    void revoke() {
        userTokensService.revoke("admin","Project scan on Travis");
    }

    @Test
    void userTokenSearchResult() {
        System.out.println(userTokensService.userTokenSearchResult("admin"));
    }
}