package com.xhb.sonar.client.api;

import com.alibaba.fastjson.JSONObject;
import com.xhb.sonar.client.builder.ApiBuilder;
import com.xhb.sonar.client.dto.IssueSearchResultDTO;
import com.xhb.sonar.client.dto.IssuesComponents;
import feign.*;
import feign.codec.DecodeException;
import feign.codec.Decoder;
import feign.form.FormEncoder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Type;


class IssuesRpcServiceTest {


    private IssuesRpcService issuesRpcService;

    @BeforeEach
    void setUp() {
        this.issuesRpcService = new ApiBuilder("admin", "admin", "http://192.168.0.213:9000/").buildApi(IssuesRpcService.class);
//        this.issuesRpcService = new ApiBuilder(null, null, "http://192.168.0.213:9000/").buildApi(IssuesRpcService.class);

    }

    @Test
    void testAddComment() {
        System.out.println(JsonUtil.bean2json(issuesRpcService.addComment("AXTFB9MjX7mzCE_GVZ1o", "sb")));
    }

    @Test
    void issueSearchResult() {
        IssueSearchResultDTO issueSearchResultDTO = issuesRpcService.issueSearch("comments", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, "false", null, null, null, null, null, null, null, null, "BUG");

        for (IssuesComponents component : issueSearchResultDTO.getComponents())
        {
            System.out.println(component.getKey());
        }
//        System.out.println(issuesRpcService.issueSearchResult(null,null,null,null,null,"ks-cms-unicorn",null,null,null,null,null,"severities%2Ctypes",null,null,null,null,null,100,null,"false",null,"FILE_LINE",null,null,null,null,null,null,null));
    }

    @Test
    void testSetType() {
        System.out.println(JsonUtil.bean2json(issuesRpcService.setType("AXTFB9MjX7mzCE_GVZ1o", "CODE_SMELL")));
    }

    @Test
    void assign() {
        System.out.println(JsonUtil.bean2json(issuesRpcService.assign(null,"AXTFB_QwX7mzCE_GVcgJ")));
    }

    @Test
    void authors() {
        System.out.println(issuesRpcService.authors(null,null,null));
    }

    //---------------------- 参数不存在 START ------------------------

    @Test
    void bulkChange() {
        System.out.println(issuesRpcService.bulkChange(null, null, null, null, null, null, null, null, null));
    }

    @Test
    void changelog() {
        System.out.println(issuesRpcService.changelog("AU-Tpxb--iU5OvuD2FLy"));
    }

    @Test
    void deleteCommentBeanDTO() {
        System.out.println(issuesRpcService.deleteCommentBeanDTO("AU-Tpxb--iU5OvuD2FLy"));
    }

    @Test
    void doTransitionBeanDTO() {
        System.out.println(issuesRpcService.doTransitionBeanDTO("AU-Tpxb--iU5OvuD2FLy", "confirm"));
    }

    @Test
    void editComment() {
        issuesRpcService.editComment("AU-Tpxb--iU5OvuD2FLy", "Won't fix because it doesn't apply to the context");
    }

    @Test
    void setSeverity() {
        System.out.println(issuesRpcService.setSeverity("AU-Tpxb--iU5OvuD2FLy", "INFO"));
    }

    @Test
    void set_tags() {
        System.out.println(issuesRpcService.setTags("AU-Tpxb--iU5OvuD2FLy", "security"));
    }

    @Test
    void getTags() {
        System.out.println(issuesRpcService.getTags("my_project", "20", "misra"));
    }


    public static class FastJsonDecoder implements Decoder {

        @Override
        public Object decode(Response response, Type type) throws IOException, DecodeException, FeignException {
            StringBuilder content = new StringBuilder();
            BufferedReader reader = new BufferedReader(response.body().asReader(Util.UTF_8));
            String line = null;
            while ((line = reader.readLine()) != null) {
                content.append(line);
            }
            System.out.println(content);
            try {
                return JSONObject.parseObject(content.toString(), Class.forName(type.getTypeName()));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}