package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WebservicesRpcServiceTest {

    private WebservicesRpcService webservicesRpcService;

    @BeforeEach
    void setUp() {
        webservicesRpcService = ApiBilderFactory.getApiBuilder().buildApi(WebservicesRpcService.class);
    }


    @Test
    void getWebServiceList() {
        System.out.println(webservicesRpcService.getWebServiceList(true));
    }

    @Test
    void getResponseExample() {
        System.out.println(webservicesRpcService.getResponseExample("search", "api/issues"));
    }
}