package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ServerRpcServiceTest {
    private ServerRpcService serverRpcService;

    @BeforeEach
    void setUp() {
        serverRpcService = ApiBilderFactory.getApiBuilder().buildApi(ServerRpcService.class);
    }

    @Test
    void getServerVersion() {
        System.out.println(serverRpcService.getServerVersion());
    }
}