package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class ProjectsRpcServiceTest {

    private ProjectsRpcService projectsRpcService;

    @BeforeEach
    void setUp() {
        projectsRpcService = ApiBilderFactory.getApiBuilder().buildApi(ProjectsRpcService.class);
    }

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    String date = sdf.format(new Date());

    @Test
    void deleteProjects() {
        projectsRpcService.deleteProjects(date,false,"aa",null,null);
    }

    @Test
    void createProject() {
        System.out.println(projectsRpcService.createProject("heihei", "gaga", "public"));
    }

    @Test
    void deleteProject() {
        projectsRpcService.deleteProject("gaga");
    }

    @Test
    void searchProjects() {
        System.out.println(projectsRpcService.searchProjects(date, false, 1, "gaga", 12, "heihei", "TRK"));
    }

    @Test
    void updateProjectKey() {
        projectsRpcService.updateProjectKey("heihei","gaga");
    }

    @Test
    void updateVisibility() {
        projectsRpcService.updateVisibility("gaga","private");
    }
}