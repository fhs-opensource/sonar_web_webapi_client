package com.xhb.sonar.client.api;

import com.xhb.sonar.client.builder.ApiBuilder;

/**
 * 用来获取ApiBuilder
 */
public class ApiBilderFactory {
    /**
     * 获取ApiBuilder
     * @return
     */
    public static ApiBuilder getApiBuilder(){
        return new ApiBuilder("admin","admin","http://192.168.0.213:9000/");
    }
}
