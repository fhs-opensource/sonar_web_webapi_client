package com.xhb.sonar.client.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SettingsRpcServiceTest {
    private SettingsRpcService settingsRpcService;

    @BeforeEach
    void setUp() {
        settingsRpcService = ApiBilderFactory.getApiBuilder().buildApi(SettingsRpcService.class);
    }

    @Test
    void getDefinitionsList() {
        System.out.println(settingsRpcService.getDefinitionsList("gaga"));
    }

    @Test
    void removeSettingValue() {
        settingsRpcService.removeSettingValue("gaga","sonar.links.scm,sonar.debt.hoursInDay");
    }

    @Test
    void getSettingsValues() {
        System.out.println(settingsRpcService.getSettingsValues("gaga", "sonar.links.scm,sonar.debt.hoursInDay"));
    }
}