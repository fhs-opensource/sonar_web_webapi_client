package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.CheckCredentialsDTO;
import com.xhb.sonar.client.dto.ComponentsSearchResultDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

/**
 * 处理身份验证
 */
public interface AuthenticationRpcService {

    /**
     * 验证用户
     *
     * @param login
     * @param password
     * @return
     */
    @RequestLine("POST api/authentication/login")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    ComponentsSearchResultDTO login(
            @Param("login") String login,
            @Param("password") String password
    );

    /**
     * 注销用户
     */
    @RequestLine("POST api/authentication/logout")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void logout();


    /**
     * 检查凭证
     *
     * @return
     */
    @RequestLine("POST api/authentication/validate")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    CheckCredentialsDTO checkCredentials();
}
