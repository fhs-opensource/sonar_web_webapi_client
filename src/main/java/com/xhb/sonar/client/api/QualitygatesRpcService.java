package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.*;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface QualitygatesRpcService {


    /**
     * 复制qualitygates。
     * 需要“Administer Quality Gates”权限。
     *
     * @param id
     * @param name
     * @param organizationKey
     */
    @RequestLine("POST api/qualitygates/copy")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void copyQualityGate(
            @Param("id") String id,
            @Param("name") Boolean name,
            @Param("organization") String organizationKey
    );


    /**
     * 创建质量门。
     * 需要“管理质量门”权限。
     *
     * @param name            要创建的质量门的名称
     * @param organizationKey 非必填 组织密钥。如果未提供任何组织，则使用默认组织。
     * @return
     */
    @RequestLine("POST api/qualitygates/create")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    CreateQualityGateDTO createQualityGate(
            @Param("name") String name,
            @Param("organization") String organizationKey
    );


    /**
     * 向qualitygates添加新条件。
     * 需要“admin qualitygates”权限。
     *
     * @param error             条件误差阈值
     * @param qualitygateId
     * @param conditionMetric   条件指标。
     *                          仅允许使用以下类型的指标：
     *                          INT
     *                          MILLISEC
     *                          RATING
     *                          WORK_DUR
     *                          FLOAT
     *                          PERCENT
     *                          LEVEL
     * @param conditionOperator 非必填 条件运算符：
     *                          LT =低于
     *                          GT =大于
     * @param organizationKey   非必填 组织密钥。如果未提供任何组织，则使用默认组织。
     * @return
     */
    @RequestLine("POST api/qualitygates/create_condition")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    CreateConditionForQualitygatesDTO createCondition(
            @Param("error") String error,
            @Param("gateId") String qualitygateId,
            @Param("metric") String conditionMetric,
            @Param("op") String conditionOperator,
            @Param("organization") String organizationKey
    );


    /**
     * 从qualitygates中删除条件。
     * 需要“admin qualitygates”权限。
     *
     * @param conditionID
     * @param organizationKey 非必填 组织密钥。如果未提供任何组织，则使用默认组织。
     */
    @RequestLine("POST api/qualitygates/delete_condition")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void deleteCondition(
            @Param("id") String conditionID,
            @Param("organization") String organizationKey
    );


    /**
     * 从质量门中删除项目的关联。
     * 需要以下权限之一：
     * “管理质量门”
     * 项目的“管理”权
     *
     * @param organizationKey 非必填 组织密钥。如果未提供任何组织，则使用默认组织。
     * @param projectKey      非必填
     */
    @RequestLine("POST api/qualitygates/deselect")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void removeProjectAssociationForQualitygate(
            @Param("organization") String organizationKey,
            @Param("projectKey") String projectKey
    );


    /**
     * 删除 qualitygates
     * 需要“admin qualitygates”权限。
     *
     * @param qualitygatesId
     * @param organizationKey
     */
    @RequestLine("POST api/qualitygates/destroy")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void deleteQualitygates(
            @Param("id") String qualitygatesId,
            @Param("organization") String organizationKey
    );


    /**
     * 获取项目的qualitygates
     * 需要以下权限之一：
     * “管理系统”
     * 对指定项目的“管理”权限
     * 在指定项目上“浏览”
     *
     * @param organizationKey 非必填     组织密钥。如果未提供任何组织，则使用默认组织。
     * @param projectKey
     * @return
     */
    @RequestLine("POST api/qualitygates/get_by_project")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    QualitygateOfProjectDTO getByProject(
            @Param("organization") String organizationKey,
            @Param("project") String projectKey
    );


    /**
     * 获取所有quality gates
     *
     * @param organizationKey 非必填 组织密钥。如果未提供任何组织，则使用默认组织。
     * @return
     */
    @RequestLine("GET api/qualitygates/list")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    GetQualitygatesDTO getQualitygates(@Param("organization") String organizationKey);

    /**
     * 获取项目或Compute Engine任务的质量门状态。
     * 必须提供“ analysisId”，“ projectId”或“ projectKey”
     * 返回的不同状态为：OK，WARN，ERROR，NONE。当没有质量门与分析相关联时，将返回NONE状态。
     * 如果找不到或不存在与任务关联的分析，则返回HTTP代码404。
     * 需要以下权限之一：
     * “管理系统”
     * 对指定项目的“管理”权限
     * 在指定项目上“浏览”
     *
     * @param analysisId  非必填
     * @param branchKey   非必填
     * @param projectId   非必填   不适用于分支或拉取请求
     * @param projectKey  非必填
     * @param pullRequest 非必填
     * @return
     */
    @RequestLine("GET api/qualitygates/project_status")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    QualitygatesStatusForProjectDTO getQualitygatesStatusForProject(
            @Param("analysisId") String analysisId,
            @Param("branch") String branchKey,
            @Param("projectId") String projectId,
            @Param("projectKey") String projectKey,
            @Param("pullRequest") String pullRequest
    );


    /**
     * 重命名qualitygates。
     * 需要“管理质量门”权限。
     *
     * @param qualitygatesId
     * @param qualitygatesName
     * @param organizationKey  非必填     组织密钥。如果未提供任何组织，则使用默认组织。
     */
    @RequestLine("POST api/qualitygates/rename")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void renameOfQualitygates(
            @Param("id") String qualitygatesId,
            @Param("name") String qualitygatesName,
            @Param("organization") String organizationKey
    );


    /**
     * 搜索与qualitygates相关（或不相关）的项目。
     * 仅返回当前用户的授权项目。
     *
     * @param gateId       qualitygatesId
     * @param organization 非必填 组织密钥。如果未提供任何组织，则使用默认组织。
     * @param pageNumber   非必填
     * @param pageSize     非必填
     * @param query        非必填 搜索包含此字符串的项目。如果设置了此参数，则“选择”设置为“全部”。
     * @param selected     非必填 根据值，仅显示选定项目（selected = selected），取消选定项目（selected = deselected）或所有具有选择状态的项目（selected = all）。
     * @return
     */
    @RequestLine("GET api/qualitygates/search")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    SearchProjeectForQualitygatesDTO searchProjeectForQualitygates(
            @Param("gateId") String gateId,
            @Param("organization") String organization,
            @Param("page") String pageNumber,
            @Param("pageSize") String pageSize,
            @Param("query") String query,
            @Param("selected") String selected
    );


    /**
     * 将项目与质量门相关联。
     * 必须提供“ projectId”或“ projectKey”。
     * 从6.1开始不推荐使用项目ID作为数字值。请使用类似于“ AU-TpxcA-iU5OvuD2FLz”的ID。
     * 需要“管理质量门”权限。
     *
     * @param gateId       qualitygatesId
     * @param organization 非必填 组织密钥。如果未提供任何组织，则使用默认组织。
     * @param projectId    非必填     专案编号。从6.1开始不推荐使用项目ID作为数字值
     * @param projectKey   非必填
     */
    @RequestLine("POST api/qualitygates/select")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void associateProjectToQualitygate(
            @Param("gateId") String gateId,
            @Param("organization") String organization,
            @Param("projectId") String projectId,
            @Param("projectKey") String projectKey
    );


    /**
     * 将qualitygates设置为默认qualitygates。
     * 需要“admin qualitygates”权限。
     *
     * @param defaultId    qualitygates的ID设置为默认值
     * @param organization 非必填 组织密钥。如果未提供任何组织，则使用默认组织。
     */
    @RequestLine("POST api/qualitygates/set_as_default")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void setQualitygatesDefault(
            @Param("id") String defaultId,
            @Param("organization") String organization
    );


    /**
     * 显示qualitygates的详细信息
     *
     * @param qualitygatesId
     * @param qualitygatesName
     * @param organizationKey  非必填     组织密钥。如果未提供任何组织，则使用默认组织。
     */
    @RequestLine("GET api/qualitygates/show")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    QualitygatesDetailsDTO getQualitygatesDetails(
            @Param("id") String qualitygatesId,
            @Param("name") String qualitygatesName,
            @Param("organization") String organizationKey
    );


    /**
     * 更新附加到质量门的条件。
     * 需要“管理质量门”权限。
     *
     * @param error             条件误差阈值
     * @param conditionId
     * @param conditionMetric   条件指标。
     *                          仅允许使用以下类型的指标：
     *                          INT
     *                          MILLISEC
     *                          RATING
     *                          WORK_DUR
     *                          FLOAT
     *                          PERCENT
     *                          LEVEL
     * @param conditionOperator 非必填 条件运算符：
     *                          LT =低于
     *                          GT =大于
     * @param organizationKey   非必填 组织密钥。如果未提供任何组织，则使用默认组织。
     * @return
     */
    @RequestLine("GET api/qualitygates/update_condition")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void updateCondition(
            @Param("error") String error,
            @Param("id") String conditionId,
            @Param("metric") String conditionMetric,
            @Param("op") String conditionOperator,
            @Param("organization") String organizationKey
    );

}
