package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.CreateTemplateDTO;
import com.xhb.sonar.client.dto.UpdateTemplateDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

/**
 * 管理权限模板，以及在全局和项目级别上授予和撤消权限。
 */
public interface PermissionsRpcService {


    /**
     * 向组添加权限。
     * 此服务默认为全局权限，但可以通过提供项目ID或项目密钥将其限制为项目权限。
     * 必须提供组名或组ID。
     * 需要以下权限之一：
     * “管理系统”
     * 对指定项目的“管理”权限
     *
     * @param groupId
     * @param groupName  非必填 组名或“任何人”（不区分大小写）
     * @param permission 允许全局权限的可能值：admin，profileadmin，gateadmin，scan，provisioning
     *                   项目权限admin，codeviewer，issueadmin，securityhotspotadmin，scan，user的可能值
     * @param projectId  非必填
     * @param projectKey 非必填
     */
    @RequestLine("POST api/permissions/add_group")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void addGroup(
            @Param("groupId") String groupId,
            @Param("groupName") String groupName,
            @Param("permission") String permission,
            @Param("projectId") String projectId,
            @Param("projectKey") String projectKey
    );

    /**
     * 将组添加到权限模板。
     * 必须提供组ID或组名。
     * 需要以下权限：“管理系统”。
     *
     * @param groupId      非必填
     * @param groupName    非必填 组名或“任何人”（不区分大小写）
     * @param permission   允许
     *                     项目权限admin，codeviewer，issueadmin，securityhotspotadmin，scan，user的可能值
     * @param templateId   非必填
     * @param templateName 非必填
     */
    @RequestLine("POST api/permissions/add_group_to_template")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void addGroupToTemplate(
            @Param("groupId") String groupId,
            @Param("groupName") String groupName,
            @Param("permission") String permission,
            @Param("templateId") String templateId,
            @Param("templateName") String templateName
    );

    /**
     * 将项目创建者添加到权限模板。
     * 需要以下权限：“管理系统”。
     *
     * @param permission   项目权限admin，codeviewer，issueadmin，securityhotspotadmin，scan，user的可能值
     * @param templateId   非必填
     * @param templateName 非必填
     */
    @RequestLine("POST api/permissions/add_project_creator_to_template")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void addProjectCreatorToTemplate(
            @Param("permission") String permission,
            @Param("templateId") String templateId,
            @Param("templateName") String templateName
    );

    /**
     * 向用户添加权限。
     * 此服务默认为全局权限，但可以通过提供项目ID或项目密钥将其限制为项目权限。
     * 需要以下权限之一：
     * “管理系统”
     * 对指定项目的“管理”权限
     *
     * @param login
     * @param permission 全局权限的可能值：admin，profileadmin，gateadmin，scan，provisioning
     *                   项目权限admin，codeviewer，issueadmin，securityhotspotadmin，scan，user的可能值
     * @param projectId  非必填
     * @param projectKey 非必填
     */
    @RequestLine("POST api/permissions/add_user")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void addUser(
            @Param("login") String login,
            @Param("permission") String permission,
            @Param("projectId") String projectId,
            @Param("projectKey") String projectKey
    );

    /**
     * 将用户添加到权限模板。
     * 需要以下权限：“管理系统”。
     *
     * @param login
     * @param permission   项目权限admin，codeviewer，issueadmin，securityhotspotadmin，scan，user的可能值
     * @param templateId   非必填
     * @param templateName 非必填
     */
    @RequestLine("POST api/permissions/add_user_to_template")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void addUserToTemplate(
            @Param("login") String login,
            @Param("permission") String permission,
            @Param("templateId") String templateId,
            @Param("templateName") String templateName
    );

    /**
     * 将权限模板应用于一个项目。
     * 必须提供项目ID或项目密钥。
     * 必须提供模板ID或名称。
     * 需要以下权限：“管理系统”。
     *
     * @param projectId    非必填
     * @param projectKey   非必填
     * @param templateId   非必填
     * @param templateName 非必填
     */
    @RequestLine("POST api/permissions/apply_template")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void applyTemplate(
            @Param("projectId") String projectId,
            @Param("projectKey") String projectKey,
            @Param("templateId") String templateId,
            @Param("templateName") String templateName
    );

    /**
     * 将权限模板应用于多个项目。
     * 必须提供模板ID或名称。
     * 需要以下权限：“管理系统”。
     *
     * @param analyzedBefore    非必须 过滤最后分析时间早于给定日期（不包括）的项目。
     *                          可以提供日期（服务器时区）或日期时间。
     * @param onProvisionedOnly 非必须 筛选配置的项目
     * @param projects          非必须 以逗号分隔的项目键清单
     * @param q                 非必须 将搜索限制为：
     *                          包含提供的字符串的项目名称
     *                          与提供的字符串完全相同的项目键
     * @param qualifiers        非必须 逗号分隔的组件限定符列表。使用指定的限定词过滤结果。可能的值为：
     *                          TRK-项目
     * @param templateId
     * @param templateName
     */
    @RequestLine("POST api/permissions/bulk_apply_template")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void bulkApplyTemplate(
            @Param("analyzedBefore") String analyzedBefore,
            @Param("onProvisionedOnly") String onProvisionedOnly,
            @Param("projects") String projects,
            @Param("q") String q,
            @Param("qualifiers") String qualifiers,
            @Param("templateId") String templateId,
            @Param("templateName") String templateName
    );

    /**
     * 创建一个权限模板。
     * 需要以下权限：“管理系统”。
     *
     * @param description       非必填 描述
     * @param name
     * @param projectKeyPattern 非必填 项目密钥模式。必须是有效的Java正则表达式
     * @return
     */
    @RequestLine("POST api/permissions/create_template")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    CreateTemplateDTO createTemplate(
            @Param("description") String description,
            @Param("name") String name,
            @Param("projectKeyPattern") String projectKeyPattern

    );


    /**
     * 删除权限模板。
     * 需要以下权限：“管理系统”。
     *
     * @param templateId   非必填
     * @param templateName 非必填
     */
    @RequestLine("POST api/permissions/delete_template")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void deleteTemplate(
            @Param("templateId") String templateId,
            @Param("templateName") String templateName
    );


    /**
     * 从组中删除权限。
     * 此服务默认为全局权限，但可以通过提供项目ID或项目密钥将其限制为项目权限。
     * 必须提供组ID或组名，不能同时提供两者。
     * 需要以下权限之一：
     * “管理系统”
     * 对指定项目的“管理”权限
     *
     * @param groupId    非必填
     * @param groupName  非必填 组名或“任何人”（不区分大小写）
     * @param permission 全局权限的可能值：admin，profileadmin，gateadmin，scan，provisioning
     *                   项目权限admin，codeviewer，issueadmin，securityhotspotadmin，scan，user的可能值
     * @param projectId  非必填
     * @param projectKey 非必填
     */
    @RequestLine("POST api/permissions/remove_group")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void removeGroup(
            @Param("groupId") String groupId,
            @Param("groupName") String groupName,
            @Param("permission") String permission,
            @Param("projectId") String projectId,
            @Param("projectKey") String projectKey
    );


    /**
     * 从权限模板中删除组。
     * 必须提供组ID或组名。
     * 需要以下权限：“管理系统”。
     *
     * @param groupId      非必填
     * @param groupName    组名或“任何人”（不区分大小写）
     * @param permission   非必填 项目权限admin，codeviewer，issueadmin，securityhotspotadmin，scan，user的可能值
     * @param templateId   非必填
     * @param templateName 非必填
     */
    @RequestLine("POST api/permissions/remove_group_from_template")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void removeGroupFromTemplate(
            @Param("groupId") String groupId,
            @Param("groupName") String groupName,
            @Param("permission") String permission,
            @Param("templateId") String templateId,
            @Param("templateName") String templateName
    );


    /**
     * 从权限模板中删除项目创建者。
     * 需要以下权限：“管理系统”。
     *
     * @param permission   非必填 项目权限admin，codeviewer，issueadmin，securityhotspotadmin，scan，user的可能值
     * @param templateId   非必填
     * @param templateName 非必填
     */
    @RequestLine("POST api/permissions/remove_project_creator_from_template")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void removeProjectCreatorFromTemplate(
            @Param("permission") String permission,
            @Param("templateId") String templateId,
            @Param("templateName") String templateName
    );


    /**
     * 删除用户的权限。
     * 此服务默认为全局权限，但可以通过提供项目ID或项目密钥将其限制为项目权限。
     * 需要以下权限之一：
     * “管理系统”
     * 对指定项目的“管理”权限
     *
     * @param login
     * @param permission 全局权限的可能值：admin，profileadmin，gateadmin，scan，provisioning
     *                   项目权限admin，codeviewer，issueadmin，securityhotspotadmin，scan，user的可能值
     * @param projectId  非必填
     * @param projectKey 非必填
     */
    @RequestLine("POST api/permissions/remove_user")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void removeUser(
            @Param("login") String login,
            @Param("permission") String permission,
            @Param("projectId") String projectId,
            @Param("projectKey") String projectKey
    );


    /**
     * 从权限模板中删除用户。
     * 需要以下权限：“管理系统”。
     *
     * @param login
     * @param permission    项目权限admin，codeviewer，issueadmin，securityhotspotadmin，scan，user的可能值
     * @param templateId    非必填
     * @param templateName  非必填
     */
    @RequestLine("POST api/permissions/remove_User_from_template")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void removeUserFromTemplate(
            @Param("login") String login,
            @Param("permission") String permission,
            @Param("templateId") String templateId,
            @Param("templateName") String templateName
    );


    /**
     * 列出权限模板。
     * 需要以下权限：“管理系统”。
     *
     * @param permissionTemplateNames 非必填 将搜索限制为包含提供的字符串的权限模板名称
     */
    @RequestLine("POST api/permissions/search_templates")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void searchTemplates(@Param("q") String permissionTemplateNames);


    /**
     * 将权限模板设置为默认值。
     * 需要以下权限：“管理系统”
     *
     * @param qualifier    非必填 项目预选赛。使用指定的限定词过滤结果
     * @param templateId   非必填
     * @param templateName 非必填
     */
    @RequestLine("POST api/permissions/set_default_template")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void setDefaultTemplate(
            @Param("qualifier") String qualifier,
            @Param("templateId") String templateId,
            @Param("templateName") String templateName
    );

    /**
     * 更新权限模板。
     * 需要以下权限：“管理系统”。
     *
     * @param description       非必填 描述
     * @param id
     * @param name              非必填
     * @param projectKeyPattern 非必填 项目密钥模式。必须是有效的Java正则表达式
     * @return
     */
    @RequestLine("POST api/permissions/update_template")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    UpdateTemplateDTO updateTemplate(
            @Param("description") String description,
            @Param("id") String id,
            @Param("name") String name,
            @Param("projectKeyPattern") String projectKeyPattern
    );

}
