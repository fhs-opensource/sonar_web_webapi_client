package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.AuthenticatedFavoritesDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface FavoritesRpcService {
    /**
     * 将组件（项目，文件等）添加为已验证用户的收藏夹。
     * 限定词只能添加100个组件作为收藏。
     * 需要身份验证和以下许可：在指定组件的项目上“浏览”。
     *
     * @param componentKey 组件键。仅支持带有限定符TRK，VW，SVW，APP，FIL，UTS的组件
     */
    @RequestLine("POST api/favorites/add")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void addComment(@Param("component") String componentKey);


    /**
     * 删除已验证用户喜欢的组件（项目，目录，文件等）。
     * 需要认证。
     * @param componentKey
     */
    @RequestLine("POST api/favorites/remove")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void removeComponent(@Param("component") String componentKey);


    /**
     * 搜索经过身份验证的用户收藏夹。
     * 需要认证。
     * @param pageNumber    非必填
     * @param pageSize  非必填
     * @return
     */
    @RequestLine("POST api/favorites/search")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    AuthenticatedFavoritesDTO searchAuthenticatedFavorites(
            @Param("p") Integer pageNumber,
            @Param("ps") Integer pageSize
    );

}
