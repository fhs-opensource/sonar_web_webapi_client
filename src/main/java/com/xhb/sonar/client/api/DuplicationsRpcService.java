package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.DuplicationsDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface DuplicationsRpcService {

    /**
     * 获取副本。需要文件项目的浏览权限
     * @param fileKey   非必填
     * @return
     */
    @RequestLine("GET api/duplications/show")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    DuplicationsDTO showDuplications(@Param("key") String fileKey);

}
