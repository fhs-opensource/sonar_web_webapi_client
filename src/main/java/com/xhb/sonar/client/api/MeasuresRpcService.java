package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.ComponentTreeDTO;
import com.xhb.sonar.client.dto.MeasuresComponentResult;
import com.xhb.sonar.client.dto.SearchHistoryDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

/**
 * 获取指标数据
 */
public interface MeasuresRpcService {

    /**
     * 返回具有指定度量的组件。必须提供componentId或component参数。
     *
     * @param additionalFields 非必填metrics periods
     * @param component        非必填
     * @param metricKeys       必填逗号分隔的度量标准键列表
     *                         ncloc  --没有注释的行数
     *                         complexity -- 圈复杂度
     *                         violations -- 异味
     *                         new_technical_debt --新增技术债
     *                         blocker_violations -- 不清楚
     *                         bugs - bug
     *                         classes 多少个类
     *                         code_smells 有气味的代码
     *                         comment_lines 注释行数
     *                         comment_lines_density 之前是ncloc 后面补充的注释行数
     *                         comment_lines_data - 又一个注释行数
     *                         class_complexity 类平均复杂度
     *                         new_branch_coverage 新增变更代码条件覆盖率--不懂
     *                         confirmed_issues 确认问题数量
     *                         coverage 测试覆盖率
     *                         new_coverage 新增的代码覆盖率
     *                         critical_violations 严重的问题
     *                         development_cost 开发成本
     *                         new_development_cost 新的代码开发成本
     *                         duplicated_blocks 重复代码
     *                         new_duplicated_blocks 新代码上的重复块
     *                         duplicated_files 重复文件
     *                         duplicated_lines 重复行
     *                         new_duplicated_lines 新代码重复行
     *                         new_duplicated_lines_density 语句平衡新代码重复行
     *                         duplications_data 重复详情
     *                         new_maintainability_rating 代码可维护等级
     *                         minor_violations 小问题
     *                         ncloc_data 没注释的数据
     *                         new_blocker_violations 新的阻断问题
     *                         new_bugs 新bug
     *                         reopened_issues 重新打开的问题
     *                         security_rating 安全等级
     *                         skipped_tests 调过单元测试数
     *                         new_sqale_debt_ratio 技术债务比例
     *                         <p>
     *                         复杂度
     *                         复杂度
     *                         复杂度/类
     *                         复杂度/文件
     *                         复杂度/方法
     *                         覆盖率
     *                         分支覆盖
     *                         集成测试的新分支覆盖
     *                         新代码的分支覆盖率
     *                         覆盖率
     *                         新集成测试覆盖
     *                         新覆盖率
     *                         集成测试分支覆盖
     *                         集成测试覆盖
     *                         集成测试覆盖行
     *                         集成测试未覆盖分支
     *                         集成测试未覆盖行
     *                         代码覆盖率
     *                         集成测试的新行覆盖
     *                         新代码覆盖率
     *                         代码行
     *                         集成测试的新行覆盖
     *                         覆盖的新代码
     *                         总体分支覆盖率
     *                         总体新分支覆盖率
     *                         总体覆盖率
     *                         总体新覆盖率
     *                         总体代码覆盖率
     *                         总体新代码覆盖率
     *                         总体覆盖的新行数
     *                         总体未覆盖分支
     *                         总体未覆盖的新分支
     *                         总体未覆盖代码
     *                         总体未覆盖新行数
     *                         单元测试忽略数
     *                         未覆盖分支
     *                         集成测试未覆盖的新分支
     *                         未覆盖新分支
     *                         未覆盖的代码
     *                         集成测试未覆盖的行
     *                         未覆盖的新代码
     *                         单元测试持续时间
     *                         单元测试错误数
     *                         单元测试失败数
     *                         单元测试成功 (%)
     *                         单元测试数
     *                         文档
     *                         注释行
     *                         注释 (%)
     *                         公共API
     *                         公共注释的API (%)
     *                         公共未注释的API
     *                         重复
     *                         重复块
     *                         重复文件
     *                         重复行
     *                         重复行(%)
     *                         问题
     *                         阻断违规
     *                         确认问题
     *                         严重违规
     *                         误判问题
     *                         提示违规
     *                         违规
     *                         主要违规
     *                         次要违规
     *                         新阻断违规
     *                         新严重违规
     *                         新提示违规
     *                         新违规
     *                         新主要违规
     *                         新次要违规
     *                         开启问题
     *                         重开问题
     *                         不修复的问题
     *                         可维护性
     *                         新代码的技术债务
     *                         坏味道
     *                         达到可维护性A级所需的工作
     *                         新增坏味道
     *                         技术债务
     *                         技术债务比率
     *                         新代码技术债务比率
     *                         Management
     *                         Burned budget
     *                         Business value
     *                         Team size
     *                         可靠性
     *                         Bugs
     *                         新增Bugs
     *                         可靠性修复工作
     *                         新代码的可靠性修复工作
     *                         安全性
     *                         新增漏洞
     *                         安全修复工作
     *                         新代码的安全修复工作
     *                         漏洞
     *                         大小
     *                         类
     *                         目录
     *                         文件
     *                         方法
     *                         生成的行数
     *                         生成的代码行数
     *                         行数
     *                         代码行数
     *                         项目
     *                         语句
     * @return
     */
    @RequestLine("GET api/measures/component")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    MeasuresComponentResult componentResult(
            @Param("additionalFields") String additionalFields,
            @Param("branch") String branch,
            @Param("component") String component,
            @Param("metricKeys") String metricKeys,
            @Param("pullRequest") String pullRequest
    );


    /**
     * 根据选择的策略以指定的方式浏览各个组件。必须提供baseComponentId或component参数。
     * 需要以下权限：在指定项目上“浏览”。
     * 当使用q参数限制搜索时，不返回目录。
     *
     * @param additionalFields 非必填  可以在响应中返回的其他字段的逗号分隔列表。
     * @param ascendingSort    非必填  升序排序
     * @param componentKey     非必填  组件键。搜索基于此组件。
     * @param metricKeys       逗号分隔的度量标准键列表。不允许使用DISTRIB，DATA类型。
     * @param metricPeriodSort 非必填  是否按泄漏时间对措施进行排序？ “ s”参数必须包含“ metricPeriod”值。
     * @param metricSort       非必填  公制键排序依据。 “ s”参数必须包含“ metric”或“ metricPeriod”值。它必须是“ metricKeys”参数的一部分
     * @param metricSortFilter 非必填  过滤组件。排序必须基于指标。可能的值为：
     *                         全部：返回所有组件
     *                         withMeasuresOnly：筛选出对已排序度量没有度量的组件
     * @param pageNumber       非必填  从1开始的页码
     * @param pageSize         非必填  页面大小。必须大于0且小于或等于500
     * @param limitSearch      非必填  将搜索限制为：
     *                         包含提供的字符串的组件名称
     *                         与提供的字符串完全相同的组件键
     * @param qualifiers       非必填  逗号分隔的组件限定符列表。使用指定的限定词过滤结果。可能的值为：
     *                         BRC-子项目
     *                         DIR-目录
     *                         FIL-文件
     *                         TRK-项目
     *                         UTS-测试文件
     * @param sortFields       非必填  以逗号分隔的排序字段列表
     * @param strategy         非必填  搜索基本组件后代的策略：
     *                         children：返回基础组件的子组件。孙子组件不返回
     *                         all：返回基本组件的所有后代组件。孙子返回了。
     *                         leaves：返回所有没有其他子代的后代组件（通常是文件）。它们是组件树的叶子。
     * @return
     */
    @RequestLine("GET api/measures/component_tree")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    ComponentTreeDTO getComponentTree(
            @Param("additionalFields") String additionalFields,
            @Param("asc") Boolean ascendingSort,
            @Param("component") String componentKey,
            @Param("metricKeys") String metricKeys,
            @Param("metricPeriodSort") String metricPeriodSort,
            @Param("metricSort") String metricSort,
            @Param("metricSortFilter") String metricSortFilter,
            @Param("p") Integer pageNumber,
            @Param("ps") Integer pageSize,
            @Param("q") String limitSearch,
            @Param("qualifiers") String qualifiers,
            @Param("s") String sortFields,
            @Param("strategy") String strategy
    );

    /**
     * 搜索可测量组件的历史记录。
     * 措施按时间顺序排列。
     * 分页适用于每个指标的度量数量。
     * 需要以下权限：在指定组件上“浏览”
     *
     * @param componentKey
     * @param createdAfterDate  非必填 过滤在给定日期（含）之后创建的度量。
     *                          可以提供日期（服务器时区）或日期时间
     * @param metrics           逗号分隔的度量标准键列表
     * @param pageNumber        非必填 从1开始的页码
     * @param pageSize          非必填 页面大小。必须大于0且小于或等于1000
     * @param createdBeforeTime 非必填 过滤在给定日期（包括该日期）之前创建的度量。
     *                          可以提供日期（服务器时区）或日期时间
     * @return
     */
    @RequestLine("GET api/measures/search_history")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    SearchHistoryDTO getSearchHistory(
            @Param("component") String componentKey,
            @Param("from") String createdAfterDate,
            @Param("metrics") String metrics,
            @Param("p") Integer pageNumber,
            @Param("ps") Integer pageSize,
            @Param("to") Integer createdBeforeTime
    );
}
