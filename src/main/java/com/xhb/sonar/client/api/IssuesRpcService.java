package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.*;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface IssuesRpcService {
    /**
     * 添加注释
     *
     * @param issueKey issueKey
     * @param text     评论文字
     * @return 结果
     */
    @RequestLine("POST /api/issues/add_comment")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    AddCommentResultDTO addComment(@Param("issue") String issueKey, @Param("text") String text);

    /**
     * 搜索问题。
     * 最多可以同时提供以下参数之一：componentKeys和componentUuids。
     * 需要对指定项目的“浏览”权限。
     *
     * @param additionalFields    非必填   以逗号分隔的要返回的可选字段列表。行动计划已在5.5中删除，未在响应中返回。
     * @param asc                 非必填   升序排序
     * @param assigned            非必填   检索分配或未分配的问题
     * @param assignees           非必填   以逗号分隔的受让人登录列表。值“ __me__”可用作执行请求的用户的占位符
     * @param author              非必填   SCM帐户。要设置多个值，必须为每个值调用一次该参数。
     * @param branch              非必填   分支 key
     * @param componentKeys       非必填   以逗号分隔的组件键列表。检索与组件（及其所有后代）的特定列表相关的问题。组件可以是项目组合，项目，模块，目录或文件。
     * @param createdAfter        非必填   检索在给定日期（含）之后创建的问题。
     *                            可以提供日期（服务器时区）或日期时间。
     *                            如果设置了此参数，则不能设置createdSince
     * @param createdAt           非必填   检索特定分析期间创建的问题的日期时间
     * @param createdBefore       非必填   检索在给定日期（包括该日期）之前创建的问题。
     *                            可以提供日期（服务器时区）或日期时间。
     * @param createdInLast       非必填   检索在当前时间（不包括）之前的时间段内创建的问题。可接受的单位是年的“ y”，月的“ m”，周的“ w”和天的“ d”。如果设置了此参数，则不得设置createdAfter
     * @param cwe                 非必填   逗号分隔的CWE标识符列表。使用“未知”选择与任何CWE无关的问题。
     * @param directories         非必填   检索与特定目录列表（目录路径的逗号分隔列表）相关的问题。仅当选择了模块时，此参数才有意义。此参数主要由“问题”页面使用，请首选使用componentKeys参数。
     * @param facets              非必填   要计算的构面的逗号分隔列表。默认情况下，不计算任何构面。
     * @param fileUuids           非必填   检索与特定文件列表（文件ID的逗号分隔列表）相关的问题。此参数主要由“问题”页面使用，请首选使用componentKeys参数。
     * @param issues              非必填   以逗号分隔的发行密钥列表
     * @param languages           非必填   以逗号分隔的语言列表。自4.4起可用
     * @param onComponentOnly     非必填   仅返回组件级别的问题，而不返回组件的后代（模块，目录，文件等）。仅当设置了componentKeys或componentUuids时，才考虑此参数。
     * @param organization        非必填   organization key
     * @param owaspTop10          非必填   OWASP十大小写类别的列表，以逗号分隔。
     * @param p                   非必填   从1开始的页码
     * @param projects            非必填   检索与特定项目列表（项目密钥的逗号分隔列表）相关的问题。此参数主要由“问题”页面使用，请首选使用componentKeys参数。如果设置了此参数，则不得设置projectUuids。
     * @param ps                  非必填   页面大小。必须大于0且小于或等于500
     * @param pullRequest         非必填   Pull request id
     * @param resolutions         非必填   逗号分隔的解决方案列表
     * @param resolved            非必填   匹配已解决或未解决的问题
     * @param rules               非必填   逗号分隔的编码规则键列表。格式为<存储库>：<规则>
     * @param s                   非必填   排序栏位
     * @param sansTop25           非必填   以逗号分隔的SANS前25个类别列表。
     * @param severities          非必填   以逗号分隔的严重等级列表
     * @param sinceLeakPeriod     非必填   检索自泄漏时段以来创建的问题。
     *                            如果将此参数设置为真值，则不得设置createdAfter，并且必须提供一个组件ID或键。
     * @param sonarsourceSecurity 非必填   以逗号分隔的SonarSource安全类别列表。使用“其他”选择与任何类别都不相关的问题
     * @param statuses            非必填   逗号分隔的状态列表
     * @param tags                非必填   逗号分隔的标签列表。
     * @param types               非必填   以逗号分隔的类型列表。
     * @return
     */
    @RequestLine("GET /api/issues/search")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    IssueSearchResultDTO issueSearch(
            @Param("additionalFields") String additionalFields,
            @Param("asc") String asc,
            @Param("assigned") String assigned,
            @Param("assignees") String assignees,
            @Param("author") String author,

            @Param("branch") String branch,

            @Param("componentKeys") String componentKeys,
            @Param("createdAfter") String createdAfter,
            @Param("createdAt") String createdAt,
            @Param("createdBefore") String createdBefore,
            @Param("createdInLast") String createdInLast,
            @Param("cwe") String cwe,

            @Param("directories") String directories,

            @Param("facets") String facets,

            @Param("fileUuids") String fileUuids,

            @Param("issues") String issues,
            @Param("languages") String languages,
            @Param("onComponentOnly") String onComponentOnly,

            @Param("organization") String organization,

            @Param("owaspTop10") String owaspTop10,
            @Param("p") Integer p,

            @Param("projects") String projects,

            @Param("ps") Integer ps,

            @Param("pullRequest") String pullRequest,

            @Param("resolutions") String resolutions,
            @Param("resolved") String resolved,
            @Param("rules") String rules,
            @Param("s") String s,
            @Param("sansTop25") String sansTop25,
            @Param("severities") String severities,
            @Param("sinceLeakPeriod") String sinceLeakPeriod,
            @Param("sonarsourceSecurity") String sonarsourceSecurity,
            @Param("statuses") String statuses,
            @Param("tags") String tags,
            @Param("types") String types
    );


    /**
     * 更改问题的类型，例如从 “ 代码气味 ” 更改为 “ 错误 ”
     *
     * @param issueKey
     * @param type     新类型
     * @return
     */
    @RequestLine("POST /api/issues/set_type")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    JsonRootDTO setType(@Param("issue") String issueKey, @Param("type") String type);


    /**
     * 分配/取消分配 issue。需要项目的身份验证和浏览权限
     *
     * @param assignee 非必填 受让人的登录。如果未设置，它将取消分配问题。使用“ _me”分配给当前用户
     * @param issue
     * @return
     */
    @RequestLine("POST /api/issues/assign")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    AssignDTO assign(@Param("assignee") String assignee, @Param("issue") String issue);

    /**
     * 搜索与给定查询匹配的SCM帐户。
     * 需要认证。
     *
     * @param project 非必填   Project key
     * @param ps      非必填 页面大小。必须大于0且小于或等于100
     * @param q       非必填 将搜索限制为包含提供的字符串的作者。
     * @return
     */
    @RequestLine("GET /api/issues/authors")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    SCMAccountsDTO authors(@Param("project") String project, @Param("ps") String ps, @Param("q") String q);


    /**
     * 在问题上进行批量更改。
     * 需要认证。
     *
     * @param addTags           非必填 添加标签
     * @param assign            非必填 要将问题列表分配给特定用户（登录），或取消分配所有问题
     * @param comment           非必填 向问题列表添加评论
     * @param doTransition      非必填 过渡
     * @param issues            以逗号分隔的发行密钥列表
     * @param removeTags        非必填 移除的标签
     * @param sendNotifications 非必填
     * @param setSeverity       非必填 更改问题列表的严重性
     * @param set_type          非必填 更改问题列表的类型
     * @return
     */
    @RequestLine("POST /api/issues/bulk_change")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    BulkChangeDTO bulkChange(@Param("add_tags") String addTags,
                             @Param("assign") String assign,
                             @Param("comment") String comment,
                             @Param("do_transition") String doTransition,
                             @Param("issues") String issues,
                             @Param("remove_tags") String removeTags,
                             @Param("sendNotifications") String sendNotifications,
                             @Param("set_severity") String setSeverity,
                             @Param("set_type") String set_type
    );


    /**
     * 显示问题的变更日志。
     * 在指定问题的项目上需要“浏览”权限。
     *
     * @param issues
     * @return
     */
    @RequestLine("GET /api/issues/changelog")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    IssuseChangelogDTO changelog(@Param("issue") String issues);

    /**
     * 删除评论。
     * 需要身份验证并具有以下权限：在指定问题的项目上“浏览”。
     *
     * @param comment
     * @return
     */
    @RequestLine("POST /api/issues/delete_comment")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    DeleteCommentDTO deleteCommentBeanDTO(@Param("comment") String comment);

    /**
     * 对问题进行工作流过渡。需要项目的身份验证和浏览权限。
     * 转换“ wontfix”和“ falsepositive”需要权限“管理问题”。
     * 涉及安全热点的过渡需要权限“管理安全热点”。
     *
     * @param issues
     * @param transition
     * @return
     */
    @RequestLine("POST /api/issues/do_transition")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    DoTransitionDTO doTransitionBeanDTO(@Param("issue") String issues, @Param("transition") String transition);

    /**
     * 编辑评论。
     * 需要身份验证并具有以下权限：在指定问题的项目上“浏览”。
     *
     * @param comment
     * @param text
     * @return
     */
    @RequestLine("POST /api/issues/edit_comment")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    EditCommentDTO editComment(@Param("comment") String comment, @Param("text") String text);

    /**
     * 更改严重性。
     * 需要以下权限：
     * “身份验证”
     * 特定发行项目的“浏览”权限
     * 指定问题项目的“管理问题”权利
     *
     * @param issues
     * @param severity 新的严重程度
     * @return
     */
    @RequestLine("POST /api/issues/set_severity")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    SetSeverityDTO setSeverity(@Param("issue") String issues, @Param("severity") String severity);

    /**
     * 在问题上设置标签。
     * 需要项目的身份验证和浏览权限
     *
     * @param issues
     * @param tags   非必填 逗号分隔的标签列表。如果参数为空或未设置，则将删除所有标签。
     * @return
     */
    @RequestLine("POST /api/issues/set_tags")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    SetTagsDTO setTags(@Param("issue") String issues, @Param("tags") String tags);

    /**
     * 列出与给定查询匹配的标签
     *
     * @param project
     * @param ps      非必填 页面大小。必须大于0且小于或等于100
     * @param q       非必填 将搜索限制为包含提供的字符串的标签。
     * @return
     */
    @RequestLine("GET /api/issues/tags")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    TagsDTO getTags(@Param("project") String project, @Param("ps") String ps, @Param("q") String q);
}
