package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.DataBaseStatusDTO;
import com.xhb.sonar.client.dto.HealthStatusDTO;
import com.xhb.sonar.client.dto.SonarQubeStatusDTO;
import com.xhb.sonar.client.dto.UpgradesSystemDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface SystemRpcService {

    /**
     * 临时更改日志级别。新级别不是永久性的，并且在重新启动服务器时会丢失。需要系统管理权限。
     *
     * @param level 新的level。注意：调试甚至更多的跟踪,可能会影响性能。
     */
    @RequestLine("POST api/system/change_log_level")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void changeLogLevel(@Param("level") String level);

    /**
     * 显示SonarQube的数据库迁移状态。
     * 状态值是：
     * NO_MIGRATION：DB是最新版本的SonarQube。
     * NOT_SUPPORTED：嵌入式数据库不支持迁移。
     * MIGRATION_RUNNING：数据库迁移正在进行中。
     * MIGRATION_SUCCEEDED：数据库迁移已运行且已成功。
     * MIGRATION_FAILED：数据库迁移已运行且失败。必须重新启动SonarQube才能重试数据库迁移（可选地，从备份还原数据库之后）。
     * MIGRATION_REQUIRED：需要数据库迁移。
     *
     * @return
     */
    @RequestLine("GET api/system/db_migration_status")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    DataBaseStatusDTO getDbMigrationStatus();


    /**
     * 提供SonarQube的健康状况。
     * 需要“管理系统”权限或使用密码进行身份验证
     * <p>
     * GREEN：SonarQube完全可以运行
     * YELLOW：SonarQube可用，但是需要注意才能完全运行
     * RED：SonarQube无法运行
     *
     * @return
     */
    @RequestLine("GET api/system/health")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    HealthStatusDTO getHealthStatus();


    /**
     * 获取纯文本格式的系统日志。需要系统管理权限。
     *
     * @param process 非必填 从中获取日志的进程
     * @return
     */
    @RequestLine("GET api/system/logs")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    String getSettingLogs(@Param("process") String process);


    /**
     * 迁移数据库以匹配SonarQube的当前版本。
     * 将POST请求发送到该URL将开始数据库迁移。强烈建议在调用此WS之前进行数据库备份。
     * 状态值是：
     * NO_MIGRATION：DB是最新版本的SonarQube。
     * NOT_SUPPORTED：嵌入式数据库不支持迁移。
     * MIGRATION_RUNNING：数据库迁移正在进行中。
     * MIGRATION_SUCCEEDED：数据库迁移已运行且已成功。
     * MIGRATION_FAILED：数据库迁移已运行且失败。必须重新启动SonarQube才能重试数据库迁移（可选地，从备份还原数据库之后）。
     * MIGRATION_REQUIRED：需要数据库迁移。
     *
     * @return
     */
    @RequestLine("POST api/system/migrate_db")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    DataBaseStatusDTO migrateDb();

    /**
     * 以纯文本形式回答“ pong”
     *
     * @return
     */
    @RequestLine("GET api/system/ping")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    String getPing();


    /**
     * 重新启动服务器。需要“管理系统”权限。对Web，Search和Compute Engine服务器进程执行完全重新启动。
     */
    @RequestLine("POST api/system/restart")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void restartServer();


    /**
     * 获取有关SonarQube的状态信息。
     * 状态：运行状态
     * <p>
     * 开始：SonarQube Web服务器已启动并正在提供某些Web服务（例如api / system / status），但初始化仍在进行中
     * UP：SonarQube实例已启动并正在运行
     * DOWN：SonarQube实例已启动但未运行，因为迁移失败（有关详细信息，请参阅WS / api / system / migrate_db）或某些其他原因（请检查日志）。
     * RESTARTING：SonarQube实例仍处于启动状态，但已请求重新启动（有关详细信息，请参阅WS / api / system / restart）。
     * DB_MIGRATION_NEEDED：需要数据库迁移。可以使用WS / api / system / migrate_db开始数据库迁移。
     * DB_MIGRATION_RUNNING：数据库迁移正在运行（有关详细信息，请参考WS / api / system / migrate_db）
     *
     * @return
     */
    @RequestLine("GET api/system/status")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    SonarQubeStatusDTO getSonarQubeStatus();

    /**
     * 列出SonarQube实例（如果有）的可用升级，并为每个实例列出不兼容的插件和需要升级的插件。
     * 从更新中心检索插件信息。响应中提供了上次刷新更新中心的日期和时间。
     *
     * @return
     */
    @RequestLine("GET api/system/upgrades")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    UpgradesSystemDTO getUpgradesSystem();

}
