package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.UserNotificationsDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface NotificationsRpcService {

    /**
     * 为经过身份验证的用户添加通知。
     * 需要以下权限之一：
     * 如果未提供登录名，则进行身份验证。如果提供了项目，则需要对指定项目具有“浏览”权限。
     * 系统管理（如果提供登录名）。如果提供了项目，则需要对指定项目具有“浏览”权限。
     *
     * @param channel 非必填  发送通知的渠道。例如，可以通过电子邮件发送通知。
     * @param login   非必填
     * @param project 非必填 Project key
     * @param type    通知类型。可能的值是：
     *                全局通知：CeReportTaskFailure，ChangesMyMyIssue，NewAlerts，SQ-MyNewIssues
     *                每个项目通知：CeReportTaskFailure，ChangesMyMyIssue，NewAlerts，NewFalsePositiveIssue，NewIssues，SQ-MyNewIssues
     */
    @RequestLine("POST api/notifications/add")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void addNotification(
            @Param("channel") String channel,
            @Param("login") String login,
            @Param("project") String project,
            @Param("type") String type
    );


    /**
     * 列出已认证用户的通知。
     * 需要以下权限之一：
     * 如果未提供登录名，则进行身份验证
     * 系统管理（如果提供登录名）
     *
     * @param login 非必填 User login
     * @return
     */
    @RequestLine("GET api/notifications/list")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    UserNotificationsDTO getUserNotifications(@Param("login") String login);


    /**
     * 删除已验证用户的通知。
     * 需要以下权限之一：
     * 如果未提供登录名，则进行身份验证
     * 系统管理（如果提供登录名）
     *
     * @param channel 非必填 发送通知的渠道。例如，可以通过电子邮件发送通知。
     * @param login   非必填 User login
     * @param project 非必填 Project key
     * @param type    通知类型。可能的值是：
     *                全局通知：CeReportTaskFailure，ChangesMyMyIssue，NewAlerts，SQ-MyNewIssues
     *                每个项目通知：CeReportTaskFailure，ChangesMyMyIssue，NewAlerts，NewFalsePositiveIssue，NewIssues，SQ-MyNewIssues
     */
    @RequestLine("POST api/notifications/remove")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void removeNotification(
            @Param("channel") String channel,
            @Param("login") String login,
            @Param("project") String project,
            @Param("type") String type
    );
}
