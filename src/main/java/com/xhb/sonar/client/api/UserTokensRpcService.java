package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.GenerateUserTokenDTO;
import com.xhb.sonar.client.dto.UserTokenSearchResultDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface UserTokensRpcService {

    /**
     * 生成用户访问令牌。
     * 请对令牌保密。它们可以对项目进行身份验证和分析。
     * 它需要管理权限才能指定“登录”并为另一个用户生成令牌。否则，将为当前用户生成令牌。
     *
     * @param login 非必填 用户登录。如果未设置，则为经过身份验证的用户生成令牌。
     * @param name  Token name
     * @return
     */
    @RequestLine("POST api/user_tokens/generate")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    GenerateUserTokenDTO generateUserToken(@Param("login") String login, @Param("name") String name);

    /**
     * 撤消用户访问令牌。
     * 它需要管理权限才能指定“登录”并撤消其他用户的令牌。否则，将撤消当前用户的令牌。
     *
     * @param login 非必填
     * @param name
     */
    @RequestLine("POST api/user_tokens/revoke")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void revoke(@Param("login") String login, @Param("name") String name);


    /**
     * 列出用户的访问令牌。
     * 该登录名必须存在且处于活动状态。
     * 字段“ lastConnectionDate”仅每小时更新一次，因此可能不准确，例如，当用户在不到一个小时的时间内多次使用令牌时。
     * @param login 非必填
     * @return
     */
    @RequestLine("POST api/user_tokens/search")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    UserTokenSearchResultDTO userTokenSearchResult(@Param("login") String login);


}
