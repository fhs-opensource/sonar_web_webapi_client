package com.xhb.sonar.client.api;

import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface ProjectsBadgesRpcService {


    /**
     * 生成用于SVG的项目度量标准的徽章。
     * 需要对指定项目的“浏览”权限。
     *
     * @param branchKey 非必填
     * @param metricKey
     * @param projectKey
     * @return
     */
    @RequestLine("GET api/project_badges/measure")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    String getProjectBadgesMeasure(
            @Param("branch") String branchKey,
            @Param("metric") String metricKey,
            @Param("project") String projectKey
    );


    /**
     * 为SVG生成项目质量门的徽章。
     * 需要对指定项目的“浏览”权限。
     * @param branchKey 非必填
     * @param projectKey
     * @return
     */
    @RequestLine("GET api/project_badges/quality_gate")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    String generateBadgeForQualityGate(
            @Param("branch") String branchKey,
            @Param("project") String projectKey
    );


}
