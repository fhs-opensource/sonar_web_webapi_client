package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.ComponentsSearchResultDTO;
import com.xhb.sonar.client.dto.ComputeByChosenStrategyDTO;
import com.xhb.sonar.client.dto.GetComputeDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

/**
 * 组件
 */
public interface ComponentsRpcService {
    /**
     * 搜索组件
     *
     * @param language   非必填 开发语言
     * @param p          非必填 第几页
     * @param ps         非必填 每页多少条
     * @param q          非必填 关键字
     * @param qualifiers 以逗号分隔的组件限定符列表。使用指定的限定符筛选结果，可能的值包括：
     *                   BRC - Sub-projects
     *                   DIR - Directories
     *                   FIL - Files
     *                   TRK - Projects
     *                   UTS - Test Files
     * @return
     */
    @RequestLine("GET api/components/search")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    ComponentsSearchResultDTO search(
            @Param("language") String language,
            @Param("p") Integer p,
            @Param("ps") Integer ps,
            @Param("q") String q,
            @Param("qualifiers") String qualifiers);

    /**
     * 返回一个组件（文件，目录，项目，视图…）及其祖先。祖先是从父项到根项目的顺序。必须提供'componentId'或'component'参数。
     * 需要以下权限：在指定组件的项目上“浏览”。
     *
     * @param componentKey 非必填
     * @return
     */
    @RequestLine("GET api/components/show")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    GetComputeDTO getCompute(
            @Param("component") String componentKey
    );


    /**
     * 根据所选策略浏览各个组件。必须提供componentId或component参数。
     * 需要以下权限：在指定项目上“浏览”。
     * 当使用q参数限制搜索时，不返回目录。
     *
     * @param asc          非必填 升序排序
     * @param componentKey 非必填 基本组件密钥。搜索基于此组件。
     * @param pageNumber   非必填 从1开始的页码
     * @param pageSize     非必填 页面大小。必须大于0且小于或等于500
     * @param limitSearch  非必填 将搜索限制为：
     *                     包含提供的字符串的组件名称
     *                     与提供的字符串完全相同的组件键
     * @param qualifiers   非必填 逗号分隔的组件限定符列表。使用指定的限定词过滤结果。可能的值为：
     *                     BRC-子项目
     *                     DIR-目录
     *                     FIL-文件
     *                     TRK-项目
     *                     UTS-测试文件
     * @param sortFieids   非必填 以逗号分隔的排序字段列表
     * @param strategy     非必填 Strategy to search for base component descendants:
     *                     children: return the children components of the base component. Grandchildren components are not returned
     *                     all: return all the descendants components of the base component. Grandchildren are returned.
     *                     leaves: return all the descendant components (files, in general) which don't have other children. They are the leaves of the component tree.
     * @return
     */
    @RequestLine("GET api/components/tree")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    ComputeByChosenStrategyDTO getComputeByChosenStrategy(
            @Param("asc") Boolean asc,
            @Param("component") String componentKey,
            @Param("p") Integer pageNumber,
            @Param("ps") Integer pageSize,
            @Param("q") String limitSearch,
            @Param("qualifiers") String qualifiers,
            @Param("s") String sortFieIds,
            @Param("strategy") String strategy
    );

}
