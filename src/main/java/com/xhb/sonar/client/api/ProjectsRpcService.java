package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.CreateProjectDTO;
import com.xhb.sonar.client.dto.SearchProjectDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface ProjectsRpcService {


    /**
     * 删除一个或多个项目。
     * 需要“管理系统”权限。
     * 在createdBefore，projects，projectIds（自6.4版弃用）和q之间，至少需要一个参数
     *
     * @param analyzedBefore    可选的        过滤最后分析时间早于给定日期（不包括）的项目。
     *                          可以提供日期（服务器时区）或日期时间。
     * @param onProvisionedOnly 可选的         筛选配置的项目
     * @param projectsKey       可选的 以逗号分隔的项目键清单
     * @param q                 可选的 限制于：
     *                          包含提供的字符串的组件名称
     *                          包含提供的字符串的组件键
     * @param qualifiers        可选的 逗号分隔的组件限定符列表。使用指定的限定词过滤结果
     */
    @RequestLine("POST api/projects/bulk_delete")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void deleteProjects(
            @Param("analyzedBefore") String analyzedBefore,
            @Param("onProvisionedOnly") Boolean onProvisionedOnly,
            @Param("projects") String projectsKey,
            @Param("q") String q,
            @Param("qualifiers") Integer qualifiers
    );


    /**
     * 创建一个项目。
     * 需要“创建项目”权限
     *
     * @param projectName 项目名称。如果名称长于500，则将其缩写。
     * @param projectKey
     * @param visibility  非必填 创建的项目对所有人还是仅对特定用户/组可见。
     *                    如果未指定可见性，则将使用组织的默认项目可见性。
     * @return
     */
    @RequestLine("POST api/projects/create")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    CreateProjectDTO createProject(
            @Param("name") String projectName,
            @Param("project") String projectKey,
            @Param("visibility") String visibility
    );


    /**
     * 删除一个项目。
     * 在项目上需要“管理系统”权限或“管理”权限。
     *
     * @param projectKey 非必填
     */
    @RequestLine("POST api/projects/delete")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void deleteProject(
            @Param("project") String projectKey
    );


    /**
     * 搜索项目或视图以对其进行管理。
     * 需要“系统管理员”权限
     *
     * @param analyzedBefore    非必填 过滤最后分析时间早于给定日期（不包括）的项目。
     *                          可以提供日期（服务器时区）或日期时间。
     * @param onProvisionedOnly 非必填 筛选配置的项目
     * @param PageNumber        非必填
     * @param projectKeys       非必填 以逗号分隔的项目键清单
     * @param pageSize          非必填 页面大小。必须大于0且小于或等于500
     * @param limitSearch       非必填 将搜索限制为：
     *                          包含提供的字符串的组件名称
     *                          包含提供的字符串的组件键
     * @param qualifiers        非必填 逗号分隔的组件限定符列表。使用指定的限定词过滤结果
     * @return
     */
    @RequestLine("GET api/projects/search")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    SearchProjectDTO searchProjects(
            @Param("analyzedBefore") String analyzedBefore,
            @Param("onProvisionedOnly") Boolean onProvisionedOnly,
            @Param("p") Integer PageNumber,
            @Param("projects") String projectKeys,
            @Param("ps") Integer pageSize,
            @Param("q") String limitSearch,
            @Param("qualifiers") String qualifiers
    );


    /**
     * 更新项目或模块密钥及其所有子组件密钥。
     * 必须提供“ from”或“ projectId”。
     * 需要以下权限之一：
     * “管理系统”
     * 对指定项目的“管理”权限
     *
     * @param projectOrModuleKey
     * @param newKey
     */
    @RequestLine("POST api/projects/update_key")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void updateProjectKey(
            @Param("from") String projectOrModuleKey,
            @Param("to") String newKey
    );


    @RequestLine("POST api/projects/update_visibility")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void updateVisibility(
            @Param("project") String projectKeys,
            @Param("visibility") String visibility
    );
}
