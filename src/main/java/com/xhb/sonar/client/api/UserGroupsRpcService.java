package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.CreateGroupDTO;
import com.xhb.sonar.client.dto.MembershipUserDTO;
import com.xhb.sonar.client.dto.UserGroupSearchResultDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface UserGroupsRpcService {

    /**
     * 将用户添加到组。
     * 必须提供“ id”或“ name”。
     * 需要以下权限：“管理系统”
     *
     * @param groupId   非必填
     * @param userLogin 非必填
     * @param groupName 非必填
     */
    @RequestLine("POST api/user_groups/add_user")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void addUserToGroup(@Param("id") String groupId,
                        @Param("login") String userLogin,
                        @Param("name") String groupName
    );


    /**
     * 创建一个组。
     * 需要以下权限：“管理系统”。
     *
     * @param description 新群组的说明。组说明不能超过200个字符。
     * @param name
     * @return
     */
    @RequestLine("POST api/user_groups/create")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    CreateGroupDTO createGroup(@Param("description") String description,
                               @Param("name") String name
    );


    /**
     * 删除组。默认组不能删除。
     * 必须提供“ id”或“ name”。
     * 需要以下权限：“管理系统”。
     *
     * @param groupId   非必填
     * @param groupName 非必填
     */
    @RequestLine("POST api/user_groups/delete")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void deleteGroup(@Param("id") String groupId,
                     @Param("name") String groupName
    );


    /**
     * 从组中删除用户。
     * 必须提供“ id”或“ name”。
     * 需要以下权限：“管理系统”。
     *
     * @param groupId   非必填
     * @param userLogin 非必填
     * @param groupName 非必填
     */
    @RequestLine("POST api/user_groups/remove_user")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void removeUser(@Param("id") String groupId,
                    @Param("login") String userLogin,
                    @Param("name") String groupName
    );

    /**
     * 搜索用户组。
     * 需要以下权限：“管理系统”。
     *
     * @param commaSeparatedList 非必填 响应中要返回的字段的逗号分隔列表。默认情况下返回所有字段。
     * @param pageNumber         非必填 从1开始的页码
     * @param pageSize           非必填 页面大小。必须大于0且小于或等于500
     * @param limitSearch        非必填 将搜索限制为包含提供的字符串的名称。
     * @return
     */
    @RequestLine("POST api/user_groups/search")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    UserGroupSearchResultDTO userGroupSearchResult(
            @Param("f") String commaSeparatedList,
            @Param("p") String pageNumber,
            @Param("ps") String pageSize,
            @Param("q") String limitSearch
    );

    /**
     * 更新群组。
     * 需要以下权限：“管理系统”。
     *
     * @param description 非必填 该组的新可选描述。组说明不能超过200个字符。
     *                    如果未定义值，则描述不会更改。
     * @param id          非必填 组的标识符。
     * @param name        非必填 组的新可选名称。组名不能超过255个字符，并且必须是唯一的。
     *                    值“任何人”（无论大小写）都保留并且不能使用。如果value为空或未定义，则名称不会更改。
     */
    @RequestLine("POST api/user_groups/update")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void updateGroup(
            @Param("description") String description,
            @Param("id") String id,
            @Param("name") String name
    );

    /**
     * 搜索具有有关组的成员资格信息的用户。
     * 需要以下权限：“管理系统”。
     *
     * @param groupId     非必填
     * @param groupName   非必填
     * @param pageNumber  非必填
     * @param pageSize    非必填
     * @param limitSearch 非必填
     * @param id          非必填   根据值，仅显示选定项目（selected = selected），
     *                    取消选定项目（selected = deselected）
     *                    或所有具有选择状态的项目（selected = all）。
     * @return
     */
    @RequestLine("GET api/user_groups/users")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    MembershipUserDTO getMembershipUser(
            @Param("id") String groupId,
            @Param("name") String groupName,
            @Param("p") String pageNumber,
            @Param("ps") String pageSize,
            @Param("q") String limitSearch,
            @Param("selected") String id
    );


}
