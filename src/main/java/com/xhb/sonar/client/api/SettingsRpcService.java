package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.DefinitionsListDTO;
import com.xhb.sonar.client.dto.SettingsValuesDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface SettingsRpcService {


    /**
     * 列出设置定义。
     * 指定组件时需要“浏览”权限
     * 要访问许可的设置，需要身份验证
     * 要访问安全设置，需要以下权限之一：
     * “执行分析”
     * “管理系统”
     * 对指定组件的“管理”权限
     *
     * @param component 非必填 Component key
     * @return
     */
    @RequestLine("GET api/settings/list_definitions")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    DefinitionsListDTO getDefinitionsList(@Param("component") String component);


    /**
     * 删除设置值。
     * conf / sonar.properties中定义的设置是只读的，无法更改。
     * 需要以下权限之一：
     * “管理系统”
     * 对指定组件的“管理”权限
     *
     * @param component 非必填 Component key
     * @param keys      逗号分隔的键列表 例: "sonar.links.scm,sonar.debt.hoursInDay"
     */
    @RequestLine("POST api/settings/reset")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void removeSettingValue(
            @Param("component") String component,
            @Param("keys") String keys
    );

    /**
     * 列出设置值。
     * 如果没有为设置设置任何值，则返回默认值。
     * conf / sonar.properties中的设置不包括在结果中。
     * 指定组件时需要“浏览”或“执行分析”权限。
     * 要访问安全设置，需要以下权限之一：
     * “执行分析”
     * “管理系统”
     * 对指定组件的“管理”权限
     *
     * @param component 非必填 component key
     * @param keys      逗号分隔的键列表 例: "sonar.links.scm,sonar.debt.hoursInDay"
     * @return
     */
    @RequestLine("GET api/settings/values")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    SettingsValuesDTO getSettingsValues(
            @Param("component") String component,
            @Param("keys") String keys
    );



}
