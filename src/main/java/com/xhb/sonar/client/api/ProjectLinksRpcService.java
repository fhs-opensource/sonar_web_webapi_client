package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.GetLinkOfProjectDTO;
import com.xhb.sonar.client.dto.NewProjectLinkDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;


public interface ProjectLinksRpcService {


    /**
     * 创建一个新的项目链接。
     * 需要对指定项目的“管理”权限，或全局“管理”权限。
     *
     * @param linkName
     * @param projectId  非必填
     * @param projectKey 非必填
     * @param linkUrl
     * @return
     */
    @RequestLine("POST api/project_links/create")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    NewProjectLinkDTO createNewProjectLink(
            @Param("name") String linkName,
            @Param("projectId") String projectId,
            @Param("projectKey") String projectKey,
            @Param("url") String linkUrl
    );

    /**
     * 删除现有项目链接。
     * 需要对指定项目的“管理”权限，或全局“管理”权限。
     *
     * @param linkId
     */
    @RequestLine("POST api/project_links/delete")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void deleteProjectLink(@Param("id") String linkId);


    /**
     * 列出项目的链接。
     * 必须提供“ projectId”或“ projectKey”。
     * 需要以下权限之一：
     * “管理系统”
     * 对指定项目的“管理”权限
     * 在指定项目上“浏览”
     *
     * @param projectId
     * @param projectKey
     * @return
     */
    @RequestLine("GET api/project_links/search")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    GetLinkOfProjectDTO getLinkOfProject(
            @Param("projectId") String projectId,
            @Param("projectKey") String projectKey
    );

}
