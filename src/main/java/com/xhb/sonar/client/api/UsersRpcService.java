package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.*;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface UsersRpcService {


    /**
     * 更新用户密码。如果帐户未链接到外部身份验证系统，则经过身份验证的用户可以更改自己的密码。
     * 需要“管理系统”权限才能更改另一个用户的密码。
     *
     * @param login
     * @param password         新密码
     * @param previousPassword 非必填  以前的密码。更改自己的密码时必填。
     */
    @RequestLine("POST /api/users/change_password")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void changePassword(@Param("login") String login, @Param("password") String password, @Param("previousPassword") String previousPassword);

    /**
     * 创建一个用户。
     * 如果使用给定的登录名存在已停用的用户帐户，它将被重新激活。
     * 需要管理系统权限
     *
     * @param email      非必填    用户邮箱
     * @param local      非必填    指定是应该从SonarQube服务器还是从外部认证系统对用户进行认证。
     *                   当local设置为false时，不应设置密码。
     * @param login
     * @param name
     * @param password   非必填    用户密码。仅在创建本地用户时是必需的，否则不应设置
     * @param scmAccount 非必填    SCM帐户列表。要设置多个值，必须为每个值调用一次该参数。
     * @return
     */
    @RequestLine("POST /api/users/create")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    CreateUserDTO createUser(@Param("email") String email, @Param("local") String local, @Param("login") String login, @Param("name") String name, @Param("password") String password, @Param("scmAccount") String scmAccount);

    /**
     * 停用用户。需要管理系统权限
     *
     * @param login
     * @return
     */
    @RequestLine("POST /api/users/deactivate")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    DeactivateUserDTO deactivateUser(@Param("login") String login);

    /**
     * @param login
     * @param p        非必填    从1开始的页码
     * @param ps       非必填    页面大小。必须大于0。
     * @param q        非必填    将搜索限制为包含提供的字符串的组名。
     * @param selected 非必填    根据值，仅显示选定项目（selected = selected），
     *                 取消选定项目（selected = deselected）或所有具有选择状态的项目（selected = all）。
     * @return
     */
    @RequestLine("POST /api/users/groups")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    GetGroupsDTO getGroups(@Param("login") String login, @Param("p") String p, @Param("ps") String ps, @Param("q") String q, @Param("selected") String selected);

    /**
     * 获取活动用户列表。
     * 仅当用户具有“管理系统”权限或登录到user时，才返回以下字段：
     * '电子邮件'
     * “外部身份”
     * 'externalProvider'
     * “团体”
     * 'lastConnectionDate'
     * 'tokensCount'
     *
     * @param p  非必填    从1开始的页码
     * @param ps 非必填    页面大小。必须大于0。
     * @param q  非必填    将搜索限制为包含提供的字符串的组名。
     * @return
     */
    @RequestLine("POST /api/users/search")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    SearchResultDTO userSearchResult(@Param("p") String p, @Param("ps") String ps, @Param("q") String q);

    /**
     * 更新用户。
     * 需要管理系统权限
     *
     * @param email      非必填
     * @param login      非必填
     * @param name
     * @param scmAccount 非必填    SCM帐户。要设置多个值，必须为每个值调用一次该参数。
     * @return
     */
    @RequestLine("POST /api/users/update")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    UpdateUserDTO updateUser(@Param("email") String email, @Param("login") String login, @Param("name") String name, @Param("scmAccount") String scmAccount);

    @RequestLine("POST /api/users/update_login")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void updateLogin(@Param("login") String login, @Param("newLogin") String newLogin);


}
