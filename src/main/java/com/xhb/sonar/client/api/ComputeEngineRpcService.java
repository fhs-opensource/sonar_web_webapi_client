package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.ActivityStatusDTO;
import com.xhb.sonar.client.dto.CeComponentDTO;
import com.xhb.sonar.client.dto.ComputeEngineTaskDTO;
import com.xhb.sonar.client.dto.SearchTasksDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

/**
 * 获取有关Compute Engine任务的信息。
 */
public interface ComputeEngineRpcService {
    /**
     * 搜索任务。
     * 需要系统管理权限，如果设置了componentId，则需要项目管理权限。
     *
     * @param componentId    非必填 要过滤的组件（项目）的ID
     * @param maxExecutedAt  非必填 任务处理结束的最大日期（含）
     * @param minSubmittedAt 非必填 任务提交的最短日期（含）
     * @param onlyCurrents   非必填 筛选最后的任务（仅按项目完成最近完成的任务）
     * @param pageSize       非必填 页面大小。必须大于0且小于或等于1000
     * @param limitSearch    非必填 将搜索限制为：
     *                       包含提供的字符串的组件名称
     *                       与提供的字符串完全相同的组件键
     *                       与提供的字符串完全相同的任务ID
     *                       不得与componentId一起设置
     * @param status         非必填 以逗号分隔的任务状态列表
     * @param type           非必填 任务类型
     * @return
     */
    @RequestLine("GET api/ce/activity")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    SearchTasksDTO searchTasks(
            @Param("componentId") String componentId,
            @Param("maxExecutedAt") String maxExecutedAt,
            @Param("minSubmittedAt") String minSubmittedAt,
            @Param("onlyCurrents") Boolean onlyCurrents,
            @Param("ps") Integer pageSize,
            @Param("q") String limitSearch,
            @Param("status") String status,
            @Param("type") String type
    );

    /**
     * 返回与CE活动相关的指标。
     * 需要具有对指定项目的“管理系统”权限或“管理”权限。
     * @param componentId   非必填 要过滤的组件（项目）的ID
     * @return
     */
    @RequestLine("GET api/ce/activity_status")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    ActivityStatusDTO getActivityStatus(@Param("componentId") String componentId);


    /**
     * 获取给定组件（通常是项目）的待处理任务，进行中的任务和最后执行的任务。
     * 需要以下权限：在指定组件上“浏览”。
     * 必须提供“ componentId”或“ component”。
     * @param component 非必填
     * @return
     */
    @RequestLine("GET api/ce/component")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    CeComponentDTO getComponent(@Param("component") String component);


    /**
     * 提供Compute Engine任务详细信息，例如类型，状态，持续时间和相关组件。
     * 需要“管理系统”或“执行分析”权限。
     * 从6.1开始，字段“ logs”已弃用，其值始终为false。
     * @param additionalFields  非必填 以逗号分隔的要返回的可选字段列表。
     * @param taskId
     * @return
     */
    @RequestLine("GET api/ce/task")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    ComputeEngineTaskDTO getComputeEngineTask(
            @Param("additionalFields") String additionalFields,
            @Param("id") String taskId
    );

}
