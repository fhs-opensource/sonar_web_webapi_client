package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.GlobalWebhooksDTO;
import com.xhb.sonar.client.dto.RecentDeliveriesDTO;
import com.xhb.sonar.client.dto.WebhookDTO;
import com.xhb.sonar.client.dto.WebhookDeliveryDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface WebhooksRpcService {
    /**
     * 创建一个Webhook。
     * 需要对指定项目的“管理”权限，或全局“管理”权限。
     *
     * @param name    显示在webhooks管理控制台中的名称
     * @param project 非必填 如果提供的话，secret将用作在“ X-Sonar-Webhook-HMAC-SHA256”标头中生成HMAC十六进制（小写）摘要值的密钥
     * @param secret  非必填 如果提供的话，secret将用作在“ X-Sonar-Webhook-HMAC-SHA256”标头中生成HMAC十六进制（小写）摘要值的密钥
     * @param url     Server endpoint that will receive the webhook payload, for example 'http://my_server/foo'. If HTTP Basic authentication is used, HTTPS is recommended to avoid man in the middle attacks. Example: 'https://myLogin:myPassword@my_server/foo'
     * @return
     */
    @RequestLine("POST api/webhooks/create")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    WebhookDTO createWebhook(
            @Param("name") String name,
            @Param("project") String project,
            @Param("secret") String secret,
            @Param("url") String url
    );

    /**
     * 删除一个Webhook。
     * 需要对指定项目的“管理”权限，或全局“管理”权限。
     * @param webhook   要删除的Webhook的密钥，可以通过api / webhooks / create或api / webhooks / list获取自动生成的值
     */
    @RequestLine("POST api/webhooks/delete")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void deleteWebhook( @Param("webhook") String webhook);

    /**
     * 获取指定项目或Compute Engine任务的最近交付。
     * 在相关项目上需要“管理”权限。
     * 请注意，其他信息由api / webhooks / delivery返回。
     *
     * @param ceTaskId     非必填     Compute Engine任务的ID
     * @param componentKey 非必填 priject key
     * @param p            非必填 从1开始的页码
     * @param ps           非必填 页面大小。必须大于0且小于500
     * @param webhook      非必填 触发这些交付的webhook的密钥，可以通过api / webhooks / create或api / webhooks / list获得自动生成的值
     * @return
     */
    @RequestLine("GET api/webhooks/deliveries")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    RecentDeliveriesDTO getRecentDeliveries(
            @Param("ceTaskId") String ceTaskId,
            @Param("componentKey") String componentKey,
            @Param("p") Integer p,
            @Param("ps") Integer ps,
            @Param("webhook") String webhook
    );


    /**
     * 通过其ID获取Webhook传递。
     * 需要“管理系统”权限。
     * 请注意，其他信息由api / webhooks / delivery返回。
     *
     * @param deliveryId 送货编号
     * @return
     */
    @RequestLine("GET api/webhooks/delivery")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    WebhookDeliveryDTO getWebhookDelivery(@Param("deliveryId") String deliveryId);

    /**
     * 搜索全局Webhooks或项目Webhooks。 Webhook按名称排序。
     * 需要对指定项目的“管理”权限，或全局“管理”权限。
     *
     * @param project
     * @return
     */
    @RequestLine("GET api/webhooks/list")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    GlobalWebhooksDTO searchGlobalWebhooks(@Param("project") String project);


    /**
     * 更新Webhook。
     * 需要对指定项目的“管理”权限，或全局“管理”权限。
     *
     * @param name    Webhook的新名称
     * @param secret  非必填 如果提供的话，secret将用作在“ X-Sonar-Webhook-HMAC-SHA256”标头中生成HMAC十六进制（小写）摘要值的密钥
     * @param url     新url
     * @param webhook 要更新的Webhook的密钥，可以通过api / webhooks / create或api / webhooks / list获取自动生成的值
     */
    @RequestLine("POST api/webhooks/update")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void updateWebhooks(
            @Param("name") String name,
            @Param("secret") String secret,
            @Param("url") String url,
            @Param("webhook") String webhook
    );

}
