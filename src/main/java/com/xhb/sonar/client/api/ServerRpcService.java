package com.xhb.sonar.client.api;

import feign.Headers;
import feign.RequestLine;

public interface ServerRpcService {
    /**
     * 获取服务的版本号
     * @return
     */
    @RequestLine("GET api/server/version")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    String getServerVersion();

}
