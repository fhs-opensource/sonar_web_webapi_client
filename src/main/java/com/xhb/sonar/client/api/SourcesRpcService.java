package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.GetSCMInformationDTO;
import com.xhb.sonar.client.dto.GetSourceCodeDTO;
import com.xhb.sonar.client.dto.SourcesLinesDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface SourcesRpcService {

    /**
     * 响应示例不完整,返回值未添加
     */
    /**
     * 获取源代码作为原始文本。要求具有“查看源代码”权限
     *
     * @param fileKey
     */
    @RequestLine("GET api/sources/raw")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void getRaw(@Param("key") String fileKey);

    /**
     * 获取源文件的SCM信息。需要查看文件项目的源代码权限
     * 结果数组的每个元素都包含：
     * 电话号码
     * 提交的作者
     * 提交的日期时间（在5.2之前只是日期）
     * 修订提交（在5.2中添加）
     *
     * @param commitsByLine 非必填 如果value为false，则按SCM提交对行进行分组，否则，即使两行连续都与同一提交相关，也将显示每行的提交。
     * @param firstLine     非必填 第一行返回。从1开始
     * @param fileKey
     * @param lastLine      非必填 最后一行返回（含）
     * @return
     */
    @RequestLine("GET api/sources/scm")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    GetSCMInformationDTO getScmInformation(
            @Param("commits_by_line") String commitsByLine,
            @Param("from") String firstLine,
            @Param("key") String fileKey,
            @Param("to") String lastLine
    );

    /**
     * 获取源代码。需要查看文件项目的源代码权限
     * 结果数组的每个元素都包含：
     * 电话号码
     * 行内容
     *
     * @param firstLine 非必填 第一行返回。从1开始
     * @param fileKey
     * @param lastLine  非必填 最后一行返回（含）
     * @return
     */
    @RequestLine("GET api/sources/show")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    GetSourceCodeDTO getSourceCode(
            @Param("from") String firstLine,
            @Param("key") String fileKey,
            @Param("to") String lastLine
    );

    /**
     * 显示带有面向行信息的源代码。需要查看文件项目的源代码权限
     * 结果数组的每个元素都是一个对象，其中包含：
     * 电话号码
     * 行内容
     * 该行的作者（来自SCM信息）
     * 修改生产线（根据SCM信息）
     * 该行的最后提交日期（来自SCM信息）
     * 覆盖范围的行点击
     * 测试中要涵盖的条件数
     * 测试涵盖的条件数
     * 这条线是否是新的
     * @param branch    非必填 Branch key
     * @param from  非必填 从哪一行开始返回。从1开始
     * @param key   非必填 文件密钥。如果未设置参数'uuid'，则为必选。自5.2起可用
     * @param pullRequest   非必填 Pull request id
     * @param to    非必填 返回的可选最后一行（含）。它必须大于或等于参数“ from”。如果未设置，则返回所有大于或等于“ from”的行。
     * @param uuid  非必填 文件uuid。如果未设置参数“key”，则必须提供
     * @return
     */
    @RequestLine("GET api/sources/lines")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    SourcesLinesDTO getSourcesLines(
            @Param("branch") String branch,
            @Param("from") Integer from,
            @Param("key") String key,
            @Param("pullRequest") String pullRequest,
            @Param("to") Integer to,
            @Param("uuid") String uuid
    );
}
