package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.ProjectAnalysisEventDTO;
import com.xhb.sonar.client.dto.SearchAnalysesAndAttachedEventsDTO;
import com.xhb.sonar.client.dto.UpdateEventDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface ProjectAnalysesRpcService {


    /**
     * 创建一个项目分析事件。
     * 只能创建类别“ VERSION”和“ OTHER”的事件。
     * 需要以下权限之一：
     * “管理系统”
     * 对指定项目的“管理”权限
     *
     * @param analysisKey
     * @param category    非必填
     * @param name
     * @return
     */
    @RequestLine("POST api/project_analyses/create_event")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    ProjectAnalysisEventDTO createAnalysisEvent(
            @Param("analysis") String analysisKey,
            @Param("category") String category,
            @Param("name") String name
    );


    /**
     * 删除项目分析。
     * 需要以下权限之一：
     * “管理系统”
     * 指定分析项目的“管理”权限
     *
     * @param analysisKey
     */
    @RequestLine("POST api/project_analyses/delete")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void deleteProjectAnalysis(@Param("analysis") String analysisKey);


    /**
     * 删除项目分析事件。
     * 只能删除类别“ VERSION”和“ OTHER”的事件。
     * 需要以下权限之一：
     * “管理系统”
     * 对指定项目的“管理”权限
     *
     * @param eventKey
     */
    @RequestLine("POST api/project_analyses/delete_event")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void deleteProjectAnalysisEvent(@Param("event") String eventKey);


    /**
     * 搜索项目分析和附加事件。
     * 需要以下权限：在指定项目上“浏览”
     *
     * @param categoryKey 非必填 活动类别。筛选分析至少具有指定类别的一个事件。
     * @param afterDate   非必填 过滤在给定日期（含）之后创建的分析。
     *                    可以提供日期（服务器时区）或日期时间
     * @param pageNumber  非必填 从1开始的页码
     * @param projectKey  非必填
     * @param pageSize    非必填 页面大小。必须大于0且小于或等于500
     * @param beforeDate  非必填 过滤在给定日期（含）之前创建的分析。
     *                    可以提供日期（服务器时区）或日期时间
     * @return
     */
    @RequestLine("POST api/project_analyses/search")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    SearchAnalysesAndAttachedEventsDTO searchAnalysesAndAttachedEvents(
            @Param("category") String categoryKey,
            @Param("from") String afterDate,
            @Param("p") Integer pageNumber,
            @Param("project") String projectKey,
            @Param("ps") Integer pageSize,
            @Param("to") String beforeDate
    );

    /**
     * 将分析设置为项目或长期分支的新代码周期的基准。
     * 手动设置的基线会覆盖“ sonar.leak.period”设置。
     * 需要以下权限之一：
     * “管理系统”
     * 对指定项目的“管理”权限
     *
     * @param analysisKey
     * @param branchKey 非必填
     * @param projectKey
     */
    @RequestLine("POST api/project_analyses/set_baseline")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void setBaseline(
            @Param("analysis") String analysisKey,
            @Param("branch") String branchKey,
            @Param("project") String projectKey
    );

    /**
     * 在项目或长期存在的分支上取消任何手动设置的“新代码周期”基准。
     * 取消设置手动基准将恢复对“ sonar.leak.period”设置的使用。
     * 需要以下权限之一：
     * “管理系统”
     * 对指定项目的“管理”权限
     * @param branchKey 非必填
     * @param projectKey
     */
    @RequestLine("POST api/project_analyses/unset_baseline")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void unsetBaseline(
            @Param("branch") String branchKey,
            @Param("project") String projectKey
    );


    /**
     * 更新项目分析事件。
     * 只能更新类别“ VERSION”和“ OTHER”的事件。
     * 需要以下权限之一：
     * “管理系统”
     * 对指定项目的“管理”权限
     * @param eventKey
     * @param name  新name
     * @return
     */
    @RequestLine("POST api/project_analyses/update_event")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    UpdateEventDTO updateEvent(
            @Param("event") String eventKey,
            @Param("name") String name
    );

}
