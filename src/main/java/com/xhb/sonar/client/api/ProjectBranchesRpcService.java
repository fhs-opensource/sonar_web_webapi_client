package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.ProjectBranchesDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface ProjectBranchesRpcService {


    /**
     * 删除项目的非主要分支。
     * 要求对指定项目具有“管理”权限。
     * @param branchName
     * @param projectKey
     */
    @RequestLine("POST api/project_branches/delete")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void deleteProjectBranches(
            @Param("branch") String branchName,
            @Param("project") String projectKey
    );


    /**
     * 列出项目的分支。
     * 要求具有对指定项目的“浏览”或“执行分析”权限。
     * @param projectKey
     * @return
     */
    @RequestLine("GET api/project_branches/list")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    ProjectBranchesDTO getProjectBranches(
            @Param("project") String projectKey
    );


    /**
     * 重命名项目的主分支。
     * 需要对指定项目的“管理”权限。
     * @param branchName
     * @param projectKey
     */
    @RequestLine("GET api/project_branches/rename")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void renameForBranch(
            @Param("name") String branchName,
            @Param("project") String projectKey

    );


}
