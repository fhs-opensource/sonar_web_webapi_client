package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.MetricTypesDTO;
import com.xhb.sonar.client.dto.MetricsSearchResult;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

/**
 * 可用扩展指标查询
 */
public interface MetricsRpcService {

    /**
     * 获取自定义指标
     *  返回的结果key 可以用来给 MeasuresRpcService.componentResult 传
     * @param p 第几页
     * @param ps 每页多少条
     * @return
     */
    @RequestLine("GET api/metrics/search")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    MetricsSearchResult search(@Param("p") Integer p, @Param("ps") Integer ps);


    @RequestLine("GET api/metrics/types")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    MetricTypesDTO getMetricTypes();
}
