package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.AvailablePluginsDTO;
import com.xhb.sonar.client.dto.PendingPluginsDTO;
import com.xhb.sonar.client.dto.PluginsInstalledDTO;
import com.xhb.sonar.client.dto.UpdatesPluginsDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface PluginsRpcService {

    /**
     * 获取可在SonarQube实例上安装的所有可用插件列表，按插件名称排序。
     * 从更新中心检索插件信息。响应中提供了上次刷新更新中心的日期和时间。
     * 更新状态值为：
     * COMPATIBLE：插件与当前的SonarQube实例兼容。
     * INCOMPATIBLE：插件与当前的SonarQube实例不兼容。
     * REQUIRES_SYSTEM_UPGRADE：插件要求SonarQube在安装之前先进行升级。
     * DEPS_REQUIRE_SYSTEM_UPGRADE：至少一个依赖于该插件的插件需要SonarQube进行升级。
     * 需要"Administer System"权限。
     *
     * @return
     */
    @RequestLine("GET api/plugins/available")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    AvailablePluginsDTO getAvailablePlugins();


    /**
     * 取消任何插件上待执行的任何操作（安装，更新或卸载）
     * 要求用户通过“管理系统”权限进行身份验证
     */
    @RequestLine("POST api/plugins/cancel_all")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void cancelAllOperation();


    /**
     * 安装由其密钥指定的最新版本的插件。
     * 从更新中心检索插件信息。
     * 要求用户通过“管理系统”权限进行身份验证
     *
     * @param pluginKey
     */
    @RequestLine("GET api/plugins/install")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void installPlugins(@Param("key") String pluginKey);


    /**
     * 获取SonarQube实例上安装的所有插件的列表，按插件名称排序。
     *
     * @param f 非必填 以逗号分隔的要返回的其他字段列表。默认情况下，不返回任何其他字段。可能的值为：
     *          category-更新中心中定义的类别。需要连接到更新中心
     * @return
     */
    @RequestLine("GET api/plugins/installed")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    PluginsInstalledDTO getPluginsInstalled(@Param("f") String f);


    /**
     * 获取将在SonarQube实例的下次启动时安装或删除的插件列表，按插件名称排序。
     * 需要“Administer System”权限。
     *
     * @return
     */
    @RequestLine("GET api/plugins/pending")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    PendingPluginsDTO getPendingPlugins();


    /**
     * 卸载由其键指定的插件。
     * 要求用户通过“管理系统”权限进行身份验证。
     *
     * @param key 标识要卸载的插件的密钥
     */
    @RequestLine("POST api/plugins/uninstall")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void uninstallPlugin(@Param("key") String key);


    /**
     * 将由其键指定的插件更新为与SonarQube实例兼容的最新版本。
     * 从更新中心检索插件信息。
     * 要求用户通过“管理系统”权限进行身份验证
     *
     * @param key 标识要更新的插件的密钥
     */
    @RequestLine("POST api/plugins/update")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void updatePlugins(@Param("key") String key);


    /**
     * 列出安装在SonarQube实例上的插件，至少可以使用一个新版本，并按插件名称排序。
     * 列出了每个较新的版本，从最旧的到最新的，并带有其自己的更新/兼容性状态。
     * 从更新中心检索插件信息。响应中提供了上次刷新更新中心的日期和时间。
     * 更新状态值为：[COMPATIBLE，INCOMPATIBLE，REQUIRES_UPGRADE，DEPS_REQUIRE_UPGRADE]。
     * 需要“管理系统”权限。
     *
     * @return
     */
    @RequestLine("GET api/plugins/updates")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    UpdatesPluginsDTO getUpdatesPlugins();
}
