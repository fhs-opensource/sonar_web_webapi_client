package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.ProgrammingLanguagesDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface LanguagesRpcService {

    /**
     * 列出支持的编程语言
     *
     * @param pageSize 非必填 要返回的列表大小，所有语言均为0
     * @param pattern  非必填 一种匹配语言键/名称的模式
     * @return
     */
    @RequestLine("GET api/languages/list")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    ProgrammingLanguagesDTO getProgrammingLanguages(
            @Param("ps") Integer pageSize,
            @Param("q") String pattern
    );

}
