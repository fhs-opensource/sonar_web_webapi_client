package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.*;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

/**
 * 管理权限模板，以及在全局和项目级别上授予和撤消权限。
 */
public interface QualityprofilesRpcService {


    /**
     * 在质量配置文件上激活规则。
     * 需要以下权限之一：
     * “管理质量资料”
     * 在指定的质量配置文件上进行修改
     *
     * @param key      质量配置文件密钥。可以通过api / qualityprofiles / search获得
     * @param params   非必填 参数为键=值的分号列表。如果参数reset为true，则忽略。
     * @param reset    非必填 重置激活规则的严重性和参数。设置在父配置文件或规则默认值中定义的值。
     * @param rule
     * @param severity 非必填 严重程度。如果参数reset为true，则忽略。
     */
    @RequestLine("POST api/qualityprofiles/activate_rule")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void activateRule(
            @Param("key") String key,
            @Param("params") String params,
            @Param("reset") String reset,
            @Param("rule") String rule,
            @Param("severity") String severity
    );


    /**
     * 在一个质量配置文件中批量激活规则。
     * 需要以下权限之一：
     * “管理质量资料”
     * 在指定的质量配置文件上进行修改
     *
     * @param activation          非必填   在选定的质量配置文件上激活或停用的过滤规则。如果未设置参数“ qprofile”，则忽略。
     * @param activeSeverities    非必填   以逗号分隔的激活级别列表，即“质量”配置文件中规则的严重性。
     * @param asc                 非必填   升序排序
     * @param availableSince      非必填   过滤自日期以来添加的规则。格式为yyyy-MM-dd
     * @param cwe                 非必填   逗号分隔的CWE标识符列表。使用“未知”来选择与任何CWE不相关的规则。
     * @param inheritance         非必填   质量配置文件中规则的继承值的逗号分隔列表。仅在设置了参数“激活”时使用。
     * @param isTemplate          非必填   过滤模板规则
     * @param languages           非必填   以逗号分隔的语言列表
     * @param owaspTop10          非必填   OWASP十大小写类别的列表，以逗号分隔。
     * @param query               非必填   UTF-8搜索查询
     * @param qprofile            非必填   要过滤的质量配置文件关键字。仅在设置了参数“激活”时使用
     * @param repositories        非必填   以逗号分隔的存储库列表
     * @param ruleKey             非必填
     * @param sortField           非必填   排序栏位
     * @param sansTop25           非必填   以逗号分隔的SANS前25个类别列表。
     * @param severities          非必填   以逗号分隔的默认严重性列表。与质量配置文件中规则的严重性不同。
     * @param sonarsourceSecurity 非必填   以逗号分隔的SonarSource安全类别列表。使用“其他”选择不与任何类别关联的规则
     * @param statuses            非必填   逗号分隔的状态码列表
     * @param tags                非必填   逗号分隔的标签列表。返回的规则与任何标签匹配（或运算符）
     * @param targetKey           非必填   完成规则激活的“质量配置文件”键。要检索质量配置文件密钥，请参阅api / qualityprofiles / search
     * @param targetSeverity      非必填   设置激活规则的严重性
     * @param template_key        非必填   要过滤的模板规则的键。用于基于此模板搜索自定义规则。
     * @param types               非必填   以逗号分隔的类型列表。返回的规则与任何标签匹配（或运算符）
     */
    @RequestLine("POST api/qualityprofiles/activate_rules")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void activateRules(
            @Param("activation") String activation,
            @Param("active_severities") String activeSeverities,
            @Param("asc") String asc,
            @Param("available_since") String availableSince,
            @Param("cwe") String cwe,
            @Param("inheritance") String inheritance,
            @Param("is_template") String isTemplate,
            @Param("languages") String languages,
            @Param("owaspTop10") String owaspTop10,
            @Param("q") String query,
            @Param("qprofile") String qprofile,
            @Param("repositories") String repositories,
            @Param("rule_key") String ruleKey,
            @Param("s") String sortField,
            @Param("sansTop25") String sansTop25,
            @Param("severities") String severities,
            @Param("sonarsourceSecurity") String sonarsourceSecurity,
            @Param("statuses") String statuses,
            @Param("tags") String tags,
            @Param("targetKey") String targetKey,
            @Param("targetSeverity") String targetSeverity,
            @Param("template_key") String template_key,
            @Param("types") String types
    );


    /**
     * 将项目与质量配置文件相关联。
     * 需要以下权限之一：
     * “管理质量资料”
     * 在指定的质量配置文件上进行修改
     * 对指定项目的管理权
     *
     * @param language       非必填 质量配置文件语言。如果未设置“键”，则为必选。
     * @param project        非必填
     * @param qualityProfile 非必填 质量配置文件名称。如果未设置“键”，则为必选。
     */
    @RequestLine("POST api/qualityprofiles/add_project")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void addProject(
            @Param("language") String language,
            @Param("project") String project,
            @Param("qualityProfile") String qualityProfile
    );


    /**
     * 备份XML格式的质量配置文件。可以通过api / qualityprofiles / restore恢复导出的配置文件。
     *
     * @param language       非必填 质量配置文件语言。如果未设置“键”，则为必选。
     * @param qualityProfile 非必填 质量配置文件名称。如果未设置“键”，则为必选。
     * @return
     */
    @RequestLine("GET api/qualityprofiles/backup")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    String backupConfigXML(
            @Param("language") String language,
            @Param("qualityProfile") String qualityProfile
    );


    /**
     * 更改质量配置文件的父项。
     * 需要以下权限之一：
     * “管理质量资料”
     * 在指定的质量配置文件上进行修改
     *
     * @param language             非必填 质量资料语言。如果未设置“键”，则为必选。
     * @param parentQualityProfile 非必填 质量配置文件名称。如果设置了此参数，则不得设置“ parentKey”，而必须将“ language”设置为消除歧义。
     * @param qualityProfile       非必填 质量配置文件名称。如果未设置“键”，则为必选。
     */
    @RequestLine("POST api/qualityprofiles/change_parent")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void changeParent(
            @Param("language") String language,
            @Param("parentQualityProfile") String parentQualityProfile,
            @Param("qualityProfile") String qualityProfile
    );


    /**
     * 获取质量配置文件中的更改历史记录：规则激活/停用，参数更改/严重性。事件按日期降序排列（最新的优先）。
     *
     * @param language       非必填 质量配置文件语言。如果未设置“键”，则为必选。
     * @param pageNumber     非必填 从1开始的页码
     * @param pageSize       非必填 页面大小。必须大于0且小于或等于500
     * @param qualityProfile 非必填 质量配置文件名称。如果未设置“键”，则为必选。
     * @param since          非必填 变更日志的开始日期。
     *                       可以提供日期（服务器时区）或日期时间。
     * @param to             非必填 变更日志的结束日期。
     *                       可以提供日期（服务器时区）或日期时间。
     * @return
     */
    @RequestLine("POST api/qualityprofiles/changelog")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    QualityprofilesChangelogDTO qualityprofilesChangelog(
            @Param("language") String language,
            @Param("p") String pageNumber,
            @Param("ps") String pageSize,
            @Param("qualityProfile") String qualityProfile,
            @Param("since") String since,
            @Param("to") String to
    );


    /**
     * 复制质量配置文件。
     * 需要登录并具有“管理质量配置文件”权限。
     *
     * @param fromKey 非必填 质量配置文件密钥
     * @param toName  非必填 新质量配置文件的名称。
     * @return
     */
    @RequestLine("POST api/qualityprofiles/copy")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    CopyQualityProfileDTO copyQualityProfile(
            @Param("fromKey") String fromKey,
            @Param("toName") String toName
    );


    /**
     * 创建质量配置文件。
     * 需要登录并具有“管理质量配置文件”权限。
     *
     * @param language 非必填
     * @param name     非必填
     * @return
     */
    @RequestLine("POST api/qualityprofiles/create")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    CreateQualityProfileDTO createQualityProfile(
            @Param("language") String language,
            @Param("name") String name,
            @Param("organization") String organization
    );


    /**
     * 批量停用质量配置文件中的规则。
     * 需要以下权限之一：
     * “管理质量资料”
     * 在指定的质量配置文件上进行修改
     *
     * @param key  非必填 质量配置文件密钥。可以通过api / qualityprofiles / search获得
     * @param rule 非必填
     */
    @RequestLine("POST api/qualityprofiles/deactivate_rule")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void deactivateRule(
            @Param("key") String key,
            @Param("rule") String rule
    );


    /**
     * 批量停用质量配置文件中的规则。
     * 需要以下权限之一：
     * “管理质量资料”
     * 在指定的质量配置文件上进行修改
     *
     * @param activation          非必填   在选定的质量配置文件上激活或停用的过滤规则。如果未设置参数“ qprofile”，则忽略。
     * @param activeSeverities    非必填   以逗号分隔的激活级别列表，即“质量”配置文件中规则的严重性。
     * @param asc                 非必填   升序排序
     * @param availableSince      非必填   过滤自日期以来添加的规则。格式为yyyy-MM-dd
     * @param cwe                 非必填   逗号分隔的CWE标识符列表。使用“未知”来选择与任何CWE不相关的规则。
     * @param inheritance         非必填   质量配置文件中规则的继承值的逗号分隔列表。仅在设置了参数“激活”时使用。
     * @param isTemplate          非必填   过滤模板规则
     * @param languages           非必填   以逗号分隔的语言列表
     * @param owaspTop10          非必填   OWASP十大小写类别的列表，以逗号分隔。
     * @param query               非必填   UTF-8搜索查询
     * @param qprofile            非必填   要过滤的质量配置文件关键字。仅在设置了参数“激活”时使用
     * @param repositories        非必填   以逗号分隔的存储库列表
     * @param ruleKey             非必填
     * @param sortField           非必填   排序栏位
     * @param sansTop25           非必填   以逗号分隔的SANS前25个类别列表。
     * @param severities          非必填   以逗号分隔的默认严重性列表。与质量配置文件中规则的严重性不同。
     * @param sonarsourceSecurity 非必填   以逗号分隔的SonarSource安全类别列表。使用“其他”选择不与任何类别关联的规则
     * @param statuses            非必填   逗号分隔的状态码列表
     * @param tags                非必填   逗号分隔的标签列表。返回的规则与任何标签匹配（或运算符）
     * @param targetKey           非必填   完成规则激活的“质量配置文件”键。要检索质量配置文件密钥，请参阅api / qualityprofiles / search
     * @param targetSeverity      非必填   设置激活规则的严重性
     * @param template_key        非必填   要过滤的模板规则的键。用于基于此模板搜索自定义规则。
     * @param types               非必填   以逗号分隔的类型列表。返回的规则与任何标签匹配（或运算符）
     */
    @RequestLine("POST api/qualityprofiles/deactivate_rules")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void deactivateRules(
            @Param("activation") String activation,
            @Param("active_severities") String activeSeverities,
            @Param("asc") String asc,
            @Param("available_since") String availableSince,
            @Param("cwe") String cwe,
            @Param("inheritance") String inheritance,
            @Param("is_template") String isTemplate,
            @Param("languages") String languages,
            @Param("owaspTop10") String owaspTop10,
            @Param("q") String query,
            @Param("qprofile") String qprofile,
            @Param("repositories") String repositories,
            @Param("rule_key") String ruleKey,
            @Param("s") String sortField,
            @Param("sansTop25") String sansTop25,
            @Param("severities") String severities,
            @Param("sonarsourceSecurity") String sonarsourceSecurity,
            @Param("statuses") String statuses,
            @Param("tags") String tags,
            @Param("targetKey") String targetKey,
            @Param("targetSeverity") String targetSeverity,
            @Param("template_key") String template_key,
            @Param("types") String types
    );


    /**
     * 删除质量配置文件及其所有后代。默认质量配置文件无法删除。
     * 需要以下权限之一：
     * “管理质量资料”
     * 在指定的质量配置文件上进行修改
     *
     * @param languages      非必填 质量资料语言。如果未设置“键”，则为必选。
     * @param qualityProfile 非必填 质量配置文件名称。如果未设置“键”，则为必选。
     */
    @RequestLine("POST api/qualityprofiles/delete")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void deleteAllQualityProfile(
            @Param("languages") String languages,
            @Param("qualityProfile") String qualityProfile
    );


    /**
     * 导出质量配置文件。
     *
     * @param language      非必填 质量资料语言
     * @param qualityProfile 非必填 要导出的质量配置文件名称。如果保留为空，则导出该语言的默认配置文件。
     * @return
     */
    @RequestLine("POST api/qualityprofiles/export")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    String exportQualityProfile(
            @Param("language") String language,
            @Param("organization") String organization,
            @Param("qualityProfile") String qualityProfile
    );


    /**
     * 列出可用的配置文件导出格式。
     *
     * @return
     */
    @RequestLine("GET api/qualityprofiles/exporters")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    QualityProfileFormatDTO getQualityProfileFormatList();


    /**
     * 列出支持的 文件导入格式
     *
     * @return
     */
    @RequestLine("POST api/qualityprofiles/delete")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    SupportedImportersDTO getSupportedImporters();


    /**
     * 显示 qualityProfile 的继承关系
     *
     * @param languages      非必填 质量资料语言。如果未设置“键”，则为必选。
     * @param qualityProfile 非必填 质量配置文件名称。如果未设置“键”，则为必选。
     * @return
     */
    @RequestLine("GET api/qualityprofiles/inheritance")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    QualityprofilesInheritanceDTO getQualityprofilesInheritance(
            @Param("languages") String languages,
            @Param("qualityProfile") String qualityProfile
    );


    /**
     * 列出project 及 qualityProfile的关联状态
     *
     * @param qualityProfileKey 非必填     质量配置文件密钥
     * @param pageNumber        非必填
     * @param pageSize          非必填
     * @param query             非必填 将搜索限制为包含提供的字符串的项目。
     * @param selected          非必填 根据值，仅显示选定项目（selected = selected），取消选定项目（selected = deselected）或所有具有选择状态的项目（selected = all）。
     * @return
     */
    @RequestLine("GET api/qualityprofiles/projects")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    ProjectsAnQualityProfileAssociationStatusDTO getProjectsAnQualityProfileAssociationStatus(
            @Param("key") String qualityProfileKey,
            @Param("p") String pageNumber,
            @Param("ps") String pageSize,
            @Param("q") String query,
            @Param("selected") String selected
    );


    /**
     * 删除项目与质量配置文件的关联。
     * 需要以下权限之一：
     * “管理质量资料”
     * 在指定的质量配置文件上进行修改
     * 对指定项目的管理权
     *
     * @param languages      非必填      质量资料语言。如果未设置“键”，则为必选。
     * @param project        非必填
     * @param qualityProfile 非必填  质量配置文件名称。如果未设置“键”，则为必选。
     */
    @RequestLine("POST api/qualityprofiles/remove_project")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void removeProject(
            @Param("languages") String languages,
            @Param("project") String project,
            @Param("qualityProfile") String qualityProfile
    );


    /**
     * 重命名质量配置文件。
     * 需要以下权限之一：
     * “管理质量资料”
     * 在指定的质量配置文件上进行修改
     *
     * @param qualityProfileKey 非必填
     * @param newName           非必填
     */
    @RequestLine("POST api/qualityprofiles/rename")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void renameToQualityProfile(
            @Param("key") String qualityProfileKey,
            @Param("name") String newName

    );


    /**
     * 使用XML文件还原质量配置文件。恢复的配置文件名称是从备份文件中获取的，因此，如果已经存在具有相同名称和语言的配置文件，它将被覆盖。
     * 需要登录并具有“管理质量配置文件”权限。
     *
     * @param backupFile XML格式的配置文件备份文件，由api / qualityprofiles / backup或以前的api / profiles / backup生成。
     */
    @RequestLine("POST api/qualityprofiles/restore")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void restoreQualityProfile(@Param("backup") String backupFile);


    /**
     * Search quality profiles
     *
     * @param defaults       非必填     如果设置为true，则仅返回标记为每种语言的默认质量配置文件
     * @param language       非必填 语言键。如果提供，则仅返回给定语言的配置文件。
     * @param project        非必填
     * @param qualityProfile 非必填
     * @return
     */
    @RequestLine("GET  api/qualityprofiles/search")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    SearchQualityProfilesDTO searchQualityProfiles(
            @Param("defaults") String defaults,
            @Param("language") String language,
            @Param("organization") String organization,
            @Param("project") String project,
            @Param("qualityProfile") String qualityProfile
    );


    /**
     * 选择给定语言的默认配置文件。
     * 需要登录并具有“管理质量配置文件”权限。
     *
     * @param language
     * @param qualityProfile
     */
    @RequestLine("POST api/qualityprofiles/set_default")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void setDefault(
            @Param("language") String language,
            @Param("qualityProfile") String qualityProfile
    );


}
