package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.GetPullRequestsForProjectDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface ProjectPullRequestsRpcService {


    /**
     * 删除拉取请求。
     * 要求对指定项目具有“管理”权限。
     * @param projectKey
     * @param pullRequestId
     */
    @RequestLine("POST api/project_pull_requests/delete")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void deletePullRequest(
            @Param("project") Integer projectKey,
            @Param("pullRequest") String pullRequestId
    );


    /**
     * 列出项目的拉取请求。
     * 需要以下权限之一：
     * 对指定项目的“浏览”权限
     * 对指定项目的“执行分析”权限
     * @param projectKey
     * @return
     */
    @RequestLine("GET api/project_pull_requests/list")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    GetPullRequestsForProjectDTO getPullRequestsFroProject(
            @Param("project") Integer projectKey
    );
}
