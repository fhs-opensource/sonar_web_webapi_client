package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.*;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface RulesRpcService {


    /**
     * @param customKey           自定义规则的Key
     * @param markdownDescription 规则说明
     * @param name                规则名称
     * @param params              非必填    参数为=的分号列表，例如'params = key1 = v1; key2 = v2'（仅适用于自定义规则）
     * @param preventReactivation 非必填    如果设置为true且规则已停用（状态“已删除”），则将返回状态409
     * @param severity            非必填    规则严重性
     * @param status              非必填    规则状态
     * @param templateKey         非必填    模板规则的关键字，以创建自定义规则（自定义规则的必需项）
     * @param type                非必填    规则类型
     * @return
     */
    @RequestLine("POST /api/rules/create")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    CreateRulesDTO createRule(
            @Param("custom_key") String customKey,
            @Param("markdown_description") String markdownDescription,
            @Param("name") String name,
            @Param("params") String params,
            @Param("prevent_reactivation") String preventReactivation,
            @Param("severity") String severity,
            @Param("status") String status,
            @Param("template_key") String templateKey,
            @Param("type") String type);

    /**
     * 删除自定义规则。
     * 需要“管理质量配置文件”权限
     *
     * @param key
     */
    @RequestLine("POST /api/rules/delete")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void deleteRule(@Param("key") String key);

    /**
     * 列出可用的规则存储库
     *
     * @param language 非必填    语言键；如果提供，将仅返回给定语言的存储库
     * @param q        非必填    一种用于匹配存储库键/名称的模式
     * @return
     */
    @RequestLine("GET /api/rules/repositories")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    RulesRepositoriesDTO getRuleRepositories(@Param("language") String language, @Param("q") String q);


    /**
     * 搜索与指定查询匹配的相关规则的集合。
     * 从5.5开始，已弃用响应中的以下字段：
     * “ effortToFixDescription”变为“ gapDescription”
     * “ debtRemFnCoeff”成为“ remFnGapMultiplier”
     * “ defaultDebtRemFnCoeff”成为“ defaultRemFnGapMultiplier”
     * “ debtRemFnOffset”变为“ remFnBaseEffort”
     * “ defaultDebtRemFnOffset”变为“ defaultRemFnBaseEffort”
     * “ debtOverloaded”变为“ remFnOverloaded”
     *
     * @return
     */
    @RequestLine("GET /api/rules/search")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    RulesSearchResultDTO ruleSearchResult();

    /**
     * 获取有关规则的详细信息
     * 从5.5开始，已弃用响应中的以下字段：
     * “ effortToFixDescription”变为“ gapDescription”
     * “ debtRemFnCoeff”成为“ remFnGapMultiplier”
     * “ defaultDebtRemFnCoeff”成为“ defaultRemFnGapMultiplier”
     * “ debtRemFnOffset”变为“ remFnBaseEffort”
     * “ defaultDebtRemFnOffset”变为“ defaultRemFnBaseEffort”
     * “ debtOverloaded”变为“ remFnOverloaded”
     * 在7.1中，添加了“作用域”字段。
     *
     * @param actives 非必填    显示所有配置文件的规则激活情况（“活动规则”）
     * @param key
     * @return
     */
    @RequestLine("GET /api/rules/show")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    GetRuleInformationDTO getRuleInfo(@Param("actives") String actives, @Param("key") String key);


    /**
     * 列出规则标签
     *
     * @param ps 非必填    页面大小。必须大于0且小于或等于100
     * @param q  非必填    将搜索限制为包含提供的字符串的标签。
     * @return
     */
    @RequestLine("GET /api/rules/tags")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    RuleTagsDTO getRuleTags(@Param("ps") String ps, @Param("q") String q);

    /**
     * 更新现有规则。
     * 需要“管理质量配置文件”权限
     *
     * @param key
     * @param markdownDescription        非必填    规则说明（自定义规则和手动规则必需)
     * @param markdownNote               非必填    降价格式的可选注释。使用空值删除当前注释。如果未设置参数，则注释不会更改。
     * @param name                       非必填    规则名称（自定义规则必需）
     * @param params                     非必填    参数为=的分号列表，例如'params = key1 = v1; key2 = v2'（仅在更新自定义规则时）
     * @param remediationFnBaseEffort    非必填    规则的补救功能的基本功
     * @param remediationFnType          非必填    规则的补救功能的类型
     * @param remediationFyGapMultiplier 非必填    规则补救功能的差距乘数
     * @param severity                   非必填    规则严重性（仅在更新自定义规则时）
     * @param status                     非必填    规则状态（仅在更新自定义规则时）
     * @param tags                       非必填    要设置的可选逗号分隔标签列表。使用空白值删除当前标签。如果未设置参数，则标签不会更改。
     * @return
     */
    @RequestLine("GET /api/rules/update")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    UpdateRuleDTO updateRule(
            @Param("key") String key,
            @Param("markdown_description") String markdownDescription,
            @Param("markdown_note") String markdownNote,
            @Param("name") String name,
            @Param("params") String params,
            @Param("remediation_fn_base_effort") String remediationFnBaseEffort,
            @Param("remediation_fn_type") String remediationFnType,
            @Param("remediation_fy_gap_multiplier") String remediationFyGapMultiplier,
            @Param("severity") String severity,
            @Param("status") String status,
            @Param("tags") String tags);

}
