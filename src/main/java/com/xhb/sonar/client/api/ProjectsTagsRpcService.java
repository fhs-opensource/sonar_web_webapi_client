package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.GetProjectTargsDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface ProjectsTagsRpcService {


    /**
     * 搜索全部 tags
     *
     * @param pageSize    非必填 页面大小。必须大于0且小于或等于100
     * @param limitSearch 非必填 将搜索限制为包含提供的字符串的标签。
     * @return
     */
    @RequestLine("GET api/project_tags/search")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    GetProjectTargsDTO getProjectTargs(
            @Param("ps") Integer pageSize,
            @Param("q") String limitSearch
    );


    /**
     * 在项目上设置标签。
     * 需要以下权限：对指定项目的“管理”权限
     *
     * @param projectKey
     * @param tags       逗号分隔的标签列表
     */
    @RequestLine("POST api/project_tags/set")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    void setTagsToProject(
            @Param("project") Integer projectKey,
            @Param("tags") String tags
    );
}
