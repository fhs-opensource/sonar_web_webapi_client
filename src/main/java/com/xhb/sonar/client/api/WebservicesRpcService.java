package com.xhb.sonar.client.api;

import com.xhb.sonar.client.dto.GetWebServiceDTO;
import com.xhb.sonar.client.dto.ResponseExampleDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface WebservicesRpcService {

    /**
     * 列出网络服务
     *
     * @param includeInternals 非必填 包括仅用于内部使用的Web服务。无法保证它们的前向兼容性
     * @return
     */
    @RequestLine("GET api/webservices/list")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    GetWebServiceDTO getWebServiceList(
            @Param("include_internals") Boolean includeInternals
    );


    /**
     * 显示Web服务响应示例
     *
     * @param action     Web服务的动作
     * @param controller Web服务的控制器
     * @return
     */
    @RequestLine("GET api/webservices/response_example")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    ResponseExampleDTO getResponseExample(
            @Param("action") String action,
            @Param("controller") String controller
    );
}
