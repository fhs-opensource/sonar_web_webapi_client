/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-28 17:5:10
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class SetTagsDTO {

    private Issue issue;
    private List<IssuesComponents> components;
    private List<Rules> rules;
    private List<com.xhb.sonar.client.dto.Users> users;
    public void setIssue(Issue issue) {
         this.issue = issue;
     }
     public Issue getIssue() {
         return issue;
     }

    public void setComponents(List<IssuesComponents> components) {
         this.components = components;
     }
     public List<IssuesComponents> getComponents() {
         return components;
     }

    public void setRules(List<Rules> rules) {
         this.rules = rules;
     }
     public List<Rules> getRules() {
         return rules;
     }

    public void setUsers(List<com.xhb.sonar.client.dto.Users> users) {
         this.users = users;
     }
     public List<Users> getUsers() {
         return users;
     }

}