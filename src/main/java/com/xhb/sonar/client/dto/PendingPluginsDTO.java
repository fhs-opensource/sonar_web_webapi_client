package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 14:49:25
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class PendingPluginsDTO {

    private List<Installing> installing;
    private List<Updating> updating;
    private List<Removing> removing;
    public void setInstalling(List<Installing> installing) {
         this.installing = installing;
     }
     public List<Installing> getInstalling() {
         return installing;
     }

    public void setUpdating(List<Updating> updating) {
         this.updating = updating;
     }
     public List<Updating> getUpdating() {
         return updating;
     }

    public void setRemoving(List<Removing> removing) {
         this.removing = removing;
     }
     public List<Removing> getRemoving() {
         return removing;
     }

}