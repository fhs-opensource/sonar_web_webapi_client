package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-09-29 17:58:9
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Profiles {

    private String key;
    private String name;
    private String language;
    private String languageName;
    private boolean isInherited;
    private boolean isBuiltIn;
    private int activeRuleCount;
    private int activeDeprecatedRuleCount;
    private boolean isDefault;
    private Date ruleUpdatedAt;
    private Date lastUsed;
    private Actions actions;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setLanguage(String language) {
         this.language = language;
     }
     public String getLanguage() {
         return language;
     }

    public void setLanguageName(String languageName) {
         this.languageName = languageName;
     }
     public String getLanguageName() {
         return languageName;
     }

    public void setIsInherited(boolean isInherited) {
         this.isInherited = isInherited;
     }
     public boolean getIsInherited() {
         return isInherited;
     }

    public void setIsBuiltIn(boolean isBuiltIn) {
         this.isBuiltIn = isBuiltIn;
     }
     public boolean getIsBuiltIn() {
         return isBuiltIn;
     }

    public void setActiveRuleCount(int activeRuleCount) {
         this.activeRuleCount = activeRuleCount;
     }
     public int getActiveRuleCount() {
         return activeRuleCount;
     }

    public void setActiveDeprecatedRuleCount(int activeDeprecatedRuleCount) {
         this.activeDeprecatedRuleCount = activeDeprecatedRuleCount;
     }
     public int getActiveDeprecatedRuleCount() {
         return activeDeprecatedRuleCount;
     }

    public void setIsDefault(boolean isDefault) {
         this.isDefault = isDefault;
     }
     public boolean getIsDefault() {
         return isDefault;
     }

    public void setRuleUpdatedAt(Date ruleUpdatedAt) {
         this.ruleUpdatedAt = ruleUpdatedAt;
     }
     public Date getRuleUpdatedAt() {
         return ruleUpdatedAt;
     }

    public void setLastUsed(Date lastUsed) {
         this.lastUsed = lastUsed;
     }
     public Date getLastUsed() {
         return lastUsed;
     }

    public void setActions(Actions actions) {
         this.actions = actions;
     }
     public Actions getActions() {
         return actions;
     }

}