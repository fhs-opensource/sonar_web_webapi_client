package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-29 17:39:47
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Profile {

    private String key;
    private String name;
    private String parent;
    private int activeRuleCount;
    private int overridingRuleCount;
    private boolean isBuiltIn;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setParent(String parent) {
         this.parent = parent;
     }
     public String getParent() {
         return parent;
     }

    public void setActiveRuleCount(int activeRuleCount) {
         this.activeRuleCount = activeRuleCount;
     }
     public int getActiveRuleCount() {
         return activeRuleCount;
     }

    public void setOverridingRuleCount(int overridingRuleCount) {
         this.overridingRuleCount = overridingRuleCount;
     }
     public int getOverridingRuleCount() {
         return overridingRuleCount;
     }

    public void setIsBuiltIn(boolean isBuiltIn) {
         this.isBuiltIn = isBuiltIn;
     }
     public boolean getIsBuiltIn() {
         return isBuiltIn;
     }

}