package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-30 9:30:21
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Project {

    private String key;
    private String name;
    private String qualifier;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setQualifier(String qualifier) {
         this.qualifier = qualifier;
     }
     public String getQualifier() {
         return qualifier;
     }

}