package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-29 17:32:25
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class QualityProfileFormatDTO {

    private List<Exporters> exporters;
    public void setExporters(List<Exporters> exporters) {
         this.exporters = exporters;
     }
     public List<Exporters> getExporters() {
         return exporters;
     }

}