/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;
import java.util.List;

/**
 * Auto-generated: 2020-09-28 18:49:44
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class RuleTagsDTO {

    private List<String> tags;
    public void setTags(List<String> tags) {
         this.tags = tags;
     }
     public List<String> getTags() {
         return tags;
     }

}