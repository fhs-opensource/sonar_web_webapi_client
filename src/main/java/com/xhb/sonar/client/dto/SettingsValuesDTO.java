package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 15:48:2
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class SettingsValuesDTO {

    private List<Settings> settings;
    public void setSettings(List<Settings> settings) {
         this.settings = settings;
     }
     public List<Settings> getSettings() {
         return settings;
     }

}