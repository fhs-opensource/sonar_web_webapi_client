package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-10-09 11:0:28
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class  ComputeEngineTaskDTO {

    private Task task;
    public void setTask(Task task) {
         this.task = task;
     }
     public Task getTask() {
         return task;
     }

}