package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-10-09 17:50:56
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class WebhookDeliveryDTO {

    private Delivery delivery;
    public void setDelivery(Delivery delivery) {
         this.delivery = delivery;
     }
     public Delivery getDelivery() {
         return delivery;
     }

}