package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-10-09 16:19:39
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class RequireUpdate {

    private String key;
    private String name;
    private String category;
    private String description;
    private String license;
    private String organizationName;
    private String organizationUrl;
    private boolean editionBundled;
    private String termsAndConditionsUrl;
    private Date version;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setCategory(String category) {
         this.category = category;
     }
     public String getCategory() {
         return category;
     }

    public void setDescription(String description) {
         this.description = description;
     }
     public String getDescription() {
         return description;
     }

    public void setLicense(String license) {
         this.license = license;
     }
     public String getLicense() {
         return license;
     }

    public void setOrganizationName(String organizationName) {
         this.organizationName = organizationName;
     }
     public String getOrganizationName() {
         return organizationName;
     }

    public void setOrganizationUrl(String organizationUrl) {
         this.organizationUrl = organizationUrl;
     }
     public String getOrganizationUrl() {
         return organizationUrl;
     }

    public void setEditionBundled(boolean editionBundled) {
         this.editionBundled = editionBundled;
     }
     public boolean getEditionBundled() {
         return editionBundled;
     }

    public void setTermsAndConditionsUrl(String termsAndConditionsUrl) {
         this.termsAndConditionsUrl = termsAndConditionsUrl;
     }
     public String getTermsAndConditionsUrl() {
         return termsAndConditionsUrl;
     }

    public void setVersion(Date version) {
         this.version = version;
     }
     public Date getVersion() {
         return version;
     }

}