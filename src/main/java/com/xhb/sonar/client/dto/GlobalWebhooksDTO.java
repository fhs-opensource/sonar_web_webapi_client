package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 17:57:23
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class GlobalWebhooksDTO {

    private List<Webhooks> webhooks;
    public void setWebhooks(List<Webhooks> webhooks) {
         this.webhooks = webhooks;
     }
     public List<Webhooks> getWebhooks() {
         return webhooks;
     }

}