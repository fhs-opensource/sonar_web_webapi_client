/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;
import java.util.List;

/**
 * Auto-generated: 2020-09-28 18:40:20
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Facets {

    private String name;
    private List<Values> values;
    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setValues(List<Values> values) {
         this.values = values;
     }
     public List<Values> getValues() {
         return values;
     }

}