package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-10-09 9:39:28
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ResponseExampleDTO {

    private String format;
    private String example;
    public void setFormat(String format) {
         this.format = format;
     }
     public String getFormat() {
         return format;
     }

    public void setExample(String example) {
         this.example = example;
     }
     public String getExample() {
         return example;
     }

}