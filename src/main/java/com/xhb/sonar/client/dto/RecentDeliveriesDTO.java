package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 17:47:6
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class RecentDeliveriesDTO {

    private Paging paging;
    private List<Deliveries> deliveries;
    public void setPaging(Paging paging) {
         this.paging = paging;
     }
     public Paging getPaging() {
         return paging;
     }

    public void setDeliveries(List<Deliveries> deliveries) {
         this.deliveries = deliveries;
     }
     public List<Deliveries> getDeliveries() {
         return deliveries;
     }

}