/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-28 17:5:10
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class TextRange {

    private int startLine;
    private int endLine;
    private int startOffset;
    private int endOffset;
    public void setStartLine(int startLine) {
         this.startLine = startLine;
     }
     public int getStartLine() {
         return startLine;
     }

    public void setEndLine(int endLine) {
         this.endLine = endLine;
     }
     public int getEndLine() {
         return endLine;
     }

    public void setStartOffset(int startOffset) {
         this.startOffset = startOffset;
     }
     public int getStartOffset() {
         return startOffset;
     }

    public void setEndOffset(int endOffset) {
         this.endOffset = endOffset;
     }
     public int getEndOffset() {
         return endOffset;
     }

}