package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-30 15:0:7
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class GetLinkOfProjectDTO {

    private List<Links> links;
    public void setLinks(List<Links> links) {
         this.links = links;
     }
     public List<Links> getLinks() {
         return links;
     }

}