package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-10-09 10:0:25
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class CheckCredentialsDTO {

    private boolean valid;
    public void setValid(boolean valid) {
         this.valid = valid;
     }
     public boolean getValid() {
         return valid;
     }

}