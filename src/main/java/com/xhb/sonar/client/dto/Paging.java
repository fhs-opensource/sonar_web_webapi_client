package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-10-09 17:47:6
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Paging {

    private int pageIndex;
    private int pageSize;
    private int total;
    public void setPageIndex(int pageIndex) {
         this.pageIndex = pageIndex;
     }
     public int getPageIndex() {
         return pageIndex;
     }

    public void setPageSize(int pageSize) {
         this.pageSize = pageSize;
     }
     public int getPageSize() {
         return pageSize;
     }

    public void setTotal(int total) {
         this.total = total;
     }
     public int getTotal() {
         return total;
     }

}