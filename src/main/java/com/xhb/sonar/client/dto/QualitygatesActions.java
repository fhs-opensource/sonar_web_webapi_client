package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-30 13:45:34
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class QualitygatesActions {

    private boolean rename;
    private boolean setAsDefault;
    private boolean copy;
    private boolean associateProjects;
    private boolean delete;
    private boolean manageConditions;

    public void setRename(boolean rename) {
        this.rename = rename;
    }

    public boolean getRename() {
        return rename;
    }

    public void setSetAsDefault(boolean setAsDefault) {
        this.setAsDefault = setAsDefault;
    }

    public boolean getSetAsDefault() {
        return setAsDefault;
    }

    public void setCopy(boolean copy) {
        this.copy = copy;
    }

    public boolean getCopy() {
        return copy;
    }

    public void setAssociateProjects(boolean associateProjects) {
        this.associateProjects = associateProjects;
    }

    public boolean getAssociateProjects() {
        return associateProjects;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public boolean getDelete() {
        return delete;
    }

    public void setManageConditions(boolean manageConditions) {
        this.manageConditions = manageConditions;
    }

    public boolean getManageConditions() {
        return manageConditions;
    }

}