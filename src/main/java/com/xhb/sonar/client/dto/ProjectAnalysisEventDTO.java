package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-30 15:23:3
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ProjectAnalysisEventDTO {

    private Event event;
    public void setEvent(Event event) {
         this.event = event;
     }
     public Event getEvent() {
         return event;
     }

}