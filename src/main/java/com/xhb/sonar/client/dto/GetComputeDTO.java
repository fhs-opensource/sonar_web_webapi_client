package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 11:16:32
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class GetComputeDTO {

    private Component component;
    private List<Ancestors> ancestors;
    public void setComponent(Component component) {
         this.component = component;
     }
     public Component getComponent() {
         return component;
     }

    public void setAncestors(List<Ancestors> ancestors) {
         this.ancestors = ancestors;
     }
     public List<Ancestors> getAncestors() {
         return ancestors;
     }

}