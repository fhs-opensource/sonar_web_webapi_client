package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 14:1:5
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class SearchHistoryDTO {

    private Paging paging;
    private List<Measures> measures;
    public void setPaging(Paging paging) {
         this.paging = paging;
     }
     public Paging getPaging() {
         return paging;
     }

    public void setMeasures(List<Measures> measures) {
         this.measures = measures;
     }
     public List<Measures> getMeasures() {
         return measures;
     }

}