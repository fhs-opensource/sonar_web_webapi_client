package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-10-09 17:50:56
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Delivery {

    private String id;
    private String componentKey;
    private Date ceTaskId;
    private String name;
    private String url;
    private Date at;
    private boolean success;
    private int httpStatus;
    private int durationMs;
    private String payload;
    public void setId(String id) {
         this.id = id;
     }
     public String getId() {
         return id;
     }

    public void setComponentKey(String componentKey) {
         this.componentKey = componentKey;
     }
     public String getComponentKey() {
         return componentKey;
     }

    public void setCeTaskId(Date ceTaskId) {
         this.ceTaskId = ceTaskId;
     }
     public Date getCeTaskId() {
         return ceTaskId;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setUrl(String url) {
         this.url = url;
     }
     public String getUrl() {
         return url;
     }

    public void setAt(Date at) {
         this.at = at;
     }
     public Date getAt() {
         return at;
     }

    public void setSuccess(boolean success) {
         this.success = success;
     }
     public boolean getSuccess() {
         return success;
     }

    public void setHttpStatus(int httpStatus) {
         this.httpStatus = httpStatus;
     }
     public int getHttpStatus() {
         return httpStatus;
     }

    public void setDurationMs(int durationMs) {
         this.durationMs = durationMs;
     }
     public int getDurationMs() {
         return durationMs;
     }

    public void setPayload(String payload) {
         this.payload = payload;
     }
     public String getPayload() {
         return payload;
     }

}