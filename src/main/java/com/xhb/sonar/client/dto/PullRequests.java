package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-09-30 14:37:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class PullRequests {

    private String key;
    private String title;
    private String branch;
    private String base;
    private Status status;
    private Date analysisDate;
    private Date url;
    private String target;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setTitle(String title) {
         this.title = title;
     }
     public String getTitle() {
         return title;
     }

    public void setBranch(String branch) {
         this.branch = branch;
     }
     public String getBranch() {
         return branch;
     }

    public void setBase(String base) {
         this.base = base;
     }
     public String getBase() {
         return base;
     }

    public void setStatus(Status status) {
         this.status = status;
     }
     public Status getStatus() {
         return status;
     }

    public void setAnalysisDate(Date analysisDate) {
         this.analysisDate = analysisDate;
     }
     public Date getAnalysisDate() {
         return analysisDate;
     }

    public void setUrl(Date url) {
         this.url = url;
     }
     public Date getUrl() {
         return url;
     }

    public void setTarget(String target) {
         this.target = target;
     }
     public String getTarget() {
         return target;
     }

}