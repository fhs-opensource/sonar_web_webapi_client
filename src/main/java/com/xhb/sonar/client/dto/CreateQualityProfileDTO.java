package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-29 17:20:15
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class CreateQualityProfileDTO {

    private Profile profile;
    private List<String> warnings;
    public void setProfile(Profile profile) {
         this.profile = profile;
     }
     public Profile getProfile() {
         return profile;
     }

    public void setWarnings(List<String> warnings) {
         this.warnings = warnings;
     }
     public List<String> getWarnings() {
         return warnings;
     }

}