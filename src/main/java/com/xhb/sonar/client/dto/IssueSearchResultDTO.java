/**
 * Copyright 2020 bejson.com
 */
package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-27 20:19:15
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class IssueSearchResultDTO {

    private Paging paging;
    private List<Issues> issues;
    private List<IssuesComponents> components;
    private List<Rules> rules;
    private List<Users> users;

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setIssues(List<Issues> issues) {
        this.issues = issues;
    }

    public List<Issues> getIssues() {
        return issues;
    }

    public void setComponents(List<IssuesComponents> components) {
        this.components = components;
    }

    public List<IssuesComponents> getComponents() {
        return components;
    }

    public void setRules(List<Rules> rules) {
        this.rules = rules;
    }

    public List<Rules> getRules() {
        return rules;
    }

    public void setUsers(List<Users> users) {
        this.users = users;
    }

    public List<Users> getUsers() {
        return users;
    }

}