package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-30 14:48:47
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class NewProjectLinkDTO {

    private Link link;
    public void setLink(Link link) {
         this.link = link;
     }
     public Link getLink() {
         return link;
     }

}