/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;
import java.util.List;

/**
 * Auto-generated: 2020-09-28 18:46:59
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class GetRuleInformationDTO {

    private Rule rule;
    private List<Actives> actives;
    public void setRule(Rule rule) {
         this.rule = rule;
     }
     public Rule getRule() {
         return rule;
     }

    public void setActives(List<Actives> actives) {
         this.actives = actives;
     }
     public List<Actives> getActives() {
         return actives;
     }

}