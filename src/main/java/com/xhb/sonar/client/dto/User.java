/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;
import java.util.List;

/**
 * Auto-generated: 2020-09-28 18:20:57
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class User {

    private String login;
    private String name;
    private String email;
    private List<String> scmAccounts;
    private boolean active;
    private boolean local;
    public void setLogin(String login) {
         this.login = login;
     }
     public String getLogin() {
         return login;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setEmail(String email) {
         this.email = email;
     }
     public String getEmail() {
         return email;
     }

    public void setScmAccounts(List<String> scmAccounts) {
         this.scmAccounts = scmAccounts;
     }
     public List<String> getScmAccounts() {
         return scmAccounts;
     }

    public void setActive(boolean active) {
         this.active = active;
     }
     public boolean getActive() {
         return active;
     }

    public void setLocal(boolean local) {
         this.local = local;
     }
     public boolean getLocal() {
         return local;
     }

}