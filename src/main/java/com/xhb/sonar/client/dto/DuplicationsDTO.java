package com.xhb.sonar.client.dto;

import java.nio.file.Files;
import java.util.List;

/**
 * Auto-generated: 2020-10-09 11:43:31
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class DuplicationsDTO {

    private List<Duplications> duplications;
    private Files files;
    public void setDuplications(List<Duplications> duplications) {
         this.duplications = duplications;
     }
     public List<Duplications> getDuplications() {
         return duplications;
     }

    public void setFiles(Files files) {
         this.files = files;
     }
     public Files getFiles() {
         return files;
     }

}