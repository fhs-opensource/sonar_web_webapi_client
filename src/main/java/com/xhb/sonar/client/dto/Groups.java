/**
  * Copyright 2020 jb51.net 
  */
package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-28 18:6:33
 *
 * @author jb51.net (i@jb51.net)
 * @website http://tools.jb51.net/code/json2javabean
 */
public class Groups {

    private int id;
    private String name;
    private String description;
    private boolean selected;
    private boolean _default;
    public void setId(int id) {
         this.id = id;
     }
     public int getId() {
         return id;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setDescription(String description) {
         this.description = description;
     }
     public String getDescription() {
         return description;
     }

    public void setSelected(boolean selected) {
         this.selected = selected;
     }
     public boolean getSelected() {
         return selected;
     }

    public void setDefault(boolean _default) {
         this._default = _default;
     }
     public boolean getDefault() {
         return _default;
     }

}