/**
 * Copyright 2020 bejson.com
 */
package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-28 18:20:57
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class UpdateUserDTO {

    private User user;

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

}