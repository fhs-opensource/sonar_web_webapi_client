package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 10:43:30
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class SearchTasksDTO {

    private List<Tasks> tasks;

    public void setTasks(List<Tasks> tasks) {
        this.tasks = tasks;
    }

    public List<Tasks> getTasks() {
        return tasks;
    }

}