package com.xhb.sonar.client.dto;

import java.util.List;
import java.util.Date;

/**
 * Auto-generated: 2020-10-09 16:19:39
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class UpgradesSystemDTO {

    private List<Upgrades> upgrades;
    private Date updateCenterRefresh;
    public void setUpgrades(List<Upgrades> upgrades) {
         this.upgrades = upgrades;
     }
     public List<Upgrades> getUpgrades() {
         return upgrades;
     }

    public void setUpdateCenterRefresh(Date updateCenterRefresh) {
         this.updateCenterRefresh = updateCenterRefresh;
     }
     public Date getUpdateCenterRefresh() {
         return updateCenterRefresh;
     }

}