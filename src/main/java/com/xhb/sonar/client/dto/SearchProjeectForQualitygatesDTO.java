package com.xhb.sonar.client.dto;


import java.util.List;

/**
 * Auto-generated: 2020-09-30 11:49:9
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class SearchProjeectForQualitygatesDTO {

    private com.xhb.sonar.client.dto.Paging paging;
    private List<Results> results;
    public void setPaging(com.xhb.sonar.client.dto.Paging paging) {
         this.paging = paging;
     }
     public Paging getPaging() {
         return paging;
     }

    public void setResults(List<Results> results) {
         this.results = results;
     }
     public List<Results> getResults() {
         return results;
     }

}