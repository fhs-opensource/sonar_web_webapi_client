package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-10-09 14:21:42
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Notifications {

    private String channel;
    private String type;
    public void setChannel(String channel) {
         this.channel = channel;
     }
     public String getChannel() {
         return channel;
     }

    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

}