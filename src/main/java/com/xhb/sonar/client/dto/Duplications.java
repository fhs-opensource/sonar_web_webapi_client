package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 11:43:31
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Duplications {

    private List<Blocks> blocks;
    public void setBlocks(List<Blocks> blocks) {
         this.blocks = blocks;
     }
     public List<Blocks> getBlocks() {
         return blocks;
     }

}