package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-30 10:41:5
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ProjectBranchesDTO {

    private List<Branches> branches;
    public void setBranches(List<Branches> branches) {
         this.branches = branches;
     }
     public List<Branches> getBranches() {
         return branches;
     }

}