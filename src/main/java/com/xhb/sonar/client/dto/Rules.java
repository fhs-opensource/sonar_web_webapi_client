/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;

import java.util.Date;
import java.util.List;

/**
 * Auto-generated: 2020-09-28 18:40:20
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Rules {

    private String key;
    private String repo;
    private String name;
    private Date createdAt;
    private Date updatedAt;
    private String htmlDesc;
    private String severity;
    private String status;
    private String internalKey;
    private boolean isTemplate;
    private List<String> tags;
    private List<String> sysTags;
    private String lang;
    private String langName;
    private String scope;
    private boolean isExternal;
    private String type;
    private List<RulesParams> params;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setRepo(String repo) {
         this.repo = repo;
     }
     public String getRepo() {
         return repo;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setCreatedAt(Date createdAt) {
         this.createdAt = createdAt;
     }
     public Date getCreatedAt() {
         return createdAt;
     }

    public void setUpdatedAt(Date updatedAt) {
         this.updatedAt = updatedAt;
     }
     public Date getUpdatedAt() {
         return updatedAt;
     }

    public void setHtmlDesc(String htmlDesc) {
         this.htmlDesc = htmlDesc;
     }
     public String getHtmlDesc() {
         return htmlDesc;
     }

    public void setSeverity(String severity) {
         this.severity = severity;
     }
     public String getSeverity() {
         return severity;
     }

    public void setStatus(String status) {
         this.status = status;
     }
     public String getStatus() {
         return status;
     }

    public void setInternalKey(String internalKey) {
         this.internalKey = internalKey;
     }
     public String getInternalKey() {
         return internalKey;
     }

    public void setIsTemplate(boolean isTemplate) {
         this.isTemplate = isTemplate;
     }
     public boolean getIsTemplate() {
         return isTemplate;
     }

    public void setTags(List<String> tags) {
         this.tags = tags;
     }
     public List<String> getTags() {
         return tags;
     }

    public void setSysTags(List<String> sysTags) {
         this.sysTags = sysTags;
     }
     public List<String> getSysTags() {
         return sysTags;
     }

    public void setLang(String lang) {
         this.lang = lang;
     }
     public String getLang() {
         return lang;
     }

    public void setLangName(String langName) {
         this.langName = langName;
     }
     public String getLangName() {
         return langName;
     }

    public void setScope(String scope) {
         this.scope = scope;
     }
     public String getScope() {
         return scope;
     }

    public void setIsExternal(boolean isExternal) {
         this.isExternal = isExternal;
     }
     public boolean getIsExternal() {
         return isExternal;
     }

    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

    public void setParams(List<RulesParams> params) {
         this.params = params;
     }
     public List<RulesParams> getParams() {
         return params;
     }

}