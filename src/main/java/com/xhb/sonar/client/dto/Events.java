package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-30 15:29:57
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Events {

    private String key;
    private String category;
    private String name;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setCategory(String category) {
         this.category = category;
     }
     public String getCategory() {
         return category;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

}