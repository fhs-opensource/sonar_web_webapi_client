package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 9:35:16
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Actions {

    private String key;
    private String description;
    private String since;
    private String deprecatedSince;
    private boolean internal;
    private boolean post;
    private boolean hasResponseExample;
    private List<Changelog> changelog;
    private List<Params> params;

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setSince(String since) {
        this.since = since;
    }

    public String getSince() {
        return since;
    }

    public void setDeprecatedSince(String deprecatedSince) {
        this.deprecatedSince = deprecatedSince;
    }

    public String getDeprecatedSince() {
        return deprecatedSince;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    public boolean getInternal() {
        return internal;
    }

    public void setPost(boolean post) {
        this.post = post;
    }

    public boolean getPost() {
        return post;
    }

    public void setHasResponseExample(boolean hasResponseExample) {
        this.hasResponseExample = hasResponseExample;
    }

    public boolean getHasResponseExample() {
        return hasResponseExample;
    }

    public void setChangelog(List<Changelog> changelog) {
        this.changelog = changelog;
    }

    public List<Changelog> getChangelog() {
        return changelog;
    }

    public void setParams(List<Params> params) {
        this.params = params;
    }

    public List<Params> getParams() {
        return params;
    }

}