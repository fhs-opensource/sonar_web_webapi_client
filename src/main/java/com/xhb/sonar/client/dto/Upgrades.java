package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-10-09 16:19:39
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Upgrades {

    private Date version;
    private String description;
    private Date releaseDate;
    private String changeLogUrl;
    private String downloadUrl;
    private Plugins plugins;
    public void setVersion(Date version) {
         this.version = version;
     }
     public Date getVersion() {
         return version;
     }

    public void setDescription(String description) {
         this.description = description;
     }
     public String getDescription() {
         return description;
     }

    public void setReleaseDate(Date releaseDate) {
         this.releaseDate = releaseDate;
     }
     public Date getReleaseDate() {
         return releaseDate;
     }

    public void setChangeLogUrl(String changeLogUrl) {
         this.changeLogUrl = changeLogUrl;
     }
     public String getChangeLogUrl() {
         return changeLogUrl;
     }

    public void setDownloadUrl(String downloadUrl) {
         this.downloadUrl = downloadUrl;
     }
     public String getDownloadUrl() {
         return downloadUrl;
     }

    public void setPlugins(Plugins plugins) {
         this.plugins = plugins;
     }
     public Plugins getPlugins() {
         return plugins;
     }

}