/**
 * Copyright 2020 bejson.com
 */
package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-28 18:29:29
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class CreateRulesDTO {

    private Rule rule;

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    public Rule getRule() {
        return rule;
    }

}