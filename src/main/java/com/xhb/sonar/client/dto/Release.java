package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-10-09 14:57:12
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Release {

    private String version;
    private Date date;
    private String description;
    private String changeLogUrl;
    public void setVersion(String version) {
         this.version = version;
     }
     public String getVersion() {
         return version;
     }

    public void setDate(Date date) {
         this.date = date;
     }
     public Date getDate() {
         return date;
     }

    public void setDescription(String description) {
         this.description = description;
     }
     public String getDescription() {
         return description;
     }

    public void setChangeLogUrl(String changeLogUrl) {
         this.changeLogUrl = changeLogUrl;
     }
     public String getChangeLogUrl() {
         return changeLogUrl;
     }

}