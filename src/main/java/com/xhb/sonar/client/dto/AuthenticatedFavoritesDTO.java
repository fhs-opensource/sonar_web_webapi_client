package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 11:54:32
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class AuthenticatedFavoritesDTO {

    private Paging paging;
    private List<Favorites> favorites;
    public void setPaging(Paging paging) {
         this.paging = paging;
     }
     public Paging getPaging() {
         return paging;
     }

    public void setFavorites(List<Favorites> favorites) {
         this.favorites = favorites;
     }
     public List<Favorites> getFavorites() {
         return favorites;
     }

}