package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 14:1:5
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Measures {

    private String metric;
    private List<History> history;
    public void setMetric(String metric) {
         this.metric = metric;
     }
     public String getMetric() {
         return metric;
     }

    public void setHistory(List<History> history) {
         this.history = history;
     }
     public List<History> getHistory() {
         return history;
     }

}