package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 14:57:12
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Updates {

    private Release release;
    private String status;
    private List<String> requires;
    public void setRelease(Release release) {
         this.release = release;
     }
     public Release getRelease() {
         return release;
     }

    public void setStatus(String status) {
         this.status = status;
     }
     public String getStatus() {
         return status;
     }

    public void setRequires(List<String> requires) {
         this.requires = requires;
     }
     public List<String> getRequires() {
         return requires;
     }

}