/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-28 18:52:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class RulesParams {

    private String key;
    private String htmlDesc;
    private String defaultValue;
    private String type;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setHtmlDesc(String htmlDesc) {
         this.htmlDesc = htmlDesc;
     }
     public String getHtmlDesc() {
         return htmlDesc;
     }

    public void setDefaultValue(String defaultValue) {
         this.defaultValue = defaultValue;
     }
     public String getDefaultValue() {
         return defaultValue;
     }

    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

}