package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-10-09 9:35:16
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Params {

    private String key;
    private boolean required;
    private boolean internal;
    private int maximumValue;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setRequired(boolean required) {
         this.required = required;
     }
     public boolean getRequired() {
         return required;
     }

    public void setInternal(boolean internal) {
         this.internal = internal;
     }
     public boolean getInternal() {
         return internal;
     }

    public void setMaximumValue(int maximumValue) {
         this.maximumValue = maximumValue;
     }
     public int getMaximumValue() {
         return maximumValue;
     }

}