/**
 * Copyright 2020 bejson.com
 */
package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-09-29 10:6:32
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class GenerateUserTokenDTO {

    private String login;
    private String name;
    private Date createdAt;
    private String token;

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

}