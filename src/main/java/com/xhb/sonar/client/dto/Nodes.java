package com.xhb.sonar.client.dto;

import java.util.Date;
import java.util.List;

/**
 * Auto-generated: 2020-10-09 16:4:33
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Nodes {

    private Date name;
    private String type;
    private String host;
    private int port;
    private Date startedAt;
    private String health;
    private List<Causes> causes;
    public void setName(Date name) {
         this.name = name;
     }
     public Date getName() {
         return name;
     }

    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

    public void setHost(String host) {
         this.host = host;
     }
     public String getHost() {
         return host;
     }

    public void setPort(int port) {
         this.port = port;
     }
     public int getPort() {
         return port;
     }

    public void setStartedAt(Date startedAt) {
         this.startedAt = startedAt;
     }
     public Date getStartedAt() {
         return startedAt;
     }

    public void setHealth(String health) {
         this.health = health;
     }
     public String getHealth() {
         return health;
     }

    public void setCauses(List<Causes> causes) {
         this.causes = causes;
     }
     public List<Causes> getCauses() {
         return causes;
     }

}