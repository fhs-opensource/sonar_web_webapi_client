package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 15:38:31
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class DefinitionsListDTO {

    private List<Definitions> definitions;
    public void setDefinitions(List<Definitions> definitions) {
         this.definitions = definitions;
     }
     public List<Definitions> getDefinitions() {
         return definitions;
     }

}