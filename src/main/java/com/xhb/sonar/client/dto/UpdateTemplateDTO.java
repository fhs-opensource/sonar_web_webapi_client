package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-29 15:21:36
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class UpdateTemplateDTO {

    private PermissionTemplate permissionTemplate;
    public void setPermissionTemplate(PermissionTemplate permissionTemplate) {
         this.permissionTemplate = permissionTemplate;
     }
     public PermissionTemplate getPermissionTemplate() {
         return permissionTemplate;
     }

}