package com.xhb.sonar.client.dto;
import java.util.List;

/**
 * Auto-generated: 2020-09-28 16:17:16
 *
 * @author http://www.itjson.com 
 * @website http://www.itjson.com/itjson/json2java.html 
 */
public class MetricsSearchResult {

    private List<Metrics> metrics;
    private int total;
    private int p;
    private int ps;
    public void setMetrics(List<Metrics> metrics) {
         this.metrics = metrics;
     }
     public List<Metrics> getMetrics() {
         return metrics;
     }

    public void setTotal(int total) {
         this.total = total;
     }
     public int getTotal() {
         return total;
     }

    public void setP(int p) {
         this.p = p;
     }
     public int getP() {
         return p;
     }

    public void setPs(int ps) {
         this.ps = ps;
     }
     public int getPs() {
         return ps;
     }

}