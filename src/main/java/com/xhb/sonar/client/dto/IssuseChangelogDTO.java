/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-28 16:34:33
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class IssuseChangelogDTO {

    private int total;
    private int success;
    private int ignored;
    private int failures;
    public void setTotal(int total) {
         this.total = total;
     }
     public int getTotal() {
         return total;
     }

    public void setSuccess(int success) {
         this.success = success;
     }
     public int getSuccess() {
         return success;
     }

    public void setIgnored(int ignored) {
         this.ignored = ignored;
     }
     public int getIgnored() {
         return ignored;
     }

    public void setFailures(int failures) {
         this.failures = failures;
     }
     public int getFailures() {
         return failures;
     }

}