/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;
import java.util.Date;

/**
 * Auto-generated: 2020-09-28 17:5:10
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Comments {

    private String key;
    private String login;
    private String htmlText;
    private String markdown;
    private boolean updatable;
    private Date createdAt;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setLogin(String login) {
         this.login = login;
     }
     public String getLogin() {
         return login;
     }

    public void setHtmlText(String htmlText) {
         this.htmlText = htmlText;
     }
     public String getHtmlText() {
         return htmlText;
     }

    public void setMarkdown(String markdown) {
         this.markdown = markdown;
     }
     public String getMarkdown() {
         return markdown;
     }

    public void setUpdatable(boolean updatable) {
         this.updatable = updatable;
     }
     public boolean getUpdatable() {
         return updatable;
     }

    public void setCreatedAt(Date createdAt) {
         this.createdAt = createdAt;
     }
     public Date getCreatedAt() {
         return createdAt;
     }

}