package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-10-10 10:15:23
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Components {

    private Date organization;
    private String id;
    private String key;
    private String qualifier;
    private String name;
    private String project;
    public void setOrganization(Date organization) {
         this.organization = organization;
     }
     public Date getOrganization() {
         return organization;
     }

    public void setId(String id) {
         this.id = id;
     }
     public String getId() {
         return id;
     }

    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setQualifier(String qualifier) {
         this.qualifier = qualifier;
     }
     public String getQualifier() {
         return qualifier;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setProject(String project) {
         this.project = project;
     }
     public String getProject() {
         return project;
     }

}