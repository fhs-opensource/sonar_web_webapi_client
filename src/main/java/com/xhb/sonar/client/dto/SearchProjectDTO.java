package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-30 10:0:48
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class SearchProjectDTO {

    private com.xhb.sonar.client.dto.Paging paging;
    private List<ProjectsComponents> components;

    public void setPaging(com.xhb.sonar.client.dto.Paging paging) {
        this.paging = paging;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setComponents(List<ProjectsComponents> components) {
        this.components = components;
    }

    public List<ProjectsComponents> getComponents() {
        return components;
    }

}