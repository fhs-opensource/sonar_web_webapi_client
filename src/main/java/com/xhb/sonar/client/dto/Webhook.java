package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-10-09 17:42:2
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Webhook {

    private String key;
    private String name;
    private String url;
    private String secret;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setUrl(String url) {
         this.url = url;
     }
     public String getUrl() {
         return url;
     }

    public void setSecret(String secret) {
         this.secret = secret;
     }
     public String getSecret() {
         return secret;
     }

}