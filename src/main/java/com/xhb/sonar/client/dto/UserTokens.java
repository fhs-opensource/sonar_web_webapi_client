package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-09-29 10:12:41
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class UserTokens {

    private String name;
    private Date createdAt;
    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setCreatedAt(Date createdAt) {
         this.createdAt = createdAt;
     }
     public Date getCreatedAt() {
         return createdAt;
     }

}