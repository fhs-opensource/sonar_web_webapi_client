package com.xhb.sonar.client.dto;


import java.util.List;

/**
 * Auto-generated: 2020-09-30 15:29:57
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class SearchAnalysesAndAttachedEventsDTO {

    private com.xhb.sonar.client.dto.Paging paging;
    private List<Analyses> analyses;
    public void setPaging(com.xhb.sonar.client.dto.Paging paging) {
         this.paging = paging;
     }
     public Paging getPaging() {
         return paging;
     }

    public void setAnalyses(List<Analyses> analyses) {
         this.analyses = analyses;
     }
     public List<Analyses> getAnalyses() {
         return analyses;
     }

}