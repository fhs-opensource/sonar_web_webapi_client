/**
 * Copyright 2020 jb51.net
 */
package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-28 18:6:33
 *
 * @author jb51.net (i@jb51.net)
 * @website http://tools.jb51.net/code/json2javabean
 */
public class GetGroupsDTO {

    private Paging paging;
    private List<Groups> groups;

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setGroups(List<Groups> groups) {
        this.groups = groups;
    }

    public List<Groups> getGroups() {
        return groups;
    }

}