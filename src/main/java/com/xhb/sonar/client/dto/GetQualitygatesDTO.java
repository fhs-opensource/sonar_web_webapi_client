package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-30 11:31:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class GetQualitygatesDTO {

    private List<Qualitygates> qualitygates;
    private boolean _default;
    private QualitygatesActions qualitygatesActions;
    public void setQualitygates(List<Qualitygates> qualitygates) {
         this.qualitygates = qualitygates;
     }
     public List<Qualitygates> getQualitygates() {
         return qualitygates;
     }

    public void set_default(boolean _default) {
         this._default = _default;
     }
     public boolean get_default() {
         return _default;
     }

    public void setQualitygatesActions(QualitygatesActions qualitygatesActions) {
         this.qualitygatesActions = qualitygatesActions;
     }
     public QualitygatesActions getQualitygatesActions() {
         return qualitygatesActions;
     }

}