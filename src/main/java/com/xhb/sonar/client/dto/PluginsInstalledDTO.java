package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 14:45:8
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class PluginsInstalledDTO {

    private List<Plugins> plugins;
    public void setPlugins(List<Plugins> plugins) {
         this.plugins = plugins;
     }
     public List<Plugins> getPlugins() {
         return plugins;
     }

}