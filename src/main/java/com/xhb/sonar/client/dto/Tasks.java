package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-10-09 10:43:30
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Tasks {

    private Date organization;
    private String id;
    private String type;
    private String componentId;
    private String componentKey;
    private String componentName;
    private String componentQualifier;
    private String analysisId;
    private String status;
    private Date submittedAt;
    private String submitterLogin;
    private Date startedAt;
    private Date executedAt;
    private int executionTimeMs;
    private boolean logs;
    private boolean hasErrorStacktrace;
    private boolean hasScannerContext;

    public void setOrganization(Date organization) {
        this.organization = organization;
    }

    public Date getOrganization() {
        return organization;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }

    public String getComponentId() {
        return componentId;
    }

    public void setComponentKey(String componentKey) {
        this.componentKey = componentKey;
    }

    public String getComponentKey() {
        return componentKey;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentQualifier(String componentQualifier) {
        this.componentQualifier = componentQualifier;
    }

    public String getComponentQualifier() {
        return componentQualifier;
    }

    public void setAnalysisId(String analysisId) {
        this.analysisId = analysisId;
    }

    public String getAnalysisId() {
        return analysisId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setSubmittedAt(Date submittedAt) {
        this.submittedAt = submittedAt;
    }

    public Date getSubmittedAt() {
        return submittedAt;
    }

    public void setSubmitterLogin(String submitterLogin) {
        this.submitterLogin = submitterLogin;
    }

    public String getSubmitterLogin() {
        return submitterLogin;
    }

    public void setStartedAt(Date startedAt) {
        this.startedAt = startedAt;
    }

    public Date getStartedAt() {
        return startedAt;
    }

    public void setExecutedAt(Date executedAt) {
        this.executedAt = executedAt;
    }

    public Date getExecutedAt() {
        return executedAt;
    }

    public void setExecutionTimeMs(int executionTimeMs) {
        this.executionTimeMs = executionTimeMs;
    }

    public int getExecutionTimeMs() {
        return executionTimeMs;
    }

    public void setLogs(boolean logs) {
        this.logs = logs;
    }

    public boolean getLogs() {
        return logs;
    }

    public void setHasErrorStacktrace(boolean hasErrorStacktrace) {
        this.hasErrorStacktrace = hasErrorStacktrace;
    }

    public boolean getHasErrorStacktrace() {
        return hasErrorStacktrace;
    }

    public void setHasScannerContext(boolean hasScannerContext) {
        this.hasScannerContext = hasScannerContext;
    }

    public boolean getHasScannerContext() {
        return hasScannerContext;
    }

}