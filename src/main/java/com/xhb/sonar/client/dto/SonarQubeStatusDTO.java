package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-10-09 16:17:14
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class SonarQubeStatusDTO {

    private String id;
    private String version;
    private String status;
    public void setId(String id) {
         this.id = id;
     }
     public String getId() {
         return id;
     }

    public void setVersion(String version) {
         this.version = version;
     }
     public String getVersion() {
         return version;
     }

    public void setStatus(String status) {
         this.status = status;
     }
     public String getStatus() {
         return status;
     }

}