package com.xhb.sonar.client.dto;

import com.xhb.sonar.client.dto.Groups;
import com.xhb.sonar.client.dto.Paging;

import java.util.List;

/**
 * Auto-generated: 2020-09-29 10:50:41
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class UserGroupSearchResultDTO {

    private com.xhb.sonar.client.dto.Paging paging;
    private List<com.xhb.sonar.client.dto.Groups> groups;
    public void setPaging(com.xhb.sonar.client.dto.Paging paging) {
         this.paging = paging;
     }
     public Paging getPaging() {
         return paging;
     }

    public void setGroups(List<com.xhb.sonar.client.dto.Groups> groups) {
         this.groups = groups;
     }
     public List<Groups> getGroups() {
         return groups;
     }

}