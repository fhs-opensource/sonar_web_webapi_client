/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;
import java.util.List;

/**
 * Auto-generated: 2020-09-28 17:7:38
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class TagsDTO {

    private List<String> tags;
    public void setTags(List<String> tags) {
         this.tags = tags;
     }
     public List<String> getTags() {
         return tags;
     }

}