package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-10-09 13:49:37
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Metrics {

    private String key;
    private String name;
    private String description;
    private String domain;
    private String type;
    private boolean higherValuesAreBetter;
    private boolean qualitative;
    private boolean hidden;
    private boolean custom;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setDescription(String description) {
         this.description = description;
     }
     public String getDescription() {
         return description;
     }

    public void setDomain(String domain) {
         this.domain = domain;
     }
     public String getDomain() {
         return domain;
     }

    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

    public void setHigherValuesAreBetter(boolean higherValuesAreBetter) {
         this.higherValuesAreBetter = higherValuesAreBetter;
     }
     public boolean getHigherValuesAreBetter() {
         return higherValuesAreBetter;
     }

    public void setQualitative(boolean qualitative) {
         this.qualitative = qualitative;
     }
     public boolean getQualitative() {
         return qualitative;
     }

    public void setHidden(boolean hidden) {
         this.hidden = hidden;
     }
     public boolean getHidden() {
         return hidden;
     }

    public void setCustom(boolean custom) {
         this.custom = custom;
     }
     public boolean getCustom() {
         return custom;
     }

}