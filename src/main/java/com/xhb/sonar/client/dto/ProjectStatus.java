package com.xhb.sonar.client.dto;


import java.util.List;

/**
 * Auto-generated: 2020-09-30 11:40:49
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ProjectStatus {

    private String status;
    private boolean ignoredConditions;
    private List<Conditions> conditions;
    private List<com.xhb.sonar.client.dto.Periods> periods;
    public void setStatus(String status) {
         this.status = status;
     }
     public String getStatus() {
         return status;
     }

    public void setIgnoredConditions(boolean ignoredConditions) {
         this.ignoredConditions = ignoredConditions;
     }
     public boolean getIgnoredConditions() {
         return ignoredConditions;
     }

    public void setConditions(List<Conditions> conditions) {
         this.conditions = conditions;
     }
     public List<Conditions> getConditions() {
         return conditions;
     }

    public void setPeriods(List<com.xhb.sonar.client.dto.Periods> periods) {
         this.periods = periods;
     }
     public List<Periods> getPeriods() {
         return periods;
     }

}