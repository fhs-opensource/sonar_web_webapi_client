package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-29 17:8:26
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class QualityprofilesChangelogDTO {

    private int total;
    private int ps;
    private int p;
    private List<Events> events;
    public void setTotal(int total) {
         this.total = total;
     }
     public int getTotal() {
         return total;
     }

    public void setPs(int ps) {
         this.ps = ps;
     }
     public int getPs() {
         return ps;
     }

    public void setP(int p) {
         this.p = p;
     }
     public int getP() {
         return p;
     }

    public void setEvents(List<Events> events) {
         this.events = events;
     }
     public List<Events> getEvents() {
         return events;
     }

}