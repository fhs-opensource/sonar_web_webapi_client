/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-28 18:36:49
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Repositories {

    private String key;
    private String name;
    private String language;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setLanguage(String language) {
         this.language = language;
     }
     public String getLanguage() {
         return language;
     }

}