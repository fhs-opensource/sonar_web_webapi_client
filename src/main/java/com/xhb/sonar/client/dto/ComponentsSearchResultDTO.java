package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-28 15:19:0
 *
 * @author http://www.itjson.com 
 * @website http://www.itjson.com/itjson/json2java.html 
 */
public class ComponentsSearchResultDTO {

    private Paging paging;
    private List<IssuesComponents> components;
    public void setPaging(Paging paging) {
         this.paging = paging;
     }
     public Paging getPaging() {
         return paging;
     }

    public void setComponents(List<IssuesComponents> components) {
         this.components = components;
     }
     public List<IssuesComponents> getComponents() {
         return components;
     }

}