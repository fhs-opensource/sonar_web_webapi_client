/**
 * Copyright 2020 bejson.com
 */
package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-28 16:39:45
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class DeleteCommentDTO {

    private Issue issue;
    private List<IssuesComponents> components;
    private List<Rules> rules;
    private List<Users> users;

    public void setIssue(Issue issue) {
        this.issue = issue;
    }

    public Issue getIssue() {
        return issue;
    }

    public void setComponents(List<IssuesComponents> components) {
        this.components = components;
    }

    public List<IssuesComponents> getComponents() {
        return components;
    }

    public void setRules(List<Rules> rules) {
        this.rules = rules;
    }

    public List<Rules> getRules() {
        return rules;
    }

    public void setUsers(List<Users> users) {
        this.users = users;
    }

    public List<Users> getUsers() {
        return users;
    }

}