package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 11:22:5
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ComputeByChosenStrategyDTO {

    private Paging paging;
    private BaseComponent baseComponent;
    private List<Components> components;
    public void setPaging(Paging paging) {
         this.paging = paging;
     }
     public Paging getPaging() {
         return paging;
     }

    public void setBaseComponent(BaseComponent baseComponent) {
         this.baseComponent = baseComponent;
     }
     public BaseComponent getBaseComponent() {
         return baseComponent;
     }

    public void setComponents(List<Components> components) {
         this.components = components;
     }
     public List<Components> getComponents() {
         return components;
     }

}