package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-29 13:43:1
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class CreateTemplateDTO {

    private PermissionTemplate permissionTemplate;
    public void setPermissionTemplate(PermissionTemplate permissionTemplate) {
         this.permissionTemplate = permissionTemplate;
     }
     public PermissionTemplate getPermissionTemplate() {
         return permissionTemplate;
     }

}