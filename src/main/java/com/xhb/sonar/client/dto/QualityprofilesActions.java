package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-29 17:58:9
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class QualityprofilesActions {

    private boolean create;

    public void setCreate(boolean create) {
        this.create = create;
    }

    public boolean getCreate() {
        return create;
    }

}