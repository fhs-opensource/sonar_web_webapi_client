package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-10-12 9:27:36
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Sources {

    private int line;
    private String code;
    private String scmAuthor;
    private Date scmDate;
    private String scmRevision;
    private boolean isNew;
    public void setLine(int line) {
         this.line = line;
     }
     public int getLine() {
         return line;
     }

    public void setCode(String code) {
         this.code = code;
     }
     public String getCode() {
         return code;
     }

    public void setScmAuthor(String scmAuthor) {
         this.scmAuthor = scmAuthor;
     }
     public String getScmAuthor() {
         return scmAuthor;
     }

    public void setScmDate(Date scmDate) {
         this.scmDate = scmDate;
     }
     public Date getScmDate() {
         return scmDate;
     }

    public void setScmRevision(String scmRevision) {
         this.scmRevision = scmRevision;
     }
     public String getScmRevision() {
         return scmRevision;
     }

    public void setIsNew(boolean isNew) {
         this.isNew = isNew;
     }
     public boolean getIsNew() {
         return isNew;
     }

    @Override
    public String toString()
    {
        return "Sources{" +
                "line=" + line +
                ", code='" + code + '\'' +
                ", scmAuthor='" + scmAuthor + '\'' +
                ", scmDate=" + scmDate +
                ", scmRevision='" + scmRevision + '\'' +
                ", isNew=" + isNew +
                '}';
    }
}