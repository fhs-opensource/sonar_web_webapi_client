/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;
import java.util.List;
import java.util.Date;

/**
 * Auto-generated: 2020-09-28 17:5:10
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Issue {

    private String key;
    private String rule;
    private String severity;
    private String component;
    private String project;
    private int line;
    private TextRange textRange;
    private List<String> flows;
    private String status;
    private String message;
    private String effort;
    private String debt;
    private String assignee;
    private String author;
    private List<String> tags;
    private List<String> transitions;
    private List<String> actions;
    private List<Comments> comments;
    private Date creationDate;
    private Date updateDate;
    private String type;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setRule(String rule) {
         this.rule = rule;
     }
     public String getRule() {
         return rule;
     }

    public void setSeverity(String severity) {
         this.severity = severity;
     }
     public String getSeverity() {
         return severity;
     }

    public void setComponent(String component) {
         this.component = component;
     }
     public String getComponent() {
         return component;
     }

    public void setProject(String project) {
         this.project = project;
     }
     public String getProject() {
         return project;
     }

    public void setLine(int line) {
         this.line = line;
     }
     public int getLine() {
         return line;
     }

    public void setTextRange(TextRange textRange) {
         this.textRange = textRange;
     }
     public TextRange getTextRange() {
         return textRange;
     }

    public void setFlows(List<String> flows) {
         this.flows = flows;
     }
     public List<String> getFlows() {
         return flows;
     }

    public void setStatus(String status) {
         this.status = status;
     }
     public String getStatus() {
         return status;
     }

    public void setMessage(String message) {
         this.message = message;
     }
     public String getMessage() {
         return message;
     }

    public void setEffort(String effort) {
         this.effort = effort;
     }
     public String getEffort() {
         return effort;
     }

    public void setDebt(String debt) {
         this.debt = debt;
     }
     public String getDebt() {
         return debt;
     }

    public void setAssignee(String assignee) {
         this.assignee = assignee;
     }
     public String getAssignee() {
         return assignee;
     }

    public void setAuthor(String author) {
         this.author = author;
     }
     public String getAuthor() {
         return author;
     }

    public void setTags(List<String> tags) {
         this.tags = tags;
     }
     public List<String> getTags() {
         return tags;
     }

    public void setTransitions(List<String> transitions) {
         this.transitions = transitions;
     }
     public List<String> getTransitions() {
         return transitions;
     }

    public void setActions(List<String> actions) {
         this.actions = actions;
     }
     public List<String> getActions() {
         return actions;
     }

    public void setComments(List<Comments> comments) {
         this.comments = comments;
     }
     public List<Comments> getComments() {
         return comments;
     }

    public void setCreationDate(Date creationDate) {
         this.creationDate = creationDate;
     }
     public Date getCreationDate() {
         return creationDate;
     }

    public void setUpdateDate(Date updateDate) {
         this.updateDate = updateDate;
     }
     public Date getUpdateDate() {
         return updateDate;
     }

    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

}