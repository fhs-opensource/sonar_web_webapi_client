package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-12 9:27:36
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class SourcesLinesDTO {

    private List<Sources> sources;
    public void setSources(List<Sources> sources) {
         this.sources = sources;
     }
     public List<Sources> getSources() {
         return sources;
     }

}