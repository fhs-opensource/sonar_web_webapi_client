package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-29 10:12:41
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class UserTokenSearchResultDTO {

    private String login;
    private List<UserTokens> userTokens;
    public void setLogin(String login) {
         this.login = login;
     }
     public String getLogin() {
         return login;
     }

    public void setUserTokens(List<UserTokens> userTokens) {
         this.userTokens = userTokens;
     }
     public List<UserTokens> getUserTokens() {
         return userTokens;
     }

}