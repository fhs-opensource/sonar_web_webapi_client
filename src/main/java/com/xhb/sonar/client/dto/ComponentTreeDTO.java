package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 13:49:37
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ComponentTreeDTO {

    private Paging paging;
    private BaseComponent baseComponent;
    private List<IssuesComponents> components;
    private List<Metrics> metrics;
    private List<Periods> periods;
    public void setPaging(Paging paging) {
         this.paging = paging;
     }
     public Paging getPaging() {
         return paging;
     }

    public void setBaseComponent(BaseComponent baseComponent) {
         this.baseComponent = baseComponent;
     }
     public BaseComponent getBaseComponent() {
         return baseComponent;
     }

    public void setComponents(List<IssuesComponents> components) {
         this.components = components;
     }
     public List<IssuesComponents> getComponents() {
         return components;
     }

    public void setMetrics(List<Metrics> metrics) {
         this.metrics = metrics;
     }
     public List<Metrics> getMetrics() {
         return metrics;
     }

    public void setPeriods(List<Periods> periods) {
         this.periods = periods;
     }
     public List<Periods> getPeriods() {
         return periods;
     }

}