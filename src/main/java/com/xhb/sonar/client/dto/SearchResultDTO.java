/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-28 18:17:4
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class SearchResultDTO {

    private com.xhb.sonar.client.dto.Paging paging;
    private List<Users> users;
    public void setPaging(com.xhb.sonar.client.dto.Paging paging) {
         this.paging = paging;
     }
     public Paging getPaging() {
         return paging;
     }

    public void setUsers(List<Users> users) {
         this.users = users;
     }
     public List<Users> getUsers() {
         return users;
     }

}