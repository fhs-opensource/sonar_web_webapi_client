package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 9:35:16
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class WebServices {

    private String path;
    private String since;
    private String description;
    private List<Actions> actions;
    public void setPath(String path) {
         this.path = path;
     }
     public String getPath() {
         return path;
     }

    public void setSince(String since) {
         this.since = since;
     }
     public String getSince() {
         return since;
     }

    public void setDescription(String description) {
         this.description = description;
     }
     public String getDescription() {
         return description;
     }

    public void setActions(List<Actions> actions) {
         this.actions = actions;
     }
     public List<Actions> getActions() {
         return actions;
     }

}