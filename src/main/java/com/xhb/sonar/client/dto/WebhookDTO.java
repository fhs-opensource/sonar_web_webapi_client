package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-10-09 17:42:2
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class WebhookDTO {

    private Webhook webhook;
    public void setWebhook(Webhook webhook) {
         this.webhook = webhook;
     }
     public Webhook getWebhook() {
         return webhook;
     }

}