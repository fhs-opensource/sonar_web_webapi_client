package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 13:49:37
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class BaseComponent {

    private String key;
    private String name;
    private String qualifier;
    private List<Measures> measures;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setQualifier(String qualifier) {
         this.qualifier = qualifier;
     }
     public String getQualifier() {
         return qualifier;
     }

    public void setMeasures(List<Measures> measures) {
         this.measures = measures;
     }
     public List<Measures> getMeasures() {
         return measures;
     }

}