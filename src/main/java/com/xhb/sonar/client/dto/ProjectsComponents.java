package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-09-30 10:0:48
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ProjectsComponents {

    private Date organization;
    private Date id;
    private Date key;
    private Date name;
    private String qualifier;
    private String visibility;
    private Date lastAnalysisDate;
    private String revision;
    public void setOrganization(Date organization) {
         this.organization = organization;
     }
     public Date getOrganization() {
         return organization;
     }

    public void setId(Date id) {
         this.id = id;
     }
     public Date getId() {
         return id;
     }

    public void setKey(Date key) {
         this.key = key;
     }
     public Date getKey() {
         return key;
     }

    public void setName(Date name) {
         this.name = name;
     }
     public Date getName() {
         return name;
     }

    public void setQualifier(String qualifier) {
         this.qualifier = qualifier;
     }
     public String getQualifier() {
         return qualifier;
     }

    public void setVisibility(String visibility) {
         this.visibility = visibility;
     }
     public String getVisibility() {
         return visibility;
     }

    public void setLastAnalysisDate(Date lastAnalysisDate) {
         this.lastAnalysisDate = lastAnalysisDate;
     }
     public Date getLastAnalysisDate() {
         return lastAnalysisDate;
     }

    public void setRevision(String revision) {
         this.revision = revision;
     }
     public String getRevision() {
         return revision;
     }

}