/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;
import java.util.Date;
import java.util.List;

/**
 * Auto-generated: 2020-09-28 15:41:53
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Issues {

    private String key;
    private String component;
    private String project;
    private String rule;
    private String status;
    private String resolution;
    private String severity;
    private String message;
    private int line;
    private String hash;
    private Date author;
    private String effort;
    private Date creationDate;
    private Date updateDate;
    private List<String> tags;
    private String type;
    private List<Comments> comments;
    private Attr attr;
    private List<String> transitions;
    private List<String> actions;
    private TextRange textRange;
    private List<Flows> flows;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setComponent(String component) {
         this.component = component;
     }
     public String getComponent() {
         return component;
     }

    public void setProject(String project) {
         this.project = project;
     }
     public String getProject() {
         return project;
     }

    public void setRule(String rule) {
         this.rule = rule;
     }
     public String getRule() {
         return rule;
     }

    public void setStatus(String status) {
         this.status = status;
     }
     public String getStatus() {
         return status;
     }

    public void setResolution(String resolution) {
         this.resolution = resolution;
     }
     public String getResolution() {
         return resolution;
     }

    public void setSeverity(String severity) {
         this.severity = severity;
     }
     public String getSeverity() {
         return severity;
     }

    public void setMessage(String message) {
         this.message = message;
     }
     public String getMessage() {
         return message;
     }

    public void setLine(int line) {
         this.line = line;
     }
     public int getLine() {
         return line;
     }

    public void setHash(String hash) {
         this.hash = hash;
     }
     public String getHash() {
         return hash;
     }

    public void setAuthor(Date author) {
         this.author = author;
     }
     public Date getAuthor() {
         return author;
     }

    public void setEffort(String effort) {
         this.effort = effort;
     }
     public String getEffort() {
         return effort;
     }

    public void setCreationDate(Date creationDate) {
         this.creationDate = creationDate;
     }
     public Date getCreationDate() {
         return creationDate;
     }

    public void setUpdateDate(Date updateDate) {
         this.updateDate = updateDate;
     }
     public Date getUpdateDate() {
         return updateDate;
     }

    public void setTags(List<String> tags) {
         this.tags = tags;
     }
     public List<String> getTags() {
         return tags;
     }

    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

    public void setComments(List<Comments> comments) {
         this.comments = comments;
     }
     public List<Comments> getComments() {
         return comments;
     }

    public void setAttr(Attr attr) {
         this.attr = attr;
     }
     public Attr getAttr() {
         return attr;
     }

    public void setTransitions(List<String> transitions) {
         this.transitions = transitions;
     }
     public List<String> getTransitions() {
         return transitions;
     }

    public void setActions(List<String> actions) {
         this.actions = actions;
     }
     public List<String> getActions() {
         return actions;
     }

    public void setTextRange(TextRange textRange) {
         this.textRange = textRange;
     }
     public TextRange getTextRange() {
         return textRange;
     }

    public void setFlows(List<Flows> flows) {
         this.flows = flows;
     }
     public List<Flows> getFlows() {
         return flows;
     }

}