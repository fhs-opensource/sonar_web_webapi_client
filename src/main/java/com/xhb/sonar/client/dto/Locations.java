/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;
import java.util.Date;

/**
 * Auto-generated: 2020-09-28 15:41:53
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Locations {

    private TextRange textRange;
    private Date msg;
    public void setTextRange(TextRange textRange) {
         this.textRange = textRange;
     }
     public TextRange getTextRange() {
         return textRange;
     }

    public void setMsg(Date msg) {
         this.msg = msg;
     }
     public Date getMsg() {
         return msg;
     }

}