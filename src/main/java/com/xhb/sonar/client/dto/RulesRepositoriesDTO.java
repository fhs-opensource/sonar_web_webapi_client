/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-28 18:36:49
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class RulesRepositoriesDTO {

    private List<Repositories> repositories;
    public void setRepositories(List<Repositories> repositories) {
         this.repositories = repositories;
     }
     public List<Repositories> getRepositories() {
         return repositories;
     }

}