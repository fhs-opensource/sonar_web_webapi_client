package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-29 10:25:44
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Group {

    private String id;
    private String organization;
    private String name;
    private String description;
    private int membersCount;
    private boolean _default;
    public void setId(String id) {
         this.id = id;
     }
     public String getId() {
         return id;
     }

    public void setOrganization(String organization) {
         this.organization = organization;
     }
     public String getOrganization() {
         return organization;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setDescription(String description) {
         this.description = description;
     }
     public String getDescription() {
         return description;
     }

    public void setMembersCount(int membersCount) {
         this.membersCount = membersCount;
     }
     public int getMembersCount() {
         return membersCount;
     }

    public void set_default(boolean _default) {
         this._default = _default;
     }
     public boolean get_default() {
         return _default;
     }

}