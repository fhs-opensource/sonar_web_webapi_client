package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 16:4:33
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class HealthStatusDTO {

    private String health;
    private List<Causes> causes;
    private List<Nodes> nodes;
    public void setHealth(String health) {
         this.health = health;
     }
     public String getHealth() {
         return health;
     }

    public void setCauses(List<Causes> causes) {
         this.causes = causes;
     }
     public List<Causes> getCauses() {
         return causes;
     }

    public void setNodes(List<Nodes> nodes) {
         this.nodes = nodes;
     }
     public List<Nodes> getNodes() {
         return nodes;
     }

}