package com.xhb.sonar.client.dto;

import java.util.Date;
import java.util.List;

/**
 * Auto-generated: 2020-09-28 15:1:47
 *
 * @author http://www.itjson.com
 * @website http://www.itjson.com/itjson/json2java.html
 */
public class MeasuresComponentResult
{

    private MeasuresComponentResult.Component component;

    public void setComponent(Component component)
    {
        this.component = component;
    }

    public Component getComponent()
    {
        return component;
    }



    private static class Component
    {

        private String id;
        private String key;
        private String name;
        private String qualifier;
        private String path;
        private String language;
        private List<MeasuresComponentResult.Measures> measures;

        public void setId(String id)
        {
            this.id = id;
        }

        public String getId()
        {
            return id;
        }

        public void setKey(String key)
        {
            this.key = key;
        }

        public String getKey()
        {
            return key;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getName()
        {
            return name;
        }

        public void setQualifier(String qualifier)
        {
            this.qualifier = qualifier;
        }

        public String getQualifier()
        {
            return qualifier;
        }

        public void setPath(String path)
        {
            this.path = path;
        }

        public String getPath()
        {
            return path;
        }

        public void setLanguage(String language)
        {
            this.language = language;
        }

        public String getLanguage()
        {
            return language;
        }

        public void setMeasures(List<Measures> measures)
        {
            this.measures = measures;
        }

        public List<Measures> getMeasures()
        {
            return measures;
        }

    }

    public static class Measures
    {

        private String metric;
        private String value;
        private boolean bestValue;

        public void setMetric(String metric)
        {
            this.metric = metric;
        }

        public String getMetric()
        {
            return metric;
        }

        public void setValue(String value)
        {
            this.value = value;
        }

        public String getValue()
        {
            return value;
        }

        public void setBestValue(boolean bestValue)
        {
            this.bestValue = bestValue;
        }

        public boolean getBestValue()
        {
            return bestValue;
        }

    }

}