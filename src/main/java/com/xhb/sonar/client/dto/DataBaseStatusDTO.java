package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-10-09 16:0:46
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class DataBaseStatusDTO {

    private String state;
    private String message;
    private Date startedAt;
    public void setState(String state) {
         this.state = state;
     }
     public String getState() {
         return state;
     }

    public void setMessage(String message) {
         this.message = message;
     }
     public String getMessage() {
         return message;
     }

    public void setStartedAt(Date startedAt) {
         this.startedAt = startedAt;
     }
     public Date getStartedAt() {
         return startedAt;
     }

}