/**
 * Copyright 2020 bejson.com
 */
package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-28 17:46:16
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class CreateUserDTO {

    private User user;

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

}