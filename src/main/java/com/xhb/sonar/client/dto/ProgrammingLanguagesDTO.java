package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 13:36:34
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class  ProgrammingLanguagesDTO {

    private List<Languages> languages;
    public void setLanguages(List<Languages> languages) {
         this.languages = languages;
     }
     public List<Languages> getLanguages() {
         return languages;
     }

}