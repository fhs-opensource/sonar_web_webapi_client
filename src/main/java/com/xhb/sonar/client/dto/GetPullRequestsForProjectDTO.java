package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-30 14:37:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class GetPullRequestsForProjectDTO {

    private List<PullRequests> pullRequests;
    public void setPullRequests(List<PullRequests> pullRequests) {
         this.pullRequests = pullRequests;
     }
     public List<PullRequests> getPullRequests() {
         return pullRequests;
     }

}