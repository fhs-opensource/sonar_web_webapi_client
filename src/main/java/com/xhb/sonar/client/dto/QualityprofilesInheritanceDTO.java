package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-29 17:39:47
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class QualityprofilesInheritanceDTO {

    private Profile profile;
    private List<Ancestors> ancestors;
    private List<Children> children;
    public void setProfile(Profile profile) {
         this.profile = profile;
     }
     public Profile getProfile() {
         return profile;
     }

    public void setAncestors(List<Ancestors> ancestors) {
         this.ancestors = ancestors;
     }
     public List<Ancestors> getAncestors() {
         return ancestors;
     }

    public void setChildren(List<Children> children) {
         this.children = children;
     }
     public List<Children> getChildren() {
         return children;
     }

}