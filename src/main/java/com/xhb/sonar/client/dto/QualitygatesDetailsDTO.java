package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-30 13:45:34
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class QualitygatesDetailsDTO {

    private int id;
    private String name;
    private List<Conditions> conditions;
    private boolean isBuiltIn;
    private QualitygatesActions qualitygatesActions;
    public void setId(int id) {
         this.id = id;
     }
     public int getId() {
         return id;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setConditions(List<Conditions> conditions) {
         this.conditions = conditions;
     }
     public List<Conditions> getConditions() {
         return conditions;
     }

    public void setIsBuiltIn(boolean isBuiltIn) {
         this.isBuiltIn = isBuiltIn;
     }
     public boolean getIsBuiltIn() {
         return isBuiltIn;
     }

    public void setQualitygatesActions(QualitygatesActions qualitygatesActions) {
         this.qualitygatesActions = qualitygatesActions;
     }
     public QualitygatesActions getQualitygatesActions() {
         return qualitygatesActions;
     }

}