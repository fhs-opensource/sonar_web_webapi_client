package com.xhb.sonar.client.dto;

import java.util.List;
import java.util.Date;

/**
 * Auto-generated: 2020-10-09 14:37:32
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class AvailablePluginsDTO {

    private List<Plugins> plugins;
    private Date updateCenterRefresh;
    public void setPlugins(List<Plugins> plugins) {
         this.plugins = plugins;
     }
     public List<Plugins> getPlugins() {
         return plugins;
     }

    public void setUpdateCenterRefresh(Date updateCenterRefresh) {
         this.updateCenterRefresh = updateCenterRefresh;
     }
     public Date getUpdateCenterRefresh() {
         return updateCenterRefresh;
     }

}