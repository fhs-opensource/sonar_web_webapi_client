package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-09-29 17:39:47
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class QualityprofilesAncestors {

    private String key;
    private String name;
    private Date parent;
    private int activeRuleCount;
    private boolean isBuiltIn;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setParent(Date parent) {
         this.parent = parent;
     }
     public Date getParent() {
         return parent;
     }

    public void setActiveRuleCount(int activeRuleCount) {
         this.activeRuleCount = activeRuleCount;
     }
     public int getActiveRuleCount() {
         return activeRuleCount;
     }

    public void setIsBuiltIn(boolean isBuiltIn) {
         this.isBuiltIn = isBuiltIn;
     }
     public boolean getIsBuiltIn() {
         return isBuiltIn;
     }

}