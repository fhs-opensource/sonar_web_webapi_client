package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 15:38:31
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Definitions {

    private String key;
    private String name;
    private String description;
    private String type;
    private String category;
    private String subCategory;
    private boolean multiValues;
    private String defaultValue;
    private List<String> options;
    private List<String> fields;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setDescription(String description) {
         this.description = description;
     }
     public String getDescription() {
         return description;
     }

    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

    public void setCategory(String category) {
         this.category = category;
     }
     public String getCategory() {
         return category;
     }

    public void setSubCategory(String subCategory) {
         this.subCategory = subCategory;
     }
     public String getSubCategory() {
         return subCategory;
     }

    public void setMultiValues(boolean multiValues) {
         this.multiValues = multiValues;
     }
     public boolean getMultiValues() {
         return multiValues;
     }

    public void setDefaultValue(String defaultValue) {
         this.defaultValue = defaultValue;
     }
     public String getDefaultValue() {
         return defaultValue;
     }

    public void setOptions(List<String> options) {
         this.options = options;
     }
     public List<String> getOptions() {
         return options;
     }

    public void setFields(List<String> fields) {
         this.fields = fields;
     }
     public List<String> getFields() {
         return fields;
     }

}