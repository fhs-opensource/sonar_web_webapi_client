package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-30 11:40:49
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class QualitygatesStatusForProjectDTO {

    private ProjectStatus projectStatus;
    public void setProjectStatus(ProjectStatus projectStatus) {
         this.projectStatus = projectStatus;
     }
     public ProjectStatus getProjectStatus() {
         return projectStatus;
     }

}