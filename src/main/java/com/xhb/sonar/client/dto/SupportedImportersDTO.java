package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-29 17:37:22
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class SupportedImportersDTO {

    private List<Importers> importers;

    public void setImporters(List<Importers> importers) {
        this.importers = importers;
    }

    public List<Importers> getImporters() {
        return importers;
    }
}