package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-10-09 9:14:4
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class UpdateEventDTO {

    private Event event;
    public void setEvent(Event event) {
         this.event = event;
     }
     public Event getEvent() {
         return event;
     }

}