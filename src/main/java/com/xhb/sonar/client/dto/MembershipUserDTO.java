package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-29 11:11:55
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class MembershipUserDTO {

    private List<Users> users;
    private int p;
    private int ps;
    private int total;
    public void setUsers(List<Users> users) {
         this.users = users;
     }
     public List<Users> getUsers() {
         return users;
     }

    public void setP(int p) {
         this.p = p;
     }
     public int getP() {
         return p;
     }

    public void setPs(int ps) {
         this.ps = ps;
     }
     public int getPs() {
         return ps;
     }

    public void setTotal(int total) {
         this.total = total;
     }
     public int getTotal() {
         return total;
     }

}