package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-10-09 13:49:37
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Periods {

    private int index;
    private String mode;
    private Date date;
    private String parameter;
    public void setIndex(int index) {
         this.index = index;
     }
     public int getIndex() {
         return index;
     }

    public void setMode(String mode) {
         this.mode = mode;
     }
     public String getMode() {
         return mode;
     }

    public void setDate(Date date) {
         this.date = date;
     }
     public Date getDate() {
         return date;
     }

    public void setParameter(String parameter) {
         this.parameter = parameter;
     }
     public String getParameter() {
         return parameter;
     }

}