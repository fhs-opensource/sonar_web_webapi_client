package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-29 10:25:44
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class CreateGroupDTO {

    private Group group;
    public void setGroup(Group group) {
         this.group = group;
     }
     public Group getGroup() {
         return group;
     }

}