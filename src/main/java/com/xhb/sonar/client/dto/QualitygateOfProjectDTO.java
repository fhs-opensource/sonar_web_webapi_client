package com.xhb.sonar.client.dto;

import com.xhb.sonar.client.dto.QualityGate;

/**
 * Auto-generated: 2020-09-30 11:23:11
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class QualitygateOfProjectDTO {

    private QualityGate qualityGate;
    public void setQualityGate(QualityGate qualityGate) {
         this.qualityGate = qualityGate;
     }
     public QualityGate getQualityGate() {
         return qualityGate;
     }

}