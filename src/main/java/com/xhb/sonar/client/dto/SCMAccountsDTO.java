/**
  * Copyright 2020 bejson.com
  */
package com.xhb.sonar.client.dto;
import java.util.List;

/**
 * Auto-generated: 2020-09-28 16:0:11
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class SCMAccountsDTO {

    private List<String> authors;
    public void setAuthors(List<String> authors) {
         this.authors = authors;
     }
     public List<String> getAuthors() {
         return authors;
     }

}