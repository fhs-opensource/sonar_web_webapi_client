/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;
import java.util.List;

/**
 * Auto-generated: 2020-09-28 18:17:4
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Users {

    private String login;
    private String name;
    private boolean active;
    private String email;
    private List<String> groups;
    private int tokensCount;
    private boolean local;
    private String externalIdentity;
    private String externalProvider;
    private String avatar;
    public void setLogin(String login) {
         this.login = login;
     }
     public String getLogin() {
         return login;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setActive(boolean active) {
         this.active = active;
     }
     public boolean getActive() {
         return active;
     }

    public void setEmail(String email) {
         this.email = email;
     }
     public String getEmail() {
         return email;
     }

    public void setGroups(List<String> groups) {
         this.groups = groups;
     }
     public List<String> getGroups() {
         return groups;
     }

    public void setTokensCount(int tokensCount) {
         this.tokensCount = tokensCount;
     }
     public int getTokensCount() {
         return tokensCount;
     }

    public void setLocal(boolean local) {
         this.local = local;
     }
     public boolean getLocal() {
         return local;
     }

    public void setExternalIdentity(String externalIdentity) {
         this.externalIdentity = externalIdentity;
     }
     public String getExternalIdentity() {
         return externalIdentity;
     }

    public void setExternalProvider(String externalProvider) {
         this.externalProvider = externalProvider;
     }
     public String getExternalProvider() {
         return externalProvider;
     }

    public void setAvatar(String avatar) {
         this.avatar = avatar;
     }
     public String getAvatar() {
         return avatar;
     }

}