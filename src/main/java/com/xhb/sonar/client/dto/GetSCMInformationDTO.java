package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-29 11:33:2
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class GetSCMInformationDTO {

    private List<List<Integer>> scm;
    public void setScm(List<List<Integer>> scm) {
         this.scm = scm;
     }
     public List<List<Integer>> getScm() {
         return scm;
     }

}