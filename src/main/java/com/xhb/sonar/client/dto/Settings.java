package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-10-09 15:48:2
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Settings {

    private String key;
    private String value;
    private boolean inherited;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setValue(String value) {
         this.value = value;
     }
     public String getValue() {
         return value;
     }

    public void setInherited(boolean inherited) {
         this.inherited = inherited;
     }
     public boolean getInherited() {
         return inherited;
     }

}