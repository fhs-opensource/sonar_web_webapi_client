package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 10:55:55
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class CeComponentDTO {

    private List<Queue> queue;
    private Current current;
    public void setQueue(List<Queue> queue) {
         this.queue = queue;
     }
     public List<Queue> getQueue() {
         return queue;
     }

    public void setCurrent(Current current) {
         this.current = current;
     }
     public Current getCurrent() {
         return current;
     }

}