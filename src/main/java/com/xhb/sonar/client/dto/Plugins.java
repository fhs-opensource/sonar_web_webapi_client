package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 16:19:39
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Plugins {

    private List<RequireUpdate> requireUpdate;
    private List<Incompatible> incompatible;
    public void setRequireUpdate(List<RequireUpdate> requireUpdate) {
         this.requireUpdate = requireUpdate;
     }
     public List<RequireUpdate> getRequireUpdate() {
         return requireUpdate;
     }

    public void setIncompatible(List<Incompatible> incompatible) {
         this.incompatible = incompatible;
     }
     public List<Incompatible> getIncompatible() {
         return incompatible;
     }

}