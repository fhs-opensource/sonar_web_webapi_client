package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-29 17:45:0
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ProjectsAnQualityProfileAssociationStatusDTO {

    private Paging paging;
    private List<Results> results;
    public void setPaging(Paging paging) {
         this.paging = paging;
     }
     public Paging getPaging() {
         return paging;
     }

    public void setResults(List<Results> results) {
         this.results = results;
     }
     public List<Results> getResults() {
         return results;
     }

}