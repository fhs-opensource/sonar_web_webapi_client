package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-10-09 11:16:32
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Component {

    private Date organization;
    private String id;
    private String key;
    private String name;
    private String qualifier;
    private String language;
    private String path;
    private Date analysisDate;
    private Date leakPeriodDate;
    private String version;
    public void setOrganization(Date organization) {
         this.organization = organization;
     }
     public Date getOrganization() {
         return organization;
     }

    public void setId(String id) {
         this.id = id;
     }
     public String getId() {
         return id;
     }

    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setQualifier(String qualifier) {
         this.qualifier = qualifier;
     }
     public String getQualifier() {
         return qualifier;
     }

    public void setLanguage(String language) {
         this.language = language;
     }
     public String getLanguage() {
         return language;
     }

    public void setPath(String path) {
         this.path = path;
     }
     public String getPath() {
         return path;
     }

    public void setAnalysisDate(Date analysisDate) {
         this.analysisDate = analysisDate;
     }
     public Date getAnalysisDate() {
         return analysisDate;
     }

    public void setLeakPeriodDate(Date leakPeriodDate) {
         this.leakPeriodDate = leakPeriodDate;
     }
     public Date getLeakPeriodDate() {
         return leakPeriodDate;
     }

    public void setVersion(String version) {
         this.version = version;
     }
     public String getVersion() {
         return version;
     }

}