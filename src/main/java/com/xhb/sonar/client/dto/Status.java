package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-30 10:41:5
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Status {

    private String qualityGateStatus;
    private int bugs;
    private int vulnerabilities;
    private int codeSmells;
    public void setQualityGateStatus(String qualityGateStatus) {
         this.qualityGateStatus = qualityGateStatus;
     }
     public String getQualityGateStatus() {
         return qualityGateStatus;
     }

    public void setBugs(int bugs) {
         this.bugs = bugs;
     }
     public int getBugs() {
         return bugs;
     }

    public void setVulnerabilities(int vulnerabilities) {
         this.vulnerabilities = vulnerabilities;
     }
     public int getVulnerabilities() {
         return vulnerabilities;
     }

    public void setCodeSmells(int codeSmells) {
         this.codeSmells = codeSmells;
     }
     public int getCodeSmells() {
         return codeSmells;
     }

}