package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-10-09 14:49:25
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Removing {

    private String key;
    private String name;
    private String description;
    private String version;
    private String category;
    private Date license;
    private String organizationName;
    private String organizationUrl;
    private String homepageUrl;
    private String issueTrackerUrl;
    private String implementationBuild;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setDescription(String description) {
         this.description = description;
     }
     public String getDescription() {
         return description;
     }

    public void setVersion(String version) {
         this.version = version;
     }
     public String getVersion() {
         return version;
     }

    public void setCategory(String category) {
         this.category = category;
     }
     public String getCategory() {
         return category;
     }

    public void setLicense(Date license) {
         this.license = license;
     }
     public Date getLicense() {
         return license;
     }

    public void setOrganizationName(String organizationName) {
         this.organizationName = organizationName;
     }
     public String getOrganizationName() {
         return organizationName;
     }

    public void setOrganizationUrl(String organizationUrl) {
         this.organizationUrl = organizationUrl;
     }
     public String getOrganizationUrl() {
         return organizationUrl;
     }

    public void setHomepageUrl(String homepageUrl) {
         this.homepageUrl = homepageUrl;
     }
     public String getHomepageUrl() {
         return homepageUrl;
     }

    public void setIssueTrackerUrl(String issueTrackerUrl) {
         this.issueTrackerUrl = issueTrackerUrl;
     }
     public String getIssueTrackerUrl() {
         return issueTrackerUrl;
     }

    public void setImplementationBuild(String implementationBuild) {
         this.implementationBuild = implementationBuild;
     }
     public String getImplementationBuild() {
         return implementationBuild;
     }

}