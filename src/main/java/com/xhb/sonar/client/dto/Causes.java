package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-10-09 16:4:33
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Causes {

    private String message;
    public void setMessage(String message) {
         this.message = message;
     }
     public String getMessage() {
         return message;
     }

}