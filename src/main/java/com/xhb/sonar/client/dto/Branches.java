package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-09-30 10:41:5
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Branches {

    private String name;
    private boolean isMain;
    private String type;
    private String mergeBranch;
    private Status status;
    private Date analysisDate;
    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setIsMain(boolean isMain) {
         this.isMain = isMain;
     }
     public boolean getIsMain() {
         return isMain;
     }

    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

    public void setMergeBranch(String mergeBranch) {
         this.mergeBranch = mergeBranch;
     }
     public String getMergeBranch() {
         return mergeBranch;
     }

    public void setStatus(Status status) {
         this.status = status;
     }
     public Status getStatus() {
         return status;
     }

    public void setAnalysisDate(Date analysisDate) {
         this.analysisDate = analysisDate;
     }
     public Date getAnalysisDate() {
         return analysisDate;
     }

}