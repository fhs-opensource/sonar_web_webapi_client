package com.xhb.sonar.client.dto;

import java.util.Date;
import java.util.List;

/**
 * Auto-generated: 2020-09-30 15:29:57
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Analyses {

    private String key;
    private Date date;
    private Date projectVersion;
    private String buildString;
    private String revision;
    private boolean manualNewCodePeriodBaseline;
    private List<Events> events;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setDate(Date date) {
         this.date = date;
     }
     public Date getDate() {
         return date;
     }

    public void setProjectVersion(Date projectVersion) {
         this.projectVersion = projectVersion;
     }
     public Date getProjectVersion() {
         return projectVersion;
     }

    public void setBuildString(String buildString) {
         this.buildString = buildString;
     }
     public String getBuildString() {
         return buildString;
     }

    public void setRevision(String revision) {
         this.revision = revision;
     }
     public String getRevision() {
         return revision;
     }

    public void setManualNewCodePeriodBaseline(boolean manualNewCodePeriodBaseline) {
         this.manualNewCodePeriodBaseline = manualNewCodePeriodBaseline;
     }
     public boolean getManualNewCodePeriodBaseline() {
         return manualNewCodePeriodBaseline;
     }

    public void setEvents(List<Events> events) {
         this.events = events;
     }
     public List<Events> getEvents() {
         return events;
     }

}