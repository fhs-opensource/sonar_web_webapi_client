/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;
import java.util.List;

/**
 * Auto-generated: 2020-09-28 18:46:59
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Actives {

    private String qProfile;
    private String inherit;
    private String severity;
    private List<Params> params;
    public void setQProfile(String qProfile) {
         this.qProfile = qProfile;
     }
     public String getQProfile() {
         return qProfile;
     }

    public void setInherit(String inherit) {
         this.inherit = inherit;
     }
     public String getInherit() {
         return inherit;
     }

    public void setSeverity(String severity) {
         this.severity = severity;
     }
     public String getSeverity() {
         return severity;
     }

    public void setParams(List<Params> params) {
         this.params = params;
     }
     public List<Params> getParams() {
         return params;
     }

}