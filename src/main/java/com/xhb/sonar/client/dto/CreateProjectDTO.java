package com.xhb.sonar.client.dto;

import com.xhb.sonar.client.dto.Project;

/**
 * Auto-generated: 2020-09-30 9:30:21
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class CreateProjectDTO {

    private Project project;
    public void setProject(Project project) {
         this.project = project;
     }
     public Project getProject() {
         return project;
     }

}