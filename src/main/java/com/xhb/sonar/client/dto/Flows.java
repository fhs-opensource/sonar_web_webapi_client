/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;
import java.util.List;

/**
 * Auto-generated: 2020-09-28 15:41:53
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Flows {

    private List<Locations> locations;
    public void setLocations(List<Locations> locations) {
         this.locations = locations;
     }
     public List<Locations> getLocations() {
         return locations;
     }

}