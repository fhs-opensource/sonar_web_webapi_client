package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 9:35:16
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class GetWebServiceDTO {

    private List<WebServices> webServices;
    public void setWebServices(List<WebServices> webServices) {
         this.webServices = webServices;
     }
     public List<WebServices> getWebServices() {
         return webServices;
     }

}