/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-28 18:40:20
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Values {

    private String val;
    private int count;
    public void setVal(String val) {
         this.val = val;
     }
     public String getVal() {
         return val;
     }

    public void setCount(int count) {
         this.count = count;
     }
     public int getCount() {
         return count;
     }

}