package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-30 11:23:11
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class QualityGate {

    private String id;
    private String name;
    private boolean _default;
    public void setId(String id) {
         this.id = id;
     }
     public String getId() {
         return id;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void set_default(boolean _default) {
         this._default = _default;
     }
     public boolean get_default() {
         return _default;
     }

}