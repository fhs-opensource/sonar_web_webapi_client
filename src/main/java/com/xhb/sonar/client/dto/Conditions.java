package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-30 13:45:34
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Conditions {

    private int id;
    private String metric;
    private String op;
    private String error;
    public void setId(int id) {
         this.id = id;
     }
     public int getId() {
         return id;
     }

    public void setMetric(String metric) {
         this.metric = metric;
     }
     public String getMetric() {
         return metric;
     }

    public void setOp(String op) {
         this.op = op;
     }
     public String getOp() {
         return op;
     }

    public void setError(String error) {
         this.error = error;
     }
     public String getError() {
         return error;
     }

}