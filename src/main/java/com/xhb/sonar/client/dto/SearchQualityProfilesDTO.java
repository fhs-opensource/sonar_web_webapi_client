package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-29 17:58:9
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class SearchQualityProfilesDTO {

    private List<Profiles> profiles;
    private QualityprofilesActions actions;
    public void setProfiles(List<Profiles> profiles) {
         this.profiles = profiles;
     }
     public List<Profiles> getProfiles() {
         return profiles;
     }

    public void setActions(QualityprofilesActions actions) {
         this.actions = actions;
     }
     public QualityprofilesActions getActions() {
         return actions;
     }

}