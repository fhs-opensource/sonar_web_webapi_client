package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-29 17:8:26
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class QualityprofilesParams {

    private String severity;

    public void setSeverity(String severity) {
        this.severity = severity;
    }
    public String getSeverity() {
        return severity;
    }

}