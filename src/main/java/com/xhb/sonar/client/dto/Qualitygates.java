package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-30 11:31:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Qualitygates {

    private int id;
    private String name;
    private boolean isDefault;
    private boolean isBuiltIn;
    private QualitygatesActions qualitygatesActions;
    public void setId(int id) {
         this.id = id;
     }
     public int getId() {
         return id;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setIsDefault(boolean isDefault) {
         this.isDefault = isDefault;
     }
     public boolean getIsDefault() {
         return isDefault;
     }

    public void setIsBuiltIn(boolean isBuiltIn) {
         this.isBuiltIn = isBuiltIn;
     }
     public boolean getIsBuiltIn() {
         return isBuiltIn;
     }

    public void setQualitygatesActions(QualitygatesActions qualitygatesActions) {
         this.qualitygatesActions = qualitygatesActions;
     }
     public QualitygatesActions getQualitygatesActions() {
         return qualitygatesActions;
     }

}