package com.xhb.sonar.client.dto;

import com.xhb.sonar.client.dto.QualityprofilesParams;

import java.util.Date;

/**
 * Auto-generated: 2020-09-29 17:8:26
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class QualityprofilesEvents {

    private Date date;
    private String action;
    private String authorLogin;
    private String authorName;
    private String ruleKey;
    private String ruleName;
    private QualityprofilesParams qualityprofilesParams;
    public void setDate(Date date) {
         this.date = date;
     }
     public Date getDate() {
         return date;
     }

    public void setAction(String action) {
         this.action = action;
     }
     public String getAction() {
         return action;
     }

    public void setAuthorLogin(String authorLogin) {
         this.authorLogin = authorLogin;
     }
     public String getAuthorLogin() {
         return authorLogin;
     }

    public void setAuthorName(String authorName) {
         this.authorName = authorName;
     }
     public String getAuthorName() {
         return authorName;
     }

    public void setRuleKey(String ruleKey) {
         this.ruleKey = ruleKey;
     }
     public String getRuleKey() {
         return ruleKey;
     }

    public void setRuleName(String ruleName) {
         this.ruleName = ruleName;
     }
     public String getRuleName() {
         return ruleName;
     }

    public void setQualityprofilesParams(QualityprofilesParams qualityprofilesParams) {
         this.qualityprofilesParams = qualityprofilesParams;
     }
     public QualityprofilesParams getQualityprofilesParams() {
         return qualityprofilesParams;
     }

}