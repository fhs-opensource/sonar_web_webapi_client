package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-10-09 11:43:31
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Blocks {

    private int from;
    private int size;
    private String _ref;
    public void setFrom(int from) {
         this.from = from;
     }
     public int getFrom() {
         return from;
     }

    public void setSize(int size) {
         this.size = size;
     }
     public int getSize() {
         return size;
     }

    public void set_ref(String _ref) {
         this._ref = _ref;
     }
     public String get_ref() {
         return _ref;
     }

}