package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-09-29 15:21:36
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class PermissionTemplate {

    private String id;
    private String name;
    private String description;
    private String projectKeyPattern;
    private Date createdAt;
    private Date updatedAt;
    public void setId(String id) {
         this.id = id;
     }
     public String getId() {
         return id;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setDescription(String description) {
         this.description = description;
     }
     public String getDescription() {
         return description;
     }

    public void setProjectKeyPattern(String projectKeyPattern) {
         this.projectKeyPattern = projectKeyPattern;
     }
     public String getProjectKeyPattern() {
         return projectKeyPattern;
     }

    public void setCreatedAt(Date createdAt) {
         this.createdAt = createdAt;
     }
     public Date getCreatedAt() {
         return createdAt;
     }

    public void setUpdatedAt(Date updatedAt) {
         this.updatedAt = updatedAt;
     }
     public Date getUpdatedAt() {
         return updatedAt;
     }

}