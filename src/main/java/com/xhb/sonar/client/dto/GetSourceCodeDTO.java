package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-29 11:38:30
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class GetSourceCodeDTO {

    private List<List<Integer>> sources;
    public void setSources(List<List<Integer>> sources) {
         this.sources = sources;
     }
     public List<List<Integer>> getSources() {
         return sources;
     }

}