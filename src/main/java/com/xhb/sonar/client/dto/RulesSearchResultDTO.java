/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-09-28 18:40:20
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class RulesSearchResultDTO {

    private int total;
    private int p;
    private int ps;
    private List<Rules> rules;
    private Actives actives;
    private List<Facets> facets;
    public void setTotal(int total) {
         this.total = total;
     }
     public int getTotal() {
         return total;
     }

    public void setP(int p) {
         this.p = p;
     }
     public int getP() {
         return p;
     }

    public void setPs(int ps) {
         this.ps = ps;
     }
     public int getPs() {
         return ps;
     }

    public void setRules(List<Rules> rules) {
         this.rules = rules;
     }
     public List<Rules> getRules() {
         return rules;
     }

    public void setActives(Actives actives) {
         this.actives = actives;
     }
     public Actives getActives() {
         return actives;
     }

    public void setFacets(List<Facets> facets) {
         this.facets = facets;
     }
     public List<Facets> getFacets() {
         return facets;
     }

}