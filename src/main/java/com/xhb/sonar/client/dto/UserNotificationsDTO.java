package com.xhb.sonar.client.dto;

import java.util.List;

/**
 * Auto-generated: 2020-10-09 14:21:42
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class UserNotificationsDTO {

    private List<Notifications> notifications;
    private List<String> channels;
    private List<String> globalTypes;
    private List<String> perProjectTypes;
    public void setNotifications(List<Notifications> notifications) {
         this.notifications = notifications;
     }
     public List<Notifications> getNotifications() {
         return notifications;
     }

    public void setChannels(List<String> channels) {
         this.channels = channels;
     }
     public List<String> getChannels() {
         return channels;
     }

    public void setGlobalTypes(List<String> globalTypes) {
         this.globalTypes = globalTypes;
     }
     public List<String> getGlobalTypes() {
         return globalTypes;
     }

    public void setPerProjectTypes(List<String> perProjectTypes) {
         this.perProjectTypes = perProjectTypes;
     }
     public List<String> getPerProjectTypes() {
         return perProjectTypes;
     }

}