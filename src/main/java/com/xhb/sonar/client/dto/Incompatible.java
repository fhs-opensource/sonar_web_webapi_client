package com.xhb.sonar.client.dto;

import java.util.Date;

/**
 * Auto-generated: 2020-10-09 16:19:39
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Incompatible {

    private String key;
    private String name;
    private String category;
    private String description;
    private Date license;
    private String organizationName;
    private String organizationUrl;
    private boolean editionBundled;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setCategory(String category) {
         this.category = category;
     }
     public String getCategory() {
         return category;
     }

    public void setDescription(String description) {
         this.description = description;
     }
     public String getDescription() {
         return description;
     }

    public void setLicense(Date license) {
         this.license = license;
     }
     public Date getLicense() {
         return license;
     }

    public void setOrganizationName(String organizationName) {
         this.organizationName = organizationName;
     }
     public String getOrganizationName() {
         return organizationName;
     }

    public void setOrganizationUrl(String organizationUrl) {
         this.organizationUrl = organizationUrl;
     }
     public String getOrganizationUrl() {
         return organizationUrl;
     }

    public void setEditionBundled(boolean editionBundled) {
         this.editionBundled = editionBundled;
     }
     public boolean getEditionBundled() {
         return editionBundled;
     }

}