package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-29 17:15:46
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class CopyQualityProfileDTO {

    private String key;
    private String name;
    private String language;
    private boolean isDefault;
    private boolean isInherited;
    private String parentKey;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setLanguage(String language) {
         this.language = language;
     }
     public String getLanguage() {
         return language;
     }

    public void setIsDefault(boolean isDefault) {
         this.isDefault = isDefault;
     }
     public boolean getIsDefault() {
         return isDefault;
     }

    public void setIsInherited(boolean isInherited) {
         this.isInherited = isInherited;
     }
     public boolean getIsInherited() {
         return isInherited;
     }

    public void setParentKey(String parentKey) {
         this.parentKey = parentKey;
     }
     public String getParentKey() {
         return parentKey;
     }

}