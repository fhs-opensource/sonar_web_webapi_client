/**
  * Copyright 2020 bejson.com 
  */
package com.xhb.sonar.client.dto;

import java.util.Date;
import java.util.List;

/**
 * Auto-generated: 2020-09-28 18:52:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Rule {

    private String key;
    private String repo;
    private String name;
    private Date createdAt;
    private String htmlDesc;
    private String mdDesc;
    private String severity;
    private String status;
    private boolean isTemplate;
    private String templateKey;
    private List<String> tags;
    private List<String> sysTags;
    private String lang;
    private String langName;
    private List<RulesParams> params;
    private boolean debtOverloaded;
    private boolean remFnOverloaded;
    private String scope;
    private boolean isExternal;
    private String type;
    public void setKey(String key) {
         this.key = key;
     }
     public String getKey() {
         return key;
     }

    public void setRepo(String repo) {
         this.repo = repo;
     }
     public String getRepo() {
         return repo;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setCreatedAt(Date createdAt) {
         this.createdAt = createdAt;
     }
     public Date getCreatedAt() {
         return createdAt;
     }

    public void setHtmlDesc(String htmlDesc) {
         this.htmlDesc = htmlDesc;
     }
     public String getHtmlDesc() {
         return htmlDesc;
     }

    public void setMdDesc(String mdDesc) {
         this.mdDesc = mdDesc;
     }
     public String getMdDesc() {
         return mdDesc;
     }

    public void setSeverity(String severity) {
         this.severity = severity;
     }
     public String getSeverity() {
         return severity;
     }

    public void setStatus(String status) {
         this.status = status;
     }
     public String getStatus() {
         return status;
     }

    public void setIsTemplate(boolean isTemplate) {
         this.isTemplate = isTemplate;
     }
     public boolean getIsTemplate() {
         return isTemplate;
     }

    public void setTemplateKey(String templateKey) {
         this.templateKey = templateKey;
     }
     public String getTemplateKey() {
         return templateKey;
     }

    public void setTags(List<String> tags) {
         this.tags = tags;
     }
     public List<String> getTags() {
         return tags;
     }

    public void setSysTags(List<String> sysTags) {
         this.sysTags = sysTags;
     }
     public List<String> getSysTags() {
         return sysTags;
     }

    public void setLang(String lang) {
         this.lang = lang;
     }
     public String getLang() {
         return lang;
     }

    public void setLangName(String langName) {
         this.langName = langName;
     }
     public String getLangName() {
         return langName;
     }

    public void setParams(List<RulesParams> params) {
         this.params = params;
     }
     public List<RulesParams> getParams() {
         return params;
     }

    public void setDebtOverloaded(boolean debtOverloaded) {
         this.debtOverloaded = debtOverloaded;
     }
     public boolean getDebtOverloaded() {
         return debtOverloaded;
     }

    public void setRemFnOverloaded(boolean remFnOverloaded) {
         this.remFnOverloaded = remFnOverloaded;
     }
     public boolean getRemFnOverloaded() {
         return remFnOverloaded;
     }

    public void setScope(String scope) {
         this.scope = scope;
     }
     public String getScope() {
         return scope;
     }

    public void setIsExternal(boolean isExternal) {
         this.isExternal = isExternal;
     }
     public boolean getIsExternal() {
         return isExternal;
     }

    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

}