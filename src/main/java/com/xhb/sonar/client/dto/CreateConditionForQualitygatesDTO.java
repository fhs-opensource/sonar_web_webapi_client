package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-30 11:7:26
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class CreateConditionForQualitygatesDTO {

    private int id;
    private String metric;
    private String op;
    private String error;
    private String warning;
    public void setId(int id) {
         this.id = id;
     }
     public int getId() {
         return id;
     }

    public void setMetric(String metric) {
         this.metric = metric;
     }
     public String getMetric() {
         return metric;
     }

    public void setOp(String op) {
         this.op = op;
     }
     public String getOp() {
         return op;
     }

    public void setError(String error) {
         this.error = error;
     }
     public String getError() {
         return error;
     }

    public void setWarning(String warning) {
         this.warning = warning;
     }
     public String getWarning() {
         return warning;
     }

}