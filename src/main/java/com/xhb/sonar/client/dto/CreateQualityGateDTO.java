package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-09-30 10:58:10
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class CreateQualityGateDTO {

    private int id;
    private String name;
    public void setId(int id) {
         this.id = id;
     }
     public int getId() {
         return id;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

}