package com.xhb.sonar.client.dto;

/**
 * Auto-generated: 2020-10-09 10:51:51
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ActivityStatusDTO {

    private int pending;
    private int inProgress;
    private int failing;
    private long pendingTime;
    public void setPending(int pending) {
         this.pending = pending;
     }
     public int getPending() {
         return pending;
     }

    public void setInProgress(int inProgress) {
         this.inProgress = inProgress;
     }
     public int getInProgress() {
         return inProgress;
     }

    public void setFailing(int failing) {
         this.failing = failing;
     }
     public int getFailing() {
         return failing;
     }

    public void setPendingTime(long pendingTime) {
         this.pendingTime = pendingTime;
     }
     public long getPendingTime() {
         return pendingTime;
     }

}